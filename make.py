import sys
import os
import commands

def findfiles(path,ext):
    cppfiles=[]
    for root, dirs, files in os.walk(path):
        if '.svn' in dirs:
            dirs.remove('.svn')
        for file in files:
            if file.endswith(ext):
                 cppfiles.append(os.path.join(root, file))
    return cppfiles
directory=os.path.dirname(os.path.realpath(__file__))
whereami=os.uname()[1]
lib_path=[]
header_path=[]
env=[]
if whereami=="tech-wn50":
    commands.getstatusoutput("source /cvmfs/atlas.cern.ch/repo/sw/atlas-gcc/472/x86_64/setup.sh")    
elif whereami=="tamnun":
    env="source /usr/local/profiles/gcc4.7.sh;"
    lib_path=["/u/snirgaz/local/lib"]
    header_path=["/u/snirgaz/local/include"]
CC="g++ -w -O3 -std=c++11 -fopenmp"
SOURCE=" ".join(findfiles(directory+"/src",".cpp"))
CFLAGS="-DNDEBUG -DPARALLEL_OPENMP -DCENTER_OF_MASS_MOVE -DSWAP -DFORCE -DPOT_AZIZ -DSIM_DIM=3"
LIBFLAGS=" ".join(["-L"+x for x in lib_path])
INCLUDEFLAGS=" ".join(["-I"+x for x in header_path])
LFLAGS="-lhdf5 -lfftw3 -lpthread -lhdf5_cpp -lz -lyaml-cpp"
OUT_DIR=directory+"/../Release"
if not os.path.exists(OUT_DIR):
    os.makedirs(OUT_DIR)
binary=OUT_DIR+"/pimc"
command=" ".join([env,CC,INCLUDEFLAGS,SOURCE,CFLAGS,LIBFLAGS,LFLAGS,"-o",binary])
print whereami
if whereami=="phsnir" or whereami=="phantigone.technion.ac.il":
    command=" ".join(["scl enable devtoolset-1.1","\'"+command+"\'"])
print command
commands.getstatusoutput(command)

