/*
 * Simulation.h
 *
 *  Created on: Jan 24, 2010
 *      Author: snirgaz
 */

#ifndef SIMULATION_H_
#define SIMULATION_H_

#include "Measure.h"
#include "HelpLibs/def.h"
#include "WorldLines.h"
#include "RandomWalks.h"
#include "EnergyCalc.h"
#include "HelpLibs/RandomGen.h"
#include "HelpLibs/QmcParameters.h"
#include "Bisection.h"
#include "SimParams.h"
#include "HelpLibs/InputFile.h"
#include "move.h"
#include "omp.h"

class Qmc {
private:
	Measurements measurements_;
	WorldLines wl_;
	Bisection bisection_;
	MoveParams moveParams_;
	// energy Calculation
	EnergyCalc energyCalc_;
	// Moves
	OpenMove openMove_;
	CloseMove closeMove_;
	WiggleMove wiggleMove_;
#ifdef SWAP
	SwapMove swapMove_;
#endif
#ifdef CENTER_OF_MASS_MOVE
	CenterOfMassMove centerOfMassMove_;
#endif
	QmcParameters qmcParams_;
	SimulationParameters simParams_;
	UniformInt nextMove_;
	ofstream outFile_;
	MTGen rndEng_;

public:
	void init();
	void readParamsFromFile(string fileName);
	//Initialize Configurations
	void initConf();
	void initializeWLDataFromFile(string fileName);
	void initializeFromFile();
	void thermalize();
	void runN(int n);
	bool run();
	bool nextMove();
	void dump(bool final);
	void updateLambda(double lambda);
	void updateRS(double rs);
	// Debug
	bool EKcheck();
	bool energy_diff();
	void printEnergyDiff();
	void energytest();
#ifdef PYTHON
	void getParamsFromPython(boost::python::dict qmcParams);
	boost::python::object getConfPython();
	boost::python::dict calcTotalEnergy();
#endif
};

#ifdef PYTHON
BOOST_PYTHON_MODULE(pimc_lib)
{
	using namespace boost::python;
	boost::numpy::initialize();
	class_<Qmc, boost::noncopyable>("pimc_lib")
			.def("init", &Qmc::init)
			.def("nextMove", &Qmc::nextMove)
			.def("getConf", &Qmc::getConfPython)
			.def("getParamsFromPython", &Qmc::getParamsFromPython)
			.def("calcTotalEnergy", &Qmc::calcTotalEnergy)
			.def("run", &Qmc::run);
}
#endif
#endif /* SIMULATION_H_ */

