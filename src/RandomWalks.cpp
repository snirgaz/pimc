/*
 * RandomWalks.cpp
 *
 *  Created on: Jan 21, 2010
 *      Author: snirgaz
 */

#include "RandomWalks.h"

using namespace std;
// Constructor

//Destructor

void RandomWalks::generateLevelPath(MoveParams *moveParams) {
	vector<int>::iterator itd;
	list<int>::iterator itb;
	int numOfBeads = moveParams->deltas.size();
	ParticlePos jump;
	double sigma;
	for (itd = moveParams->deltas.begin(), itb =
			moveParams_->currentBeads.begin();
			itb != moveParams_->currentBeads.end(); itd++, itb++) {
		// Draw RW jump
		for (ParticlePos::iterator it = jump.begin(); it != jump.end(); it++)
			*it = drawNormal();
		//QmcRnd::normal_.getMulti(QmcParams::dim, jump.getData());
		sigma = preCalcedSigmas_[*itd];
		//jump *= sigma;
#ifdef ROT
		jump.rotate();
#endif
		moveParams->beads[*itb].beadPos_ += sigma * jump;
		//itb->beadPos_.addBeadPos(jump);
		assert(moveParams->beads[*itb].beadPos_.inBoxLimits());
	}
}

void RandomWalks::init(int m, SimulationParameters *simParams, SimGen* rngGen,
		MoveParams *moveParams) {
	normal_.init(rngGen, NormalDistParams(0, simParams->sigma));
	int j;
	int M = m;
	moveParams_ = moveParams;
	preCalcedSigmas_.resize(M + 2);
	for (int i = 1; i < (M + 1); i++) {
		double total = double(i), alpha = double(i / 2) / total;
		preCalcedSigmas_[i] = sqrt(total * alpha * (1 - alpha));
	}
}

void RandomWalks::generateJumpBeadPos(BeadVec::const_iterator const &beadStart,
		BeadVec::const_iterator const &beadEnd, BeadVec::iterator &beadTau) {
	int deltaRight, deltaLeft, numOfBeads;
	numOfBeads = beadEnd->RWSlice_ - beadStart->RWSlice_ + 1;
	deltaLeft = beadTau->RWSlice_;
	deltaRight = numOfBeads - (deltaLeft + 1);
	for (ParticlePos::iterator it = beadTau->beadPos_.begin(); it != beadTau->beadPos_.end();
			it++)
		*it = sqrt(double(deltaLeft))*drawNormal();
	for (ParticlePos::iterator it = beadTau->jumpPos_.begin(); it != beadTau->jumpPos_.end();
			it++)
		*it =sqrt(double(deltaRight))* drawNormal();
	//beadTau->beadPos_ *= sqrt(double(deltaLeft));
	//beadTau->jumpPos_ *= sqrt(double(deltaRight));
//	beadTau->beadPos_.mulBeadPos(sqrt(double(deltaLeft)));
//		beadTau->jumpPos_.mulBeadPos(sqrt(double(deltaRight)));
#ifdef ROT
	beadTau->beadPos_.rotate();
	beadTau->jumpPos_.rotate();
#endif
	//beadTau->beadPos_ += beadStart->beadPos_;
	//beadTau->jumpPos_ += beadEnd->beadPos_;
	beadTau->beadPos_ += beadStart->beadPos_;
	beadTau->jumpPos_ += beadEnd->beadPos_;
//	beadTau->beadPos_.addBeadPos(beadStart.beadPos_);
//	beadTau->jumpPos_.addBeadPos(beadEnd.beadPos_);
	assert(beadTau->beadPos_.inBoxLimits());
	assert(beadTau->jumpPos_.inBoxLimits());
}

void RandomWalks::updateVar(double var) {
	normal_.setParams(NormalDistParams(0, var));
}

double RandomWalks::drawNormal() {
	return normal_.draw();
}

