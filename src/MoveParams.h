#ifndef MOVE_PARAMS_H_
#define MOVE_PARAMS_H_

#include <list>
#include "WorldLines.h"
#include <vector>
#include "HelpLibs/QmcParameters.h"

enum OP {
	ORIGINAL, PROPOSED
};

struct CalcBead {
	int m;
	OP op;
	bool jump;
	CalcBead(int min, OP opin, bool jumpin) {
		m = min;
		op = opin;
		jump = jumpin;
	}
};
struct MoveParams {
public:
	bool accepted;
	BeadVec beads;
	list<int> allBeads;
	list<int> currentBeads;
	list<CalcBead> calcBeads;
	vector<int> deltas;
	WorldLinesForce forecUpdate;
	int mStart;
	int mEnd;
	int wlStart;
	int wlEnd;
	int tau;
	int mbar;
	int moveType;
	EnergyData energyDiff;
	void init(QmcParameters qmcParameters);
	void dumpBeads(string fileName);
#ifdef PYTHON
	boost::python::object toPython();
#endif
};

#endif /* MOVE_PARAMS_H_*/
