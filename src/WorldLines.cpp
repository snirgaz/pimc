/*
 * WorldLines.cpp
 *
 *  Created on: Jan 24, 2010
 *      Author: snirgaz
 */
#include "WorldLines.h"
bool isInRange(double *vec, int size, double lim) {
	for (int p = 0; p < size; p++) {
		if (fabs(vec[p]) > lim)
			return false;
	}
	return true;
}

////////////////////////
// World Line Force Data
///////////////////////
void WorldLinesForce::init(int m, int n) {
	resize(m, n);
}

void WorldLinesForce::operator=(WorldLinesForce & wlForce) {
	forceData_ = wlForce.forceData_.copy();
	jumpForceData_ = wlForce.jumpForceData_.copy();
	totalForce_ = wlForce.totalForce_;
	totalForceJump_ = wlForce.totalForceJump_;

}
void WorldLinesForce::resize(int m, int n) {
	forceData_.resize(m, n);
//	jumpForceDataAlloc_ = new double[NumP * dim];
//	totalForceAlloc_ = new double[M];
//	jumpForceData_ = jumpForceDataAlloc_;
//	totalForce_ = totalForceAlloc_;
	jumpForceData_.resize(n);
	totalForce_.resize(m, 0);
	fill(totalForce_.begin(), totalForce_.end(), 0.);
	fill(forceData_.begin(), forceData_.end(), Force(0.));
	fill(jumpForceData_.begin(), jumpForceData_.end(), Force(0.));
	totalForceJump_ = 0;
}

void WorldLinesForce::dumpToFile(hdf5RW &hdf5File) const {
	string groupName = "/Force", dsNameTemp;
	hdf5File.createGroup(groupName);
	dsNameTemp = groupName + "/Data";
// Force Data
	hdf5File.dumpPosArray(dsNameTemp, forceData_);
// Jump Force Data
	dsNameTemp = groupName + "/JumpForceData";
	hdf5File.dumpPosArray(dsNameTemp, jumpForceData_);
// Total Force
	dsNameTemp = groupName + "/TotalForce";
	hdf5File.dumpVector<double>(dsNameTemp, totalForce_);
// Total Jump
// WORM_WL
	dsNameTemp = groupName + "/TotalForceJump";
	hdf5File.writeSingle<double>(dsNameTemp, totalForceJump_);
}
void WorldLinesForce::readFromFile(string fileName) {
	string ds_name;
	hdf5RW hdf5File;
	hdf5File.initRead(fileName);
	// Force Data
	ds_name = "/Force/Data";
	hdf5File.read<double>(ds_name, &(forceData_(0, 0)[0]));
	// Jump Force Data
	ds_name = "/Force/JumpForceData";
	hdf5File.read<double>(ds_name, &(jumpForceData_(0)[0]));
	// Total Force
	ds_name = "/Force/TotalForce";
	hdf5File.read<double>(ds_name, &(totalForce_[0]));
	// Total Force Jump
	ds_name = "/Force/TotalForceJump";
	hdf5File.read<double>(ds_name, &totalForceJump_);
}
void WorldLinesForce::dumpToFile(string fileName) const {
// Open File
	hdf5RW hdf5File;
	hdf5File.initNew(fileName);
	this->dumpToFile(hdf5File);
}
#ifdef PYTHON
boost::python::dict WorldLinesForce::toPython() const {
	boost::python::dict forcedata;
	int M = forceData_.shape()[0];
	int numP = forceData_.shape()[1];
	boost::python::tuple dims = boost::python::make_tuple(M, numP, SIM_DIM);
	boost::numpy::dtype dtype = boost::numpy::dtype::get_builtin<double>();
	boost::numpy::ndarray forcenp = boost::numpy::zeros(dims, dtype);
	dims = boost::python::make_tuple(numP, SIM_DIM);
	boost::numpy::ndarray jumpforcenp = boost::numpy::zeros(dims, dtype);
	dims = boost::python::make_tuple(M);
	boost::numpy::ndarray totalforce = boost::numpy::zeros(dims, dtype);
	for (int m = 0; m < M; m++) {
		for (int n = 0; n < numP; n++) {
			for (int d = 0; d < SIM_DIM; d++) {
				forcenp[m][n][d] = forceData_(m, n)[d];
			}
		}
	}
	for (int n = 0; n < numP; n++) {
		for (int d = 0; d < SIM_DIM; d++) {
			jumpforcenp[n][d] = jumpForceData_(n)[d];
		}
	}
	for (int m = 0; m < M; m++) {
		totalforce[m] = totalForce_[m];

	}
	forcedata["data"] = forcenp;
	forcedata["jump_data"] = jumpforcenp;
	forcedata["total"] = totalforce;
	forcedata["total_jump"] = totalForceJump_;
	return forcedata;
}
#endif
/////////////////////
// World Line Worm
/////////////////////
void WorldLinesWorm::init(int M, int NumP) {
	wormNum_ = 0;
	wormSlice_ = 0;
	isOpenClosed_ = CLOSED;
	confData_.resize(M, NumP);
	loops_.resize(NumP);
}

int WorldLinesWorm::findPreviousLoop(int n) const {
	vector<int>::const_iterator it = find(loops_.begin(), loops_.end(), n);
	return (it - loops_.begin());
}

// IO
void WorldLinesWorm::dumpToFile(hdf5RW &hdf5File) const {
	string groupName = "/Worm", dsNameTemp;
	hdf5File.createGroup(groupName);
	int openClosed;
	if (isOpenClosed_ == CLOSED)
		openClosed = 1;
	else
		openClosed = 0;
	dsNameTemp = groupName + "/IsClosed";
	hdf5File.writeSingle<int>(dsNameTemp, openClosed);
	dsNameTemp = groupName + "/Conf";
	hdf5File.dumpPosArray(dsNameTemp, confData_);
// Loops
	DimsHdf5 loops_dims = boost::assign::list_of(loops_.size());
	dsNameTemp = groupName + "/Loops";
	hdf5File.writeComp<int>(dsNameTemp, loops_dims, &(loops_[0]));
// Delta
	DimsHdf5 delta_dims = boost::assign::list_of(SIM_DIM);
	vector<double> jumpPos(SIM_DIM);
	for (int d=0;d<SIM_DIM;d++)
		jumpPos[d]=jumpPos_[d];
	dsNameTemp = groupName + "/JumpPos";
	hdf5File.writeComp<double>(dsNameTemp, delta_dims, &(jumpPos[0]));
// Tau
	dsNameTemp = groupName + "/Tau";
	hdf5File.writeSingle<int>(dsNameTemp, wormSlice_);
// WORM_WL
	dsNameTemp = groupName + "/Worm";
	hdf5File.writeSingle<int>(dsNameTemp, wormNum_);
}
void WorldLinesWorm::dumpToFile(string fileName) const {
// Open File
	hdf5RW hdf5File;
	hdf5File.initNew(fileName.c_str());
	this->dumpToFile(hdf5File);
}
void WorldLinesWorm::readFromFile(string fileName) {
	string ds_name;
	hdf5RW hdf5File;
	hdf5File.initRead(fileName);
	ds_name = "/Worm/Conf";
	hdf5File.read<double>(ds_name, &(confData_(0, 0)[0]));
	ds_name = "/Worm/Loops";
	hdf5File.read<int>(ds_name, &(loops_[0]));
	ds_name = "/Worm/JumpPos";
	hdf5File.read<double>(ds_name, &(jumpPos_[0]));
// Tau
	ds_name = "/Worm/Tau";
	hdf5File.read<int>(ds_name, &wormSlice_);
// Worm_WL
	ds_name = "/Worm/Worm";
	hdf5File.read<int>(ds_name, &wormNum_);
// Is Closed
	ds_name = "/Worm/IsClosed";
	int openClosed;
	hdf5File.read<int>(ds_name, &openClosed);
	isOpenClosed_ = (openClosed == 1) ? CLOSED : OPEN;
}
#ifdef PYTHON
boost::python::dict WorldLinesWorm::toPython() const {
	boost::python::dict wlData;
	boost::python::dict wlEnergy;
	boost::python::list wlconf;
	int M = confData_.shape()[0];
	int numP = confData_.shape()[1];
	boost::python::tuple dims = boost::python::make_tuple(M, numP, SIM_DIM);
	boost::numpy::dtype dtype = boost::numpy::dtype::get_builtin<double>();
	boost::numpy::ndarray wlnp = boost::numpy::zeros(dims, dtype);
	for (int m = 0; m < M; m++) {
		boost::python::list timeslice;
		for (int n = 0; n < numP; n++) {

			for (int d = 0; d < SIM_DIM; d++) {
				wlnp[m][n][d] = confData_(m, n)[d];
			}
		}
	}
	wlData["wl_conf"] = wlnp;
	dims = boost::python::make_tuple(numP);
	dtype = boost::numpy::dtype::get_builtin<int>();
	boost::numpy::ndarray loops = boost::numpy::zeros(dims, dtype);
	for (int n = 0; n < numP; n++) {
		loops[n] = (loops_[n]);
	}
	wlData["loops"] = loops;
	wlData["worm_slice"] = wormSlice_;
	wlData["worm_num"] = wormNum_;
	wlData["jump_pos"] = jumpPos_.toPython();
	wlData["is_open"] = (isOpenClosed_ == OPEN) ? true : false;
	return wlData;
}
#endif
void WorldLines::init(int m, int n) {
	wlData_.init(m, n);
	wlForce_.init(m, n);
	numP_ = n;
	M_ = m;
	for (int n = 0; n < numP_; n++)
		wlData_.loops_[n] = n;
	wlData_.isOpenClosed_ = CLOSED;

}
void WorldLines::dumpToFile(string fileName) const {
	string ds_name;
	hdf5RW hdf5File;
	hdf5File.initNew(fileName);
	wlData_.dumpToFile(hdf5File);
	wlForce_.dumpToFile(hdf5File);
	wlEnergy_.dumpToFile(hdf5File);
}
void WorldLines::readFromFile(string fileName) {
	string ds_name;
	wlData_.readFromFile(fileName);
	wlForce_.readFromFile(fileName);
	wlEnergy_.readFromFile(fileName);
}

double WorldLines::calcEK() {
	double ek = 0;
	for (int n = 0; n < numP_; n++) {
		ParticlePos delta;
		for (int m = 0; m < (M_ - 1); m++) {
			if ((n == wlData_.wormNum_) && (m == wlData_.wormSlice_)
					&& (wlData_.isOpenClosed_ == OPEN))
				delta = wlData_.confData_(m + 1, n) - wlData_.jumpPos_;
			else
				delta = wlData_.confData_(m + 1, n) - wlData_.confData_(m, n);
			ek += delta.sumOfSqures();
		}
		if ((n == wlData_.wormNum_) && ((M_ - 1) == wlData_.wormSlice_)
				&& (wlData_.isOpenClosed_ == OPEN))
			delta = wlData_.confData_(0, wlData_.loops_[n]) - wlData_.jumpPos_;
		else
			delta = wlData_.confData_(0, wlData_.loops_[n])
					- wlData_.confData_(M_ - 1, n);
		ek += delta.sumOfSqures();

	}
	return ek;
}

void WorldLines::initializeRandom(MTGen* rndEng) {
	UniformReal pos;
	pos.init(rndEng, UniformReal::param_type(-.5, .5));
	for (int n = 0; n < numP_; n++) {
		ParticlePos loc;
		for (ParticlePos::iterator it = loc.begin(); it != loc.end(); it++)
			*it = pos.draw();
		for (int m = 0; m < M_; m++)
			wlData_.confData_(m, n) = loc;

	}
}
void WorldLines::initializeSolidBCC(ofstream &outFile) {
	int n;
	double dx = 1 / (pow(double(numP_) / 2., 1 / double(SIM_DIM)));
	int np = round((pow(double(numP_) / 2., 1 / double(SIM_DIM))));
	double z_shift, shift = 0;
	vector<int> pos_int(SIM_DIM);
	ParticlePos pos;
	n = 0;
	for (pos_int[2] = 0; pos_int[2] < np * 2; pos_int[2]++) {
		pos[2] = -0.5 + dx * 0.25 + dx * 0.5 * (double(pos_int[2])) + shift;
		z_shift = (pos_int[2] % 2) ? 0.75 : 0.25;
		for (pos_int[0] = 0; pos_int[0] < np; pos_int[0]++) {
			pos[0] = -0.5 + dx * (double(pos_int[0]) + z_shift) + shift;
			for (pos_int[1] = 0; pos_int[1] < np; pos_int[1]++) {
				pos[1] = -0.5 + dx * (double(pos_int[1]) + z_shift) + shift;
				pos.cyclic();
				assert(n < numP_);
				for (int m = 0; m < M_; m++)
					wlData_.confData_(m, n) = pos;
				n++;
			}
		}
	}
	outFile << "bcc";

}
#ifdef PYTHON

boost::python::dict WorldLines::toPython() const {
	boost::python::dict wl;
	wl["force_data"] = wlForce_.toPython();
	wl["wl_data"] = wlData_.toPython();
	wl["energy"] = wlEnergy_.toPython();
//	cout << "Worm Base" << wlData_.confData_( wlData_.wormSlice_, wlData_.wormNum_) << endl;
//	cout << "Worm Jump" << wlData_.jumpPos_ << endl;
	return wl;
}

#endif

//void WorldLinesData::calcNearestNeighbors(boost::multi_array<int, 3> &nnIndices,
//		int numOfnn) {
//	double dists[NumP_][NumP_];
//	int indexTemp[NumP_];
//	double diff[Dim_];
//	DoublePointerVec dimP;
//	BeadPos pos1, pos2, delta;
//	int p;
//	for (int m = 0; m < M_; m++) {
//		for (int n1 = 0; n1 < NumP_; n1++) {
//			pos1 = this->getDataPos(n1, m);
//			for (int n2 = n1 + 1; n2 < NumP_; n2++) {
//				pos2 = this->getDataPos(n2, m);
//				pos1.subBeadPos(delta, pos2);
//				dists[n1][n2] = dists[n2][n1] = sqrt(delta.sumOfSqures());
//				//			std::cout << dists[n1][n2] << std::endl;
//			}
//			dists[n1][n1] = 0;
//		}
//		for (int n1 = 0; n1 < NumP_; n1++) {
//			ippsSortIndexAscend_64f_I(dists[n1], indexTemp, NumP_);
//			for (int i = 0; i < numOfnn; i++) {
//				nnIndices[m][n1][i] = indexTemp[i + 1];
//				p = nnIndices[m][n1][i];
//			}
//		}
//	}
//}
