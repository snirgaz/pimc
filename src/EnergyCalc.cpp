/*
 * energy_cala.cpp
 *
 *  Created on: Feb 17, 2010
 *      Author: snirgaz
 */

#include "EnergyCalc.h"

void EnergyCalc::init(QmcParameters *qmcParameters, MoveParams *moveParams,
		WorldLines *wl) {
	qmcParameters_ = qmcParameters, wl_ = wl;
	moveParams_ = moveParams;
#ifdef CENTER_OF_MASS_MOVE
	resFProposed_.resize(qmcParameters->M.getVal(), wl_->numP_);
	resFOriginal_.resize(qmcParameters->M.getVal(), wl_->numP_);
#else
	resFProposed_.resize(qmcParameters->mbar.getVal(), wl_->numP_);
	resFOriginal_.resize(qmcParameters->mbar.getVal(), wl_->numP_);
#endif
	resFProposedJump_.resize(wl_->numP_);
	resFOriginalJump_.resize(wl_->numP_);
#ifdef POT_AZIZ
	rm_ = qmcParameters->rm.getVal();
#endif
	double eps = qmcParameters->epsilon.getVal();
#ifdef FORCE
	greenFac_ = (2. / 3.) * eps;
	forceFac_ = (pow(eps, 3) / (9.)) * 2 * qmcParameters->lambda.getVal();
#else
	greenFac_ = eps;
	forceFac_ = 0.;
#endif
	numP_ = wl_->numP_;
	M_ = wl_->M_;
}

EnergyData EnergyCalc::calcBeadsEnergy() {
#ifdef POT_FREE
	EnergyData ED;
	return ED;
#endif
	EnergyData diffEnergy;
	energyOriginal_.zero();
	energyProposed_.zero();
#ifdef EXT_POT
	this->calcExtPot(currentBeads);
#endif
	double totalO = 0., totalP = 0.;
	int nbeads = moveParams_->calcBeads.size();
#pragma omp parallel for reduction(+:totalO,totalP)
	for (int i = 0; i < nbeads; i++) {
		list<CalcBead>::iterator it_cb = moveParams_->calcBeads.begin();
		advance(it_cb, i);
		CalcBead &calcBead = *it_cb;
		if (calcBead.op == PROPOSED) {
			totalP += energyCalcAllSlice(calcBead);
		} else {
			assert(calcBead.op==ORIGINAL);
			totalO += energyCalcAllSlice(calcBead);
		}
	}
	energyOriginal_.energyV_ = totalO;
	energyProposed_.energyV_ = totalP;

#ifdef FORCE
	calcTotalFallSlices();
#endif
	energyOriginal_.calcEnergy(greenFac_, forceFac_);
	energyProposed_.calcEnergy(greenFac_, forceFac_);
	diffEnergy = energyProposed_.diff(energyOriginal_);
	return diffEnergy;
}
void EnergyCalc::energyCalcSingleTimeSlice(ConfSlice const & wlTimeSlice,
		ParticlePos const & testParticlePos, int testParticleN, bool isOdd,
		ForceSlice & forceTimeSlice, double &totalv) {
	double f, v;
	totalv = 0;
	ConfSlice::const_iterator itConf = wlTimeSlice.begin();
	ForceSlice::iterator itforce = forceTimeSlice.begin();
	assert(wlTimeSlice.size() == forceTimeSlice.size());
	int pos = 0;
	for (; itConf != wlTimeSlice.end(); itConf++, itforce++, pos++) {
		if (pos == testParticleN) {
			continue;
		}
		ParticlePos diff = (*itConf) - testParticlePos;
		if (isOdd) {
			calcGreenForce(diff, v, f);
			*itforce = f * diff;
		} else {
			calcGreen(diff, v);
		}
		totalv += v;
	}
}

double EnergyCalc::energyCalcAllSlice(CalcBead &calcBead) {
	// Find Tau Bead;
	Bead &cBead = moveParams_->beads[calcBead.m];
	double totalv;
	// Get WL time Slice
	const ConfSlice wlConfSlice = wl_->wlData_.confData_(cBead.WLSlice_,
			Range::all());
	if (calcBead.op == PROPOSED) {
		if (calcBead.jump) {
			// Jump Proposed
			double jumpv;
			// Jump Proposed
			energyCalcSingleTimeSlice(wlConfSlice, cBead.jumpPos_, cBead.n_,
					cBead.isOdd, resFProposedJump_, jumpv);
			totalv = .5 * jumpv;
		} else {
			// Regular Proposed
			ForceSlice wlforceSlice = resFProposed_(cBead.RWSlice_,
					Range::all());
			energyCalcSingleTimeSlice(wlConfSlice, cBead.beadPos_, cBead.n_,
					cBead.isOdd, wlforceSlice, totalv);
			if (cBead.proposedJump_)
				totalv = .5 * totalv;
		}
	} else {
		assert(calcBead.op == ORIGINAL);
		if (calcBead.jump) {
			double jumpv;
			energyCalcSingleTimeSlice(wlConfSlice, wl_->wlData_.jumpPos_,
					cBead.n_, cBead.isOdd, resFOriginalJump_, jumpv);
			totalv = .5 * jumpv;
		} else {
			// Regular Original
			ForceSlice wlforceSlice = resFOriginal_(cBead.RWSlice_,
					Range::all());
			energyCalcSingleTimeSlice(wlConfSlice, wlConfSlice(cBead.n_),
					cBead.n_, cBead.isOdd, wlforceSlice, totalv);
			if (cBead.originalJump_)
				totalv = .5 * totalv;

		}
	}
#ifdef FORCE
	if (cBead.isOdd)
		totalv *= 2.;
#endif
	return totalv;
}
double EnergyCalc::calcTotalFSingleSlice(ForceSlice const &allO,
		ForceSlice &allP, ForceSlice const &singleO, ForceSlice const & singleP,
		int particleNum) {
	assert(numP_ == wl_->numP_);
	double sumP = 0;
	allP(particleNum).fill(0.);
	for (int n = 0; n < numP_; n++) {
		if (n == particleNum)
			continue;
		allP(n) = allO(n) - singleO(n) + singleP(n);
		allP(particleNum) -= singleP(n);
		sumP += allP(n).sumOfSqures();
	}
	sumP += allP(particleNum).sumOfSqures();
	return sumP;
}
void EnergyCalc::calcTotalFallSlices() {
	Range all = Range::all();
	double sumO = 0, sumP = 0, currentSumP, currentSumO, jumpP, jumpO;
//	// Total Sum Of Force of Slice
//	sumO = WLForce::getTotalForceJump();
//	// Pointer to Original forces of a given Slice
//	WLForce::getJumpSliceP (allO);
// Pointer to Propo
	for (list<int>::iterator itb = moveParams_->currentBeads.begin();
			itb != moveParams_->currentBeads.end(); itb++) {
		Bead &cBead = moveParams_->beads[*itb];
		// Continue if not an off bead
		if (!cBead.isOdd)
			continue;
		currentSumP = 0, currentSumO = 0;
		// Total Sum Of Force of Slice
		currentSumO = wl_->wlForce_.totalForce_[cBead.WLSlice_];
		// Pointer to Original forces of a given Slice
		ForceSlice allO = wl_->wlForce_.forceData_(cBead.WLSlice_, all);
		// Pointer to Proposed forces of a given Slice
		ForceSlice allP = moveParams_->forecUpdate.forceData_(cBead.RWSlice_,
				all);
		ForceSlice singleO = resFOriginal_(cBead.RWSlice_, all);
		ForceSlice singleP = resFProposed_(cBead.RWSlice_, all);
		// Calc Total Force
		currentSumP = calcTotalFSingleSlice(allO, allP, singleO, singleP,
				cBead.n_);
		moveParams_->forecUpdate.totalForce_[cBead.RWSlice_] = currentSumP;
#ifdef EXT_POT
		currentSumP = calcSliceForce(allO, allP, singleO, singleP,
				extPotFProposed_[itb->RWSlice_], itb->n_, QmcParams::NumP);
#elif defined(FIXED)
		currentSumP = calcSliceForce(allO, allP, singleO, singleP, itb->n_,
				QmcParams::NumPMoving, QmcParams::NumP);
#else
#endif

		if (cBead.proposedJump_) {
			assert(moveParams_->moveType != 1);
			// WLForce::getJumpSliceP(allO);
			ForceSlice allP = moveParams_->forecUpdate.jumpForceData_;
			jumpP = calcTotalFSingleSlice(allO, allP, singleO,
					resFProposedJump_, cBead.n_);
			moveParams_->forecUpdate.totalForceJump_ = jumpP;
			currentSumP = 0.5 * (jumpP + currentSumP);
		}
		if (cBead.originalJump_) {
			assert(wl_->wlData_.isOpenClosed_ == OPEN);
			jumpO = wl_->wlForce_.totalForceJump_;
			currentSumO = 0.5 * (jumpO + currentSumO);
		}
		sumP += currentSumP;
		sumO += currentSumO;
	}
	energyProposed_.setEnergyF(sumP);
	energyOriginal_.setEnergyF(sumO);
}
EnergyData EnergyCalc::calcEnergySwap(int originalWL, int swapToWL) {
#ifdef POT_FREE
	EnergyData ED;
	return ED;
#endif
	EnergyData energyDiff;
	energyOriginal_.zero();
	energyProposed_.zero();
///
/// Original
///

// Get WL time Slice
	const ConfSlice wlConfSlice = wl_->wlData_.confData_(moveParams_->mStart,
			Range::all());
// Calc for original
	bool isOdd = moveParams_->mStart % 2;
	double totalvo, totalvp;
#pragma omp parallel sections
	{
#pragma omp section
		{
			energyCalcSingleTimeSliceSwap(wlConfSlice, wlConfSlice(swapToWL),
					wl_->wlData_.jumpPos_, originalWL, swapToWL, isOdd,
					resFOriginalJump_, totalvo);
#ifdef FORCE
			if (isOdd)
				totalvo *= 2.;
#endif
			energyOriginal_.energyV_ += .5 * totalvo;
		}
#pragma omp section
		{
// Calc For Proposed
			energyCalcSingleTimeSliceSwap(wlConfSlice, wlConfSlice(originalWL),
					wl_->wlData_.jumpPos_, originalWL, swapToWL, isOdd,
					resFProposedJump_, totalvp);
#ifdef FORCE
			if (isOdd)
				totalvp *= 2.;
#endif
			energyProposed_.energyV_ += .5 * totalvp;
		}
	}
#ifdef FORCE
	if (isOdd)
		this->calcTotalFSwap(originalWL, swapToWL);
#endif
	energyOriginal_.calcEnergy(greenFac_, forceFac_);
	energyProposed_.calcEnergy(greenFac_, forceFac_);
// Jump Mul by .5
	energyDiff = (energyProposed_.diff(energyOriginal_));
	return energyDiff;

}

void EnergyCalc::energyCalcSingleTimeSliceSwap(const ConfSlice& wlTimeSlice,
		const ParticlePos& testParticlePos, const ParticlePos& jumpParticlePos,
		int originalWL, int swapToWL, bool isOdd, ForceSlice& forceTimeSlice,
		double& totalv) {
	double f, v;
	totalv = 0.;
	ConfSlice::const_iterator itConf = wlTimeSlice.begin();
	ForceSlice::iterator itforce = forceTimeSlice.begin();
	int n = 0;
	ParticlePos diff;
	for (; itConf != wlTimeSlice.end(); itConf++, n++) {
		// Self Interaction
		if (n == swapToWL) {
			itforce++;
			continue;
		}
		// Interaction with jump pos
		if (n == originalWL)
			diff = jumpParticlePos - testParticlePos;
		else
			diff = (*itConf) - testParticlePos;
		if (isOdd) {
			calcGreenForce(diff, v, f);
			*itforce = f * diff;
			itforce++;
		} else {
			calcGreen(diff, v);
		}
		totalv += v;
	}
}

void EnergyCalc::calcTotalFSwap(int wlOriginal, int wlSwapTo) {
	double sumP = 0.;
// Original forces of Swap/Jump Slice
	ForceSlice &allO = wl_->wlForce_.jumpForceData_;
// Proposed forces of Swap/Jump Slice
	ForceSlice &allP = moveParams_->forecUpdate.jumpForceData_;
	ForceSlice &singleO = resFOriginalJump_;
	ForceSlice &singleP = resFProposedJump_;
	(allP(wlSwapTo)).fill(0.);
	for (int n = 0; n < numP_; n++) {
		// Ref particle for forces
		if (n == wlSwapTo)
			continue;
		allP(n) = allO(n) - singleO(n) + singleP(n);
		allP(wlSwapTo) -= singleP(n);
		sumP += allP(n).sumOfSqures();
	}
	sumP += allP(wlSwapTo).sumOfSqures();
	moveParams_->forecUpdate.totalForceJump_ = sumP;
// Swap force data Check!!!!
	swap(allP(wlSwapTo), allP(wlOriginal));
//
	energyOriginal_.setEnergyF(.5 * wl_->wlForce_.totalForceJump_);
	energyProposed_.setEnergyF(.5 * sumP);
}

EnergyData EnergyCalc::calcConfigurationEnergy(bool update = false) {
	EnergyData ED;
	WorldLinesForce force;
	calcConfigurationEnergy(update, ED, force);
	return ED;
}
void EnergyCalc::calcConfigurationEnergy(bool update, EnergyData &ED,
		WorldLinesForce &force) {
	force.init(wl_->M_, wl_->numP_);
	vector<double> totalv(wl_->M_);
	fill(totalv.begin(), totalv.end(), 0.);
	for (int m = 0; m < wl_->M_; m++) {
		bool odd = m % 2;
		for (int n = 0; n < wl_->numP_; n++) {
			Force &cf = force.forceData_(m, n);
			for (int np = 0; np < wl_->numP_; np++) {
				if (n == np)
					continue;
				double v, f, jv, jf;
				ParticlePos diff = wl_->wlData_.confData_(m, n)
						- wl_->wlData_.confData_(m, np);
				if (odd) {
					calcGreenForce(diff, v, f);
					cf += f * diff;
#ifdef FORCE
					v = 2. * v;
#endif
				} else {
					calcGreen(diff, v);
				}
				totalv[m] += v;
			}
		}
	}
	bool jump = (wl_->wlData_.isOpenClosed_ == OPEN);
	if (jump) {
		int m = wl_->wlData_.wormSlice_;
		bool odd = m % 2;
		ParticlePos diff;
		double jv, jf, totaljv = 0.;
		for (int n = 0; n < wl_->numP_; n++) {
			//Force &cjf = force.jumpForceData_(n);
			for (int np = 0; np < wl_->numP_; np++) {
				if (n != np) {
					if (n == wl_->wlData_.wormNum_)
						diff = wl_->wlData_.jumpPos_
								- wl_->wlData_.confData_(m, np);
					else if (np == wl_->wlData_.wormNum_)
						diff = wl_->wlData_.confData_(m, n)
								- wl_->wlData_.jumpPos_;
					else
						diff = wl_->wlData_.confData_(m, n)
								- wl_->wlData_.confData_(m, np);
					if (odd) {
						calcGreenForce(diff, jv, jf);
						force.jumpForceData_(n) += jf * diff;
#ifdef FORCE
						jv = 2. * jv;
#endif
					} else {
						calcGreen(diff, jv);
					}
					totaljv += jv;
				}
			}
		}
		totalv[m] = .5 * (totalv[m] + totaljv);
	}
// Sum Over Force
	double totalf = 0.;
	for (int m = 0; m < wl_->M_; m++) {
		bool odd = m % 2;
		if (odd) {
			bool jump = ((m == wl_->wlData_.wormSlice_)
					and (wl_->wlData_.isOpenClosed_ == OPEN));
			for (int n = 0; n < wl_->numP_; n++)
				force.totalForce_[m] += force.forceData_(m, n).sumOfSqures();
			if (jump) {
				for (int n = 0; n < wl_->numP_; n++)
					force.totalForceJump_ +=
							force.jumpForceData_(n).sumOfSqures();
				totalf += 0.5 * (force.totalForce_[m] + force.totalForceJump_);
			} else {
				totalf += force.totalForce_[m];
			}
		}
	}
	ED.energyF_ = totalf;
	ED.energyV_ = .5 * accumulate(totalv.begin(), totalv.end(), 0.);
	ED.calcEnergy(greenFac_, forceFac_);
	if (update) {
		wl_->wlEnergy_.energyF_ = ED.energyF_;
		wl_->wlEnergy_.energyV_ = ED.energyV_;
		wl_->wlEnergy_.energy_ = ED.energy_;
		wl_->wlForce_ = force;
	}
}
#ifdef CENTER_OF_MASS_MOVE
EnergyData EnergyCalc::calcEnergyCOM(int moveWL, ParticlePos &dr) {
#ifdef POT_FREE
	EnergyData ED;
	return ED;
#endif
	EnergyData diffEnergy;
	energyOriginal_.zero();
	energyProposed_.zero();
#ifdef EXT_POT
	this->calcExtPot(currentBeads);
#endif
	double totalO = 0., totalP = 0.;
	int nbeads = M_ * 2;
#pragma omp parallel for reduction(+:totalO,totalP)

	for (int i = 0; i < nbeads; i++) {
		int pos = i / 2;
		if (i % 2)
			// Calc Energy for the Original Path
			totalO += calcEnergyCOMOriginalSlice(moveWL, pos);
		else
			// Calc Energy for the Proposed Path
			totalP += calcEnergyCOMProposedSlice(moveWL, dr, pos);
	}

	energyOriginal_.energyV_ = totalO;
	energyProposed_.energyV_ = totalP;

#ifdef FORCE
	calcEnergyCOMFAll(moveWL);
#endif
	energyOriginal_.calcEnergy(greenFac_, forceFac_);
	energyProposed_.calcEnergy(greenFac_, forceFac_);
	diffEnergy = energyProposed_.diff(energyOriginal_);
	return diffEnergy;
}
double EnergyCalc::calcEnergyCOMOriginalSlice(int moveWL, int m) {
	double f, v;
	double totalv = 0., totalvjump = 0.;
	bool isJump = (wl_->wlData_.wormSlice_ == m)
			and (wl_->wlData_.isOpenClosed_ == OPEN);
	bool isOdd = m % 2;
	ConfData &confdata = wl_->wlData_.confData_;
	ForceData &forcedata = resFOriginal_;
	ParticlePos testParticle, testParticleJump;
	ParticlePos diff, diffJump;
	testParticle = confdata(m, moveWL);
	if (isJump)
		testParticleJump = wl_->wlData_.jumpPos_;
	for (int n = 0; n < numP_; n++) {
		if (n == moveWL)
			continue;
		diff = confdata(m, n) - testParticle;
		if (isOdd) {
			calcGreenForce(diff, v, f);
			forcedata(m, n) = f * diff;
		} else {
			calcGreen(diff, v);
		}
		totalv += v;
		if (isJump) {
			if (n == wl_->wlData_.wormNum_)
				diffJump = wl_->wlData_.jumpPos_ - testParticle;
			else if (moveWL == wl_->wlData_.wormNum_)
				diffJump = confdata(m, n) - testParticleJump;
			else
				diffJump = confdata(m, n) - testParticle;
			if (isOdd) {
				calcGreenForce(diffJump, v, f);
				resFOriginalJump_(n) = f * diffJump;
			} else {
				calcGreen(diffJump, v);
			}
			totalvjump += v;
		}
	}
	if (isJump)
		totalv = .5 * (totalv + totalvjump);
#ifdef FORCE
	if (isOdd)
		totalv *= 2.;
#endif
	return totalv;
}

double EnergyCalc::calcEnergyCOMProposedSlice(int moveWL, ParticlePos &dr,
		int m) {
	double f, v;
	double totalv = 0., totalvjump = 0.;
	bool isJump = (wl_->wlData_.wormSlice_ == m)
			and (wl_->wlData_.isOpenClosed_ == OPEN);
	bool isOdd = m % 2;
	ConfData &confdata = wl_->wlData_.confData_;
	ForceData &forcedata = resFProposed_;
	ParticlePos testParticle, testParticleJump;
	ParticlePos diff, diffJump;
	testParticle = confdata(m, moveWL) + dr;
	if (isJump)
		testParticleJump = wl_->wlData_.jumpPos_ + dr;
	for (int n = 0; n < numP_; n++) {
		if (n == moveWL)
			continue;
		diff = confdata(m, n) - testParticle;
		if (isOdd) {
			calcGreenForce(diff, v, f);
			forcedata(m, n) = f * diff;
		} else {
			calcGreen(diff, v);
		}
		totalv += v;
		if (isJump) {
			if (n == wl_->wlData_.wormNum_)
				diffJump = wl_->wlData_.jumpPos_ - testParticle;
			else if (moveWL == wl_->wlData_.wormNum_)
				diffJump = confdata(m, n) - testParticleJump;
			else
				diffJump = confdata(m, n) - testParticle;
			if (isOdd) {
				calcGreenForce(diffJump, v, f);
				resFProposedJump_(n) = f * diffJump;
			} else {
				calcGreen(diffJump, v);
			}
			totalvjump += v;
		}
	}
	if (isJump)
		totalv = .5 * (totalv + totalvjump);
#ifdef FORCE
	if (isOdd)
		totalv *= 2.;
#endif
	return totalv;
}

void EnergyCalc::calcEnergyCOMFAll(int moveWL) {
	Range all = Range::all();
	double sumO = 0, sumP = 0, currentSumP, currentSumO, jumpP, jumpO;
//	// Total Sum Of Force of Slice
//	sumO = WLForce::getTotalForceJump();
//	// Pointer to Original forces of a given Slice
//	WLForce::getJumpSliceP (allO);
// Pointer to Propo
	for (int m = 0; m <= M_; m++) {
		// Continue if not an off bead
		if ((m % 2) == 0)
			continue;
		currentSumP = 0, currentSumO = 0;
		// Total Sum Of Force of Slice
		currentSumO = wl_->wlForce_.totalForce_[m];
		// Pointer to Original forces of a given Slice
		ForceSlice allO = wl_->wlForce_.forceData_(m, all);
		// Pointer to Proposed forces of a given Slice
		ForceSlice allP = moveParams_->forecUpdate.forceData_(m, all);
		ForceSlice singleO = resFOriginal_(m, all);
		ForceSlice singleP = resFProposed_(m, all);
		// Calc Total Force
		currentSumP = calcTotalFSingleSlice(allO, allP, singleO, singleP,
				moveWL);
		moveParams_->forecUpdate.totalForce_[m] = currentSumP;
#ifdef EXT_POT
		currentSumP = calcSliceForce(allO, allP, singleO, singleP,
				extPotFProposed_[itb->RWSlice_], itb->n_, QmcParams::NumP);
#elif defined(FIXED)
		currentSumP = calcSliceForce(allO, allP, singleO, singleP, itb->n_,
				QmcParams::NumPMoving, QmcParams::NumP);
#else
#endif

		if (m == wl_->wlData_.wormSlice_) {
			assert(moveParams_->moveType != 1);
			// WLForce::getJumpSliceP(allO);
			ForceSlice allP = moveParams_->forecUpdate.jumpForceData_;
			ForceSlice allO = wl_->wlForce_.jumpForceData_;
			jumpP = calcTotalFSingleSlice(allO, allP, resFOriginalJump_,
					resFProposedJump_, moveWL);
			moveParams_->forecUpdate.totalForceJump_ = jumpP;
			currentSumP = 0.5 * (jumpP + currentSumP);
			assert(wl_->wlData_.isOpenClosed_ == OPEN);
			jumpO = wl_->wlForce_.totalForceJump_;
			currentSumO = 0.5 * (jumpO + currentSumO);
		}
		sumP += currentSumP;
		sumO += currentSumO;
	}
	energyProposed_.setEnergyF(sumP);
	energyOriginal_.setEnergyF(sumO);
}
#endif

#ifdef POT_AZIZ
void EnergyCalc::calcGreenForce(ParticlePos const & drPos, double &v,
		double &f) {
	const double D = 1.241314;
	const double A = 0.5448504E6;
	const double EPS = 10.8;
	const double ALPHA = 13.353384;
	const double C[3] = { 1.3732412, 0.4253785, 0.1781 };
	const double Cd[3] = { 1.3732412 * 6, 0.4253785 * 8, 0.1781 * 10 };
	double h, p, dp, dh, drsqr, dr_minus_ten, dr_minus_eleven, exp_arg, drS, dr;
	dr = sqrt(drPos.sumOfSqures());
	drS = dr / rm_;
//	if (drS > drTh) {
//		v = 0;
//		f = 0;
//	} else {
	dr_minus_ten = pow(drS, -10);
	dr_minus_eleven = dr_minus_ten / drS;
	drsqr = drS * drS;
	p = dr_minus_ten * (C[2] + drsqr * (C[1] + C[0] * drsqr));
	dp = -dr_minus_eleven * (Cd[2] + drsqr * (Cd[1] + Cd[0] * drsqr));
	v = EPS * (A * exp(-ALPHA * drS));
	f = EPS * (-ALPHA * A * exp(-ALPHA * drS));
	if (drS > D) {
		v -= EPS * p;
		f -= EPS * dp;
	} else {
		exp_arg = (D / drS - 1);
		h = exp(-pow(exp_arg, 2));
		dh = 2 * D * h * exp_arg / (drsqr);
		v -= h * EPS * p;
		f -= EPS * (dh * p + h * dp);
	}
	f /= (dr * rm_);
//}
}
void EnergyCalc::calcGreen(ParticlePos const & drPos, double &v) {
	const double D = 1.241314;
	const double A = 0.5448504E6;
	const double EPS = 10.8;
	const double ALPHA = 13.353384;
	const double C[3] = { 1.3732412, 0.4253785, 0.1781 };
	double h, p, drsqr, dr_minus_ten, exp_arg, drS, dr;
	dr = sqrt(drPos.sumOfSqures());
	drS = dr / rm_;
//	if (drS > drTh)
//	v = 0;
//	else {
	dr_minus_ten = pow(drS, -10);
	drsqr = drS * drS;
	p = dr_minus_ten * (C[2] + drsqr * (C[1] + C[0] * drsqr));
	v = EPS * (A * exp(-ALPHA * drS));
	if (drS > D) {
		v -= EPS * p;
	} else {
		exp_arg = (D / drS - 1);
		h = exp(-pow(exp_arg, 2));
		v -= h * EPS * p;
	}
//	}
}

#endif
