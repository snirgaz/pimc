/*
 * random_walks.h
 *
 *  Created on: Jan 21, 2010
 *      Author: snirgaz
 */

#ifndef RANDOM_WALKS_H_
#define RANDOM_WALKS_H_

#include "HelpLibs/def.h"
#include "MoveParams.h"
#include "WorldLines.h"
#include "HelpLibs/QmcParameters.h"
#include "HelpLibs/RandomGen.h"
#include "SimParams.h"
#include "ParticlePos.h"


class RandomWalks {
private:
	vector<double> preCalcedSigmas_;
	MoveParams *moveParams_;
	Normal normal_;
public:
	void init(int m,SimulationParameters *simParams,SimGen *rngGen,MoveParams *moveParams);
	void generateLevelPath(MoveParams *moveParams);
	void generateJumpBeadPos(BeadVec::const_iterator const  &beadStart, BeadVec::const_iterator const &beadEnd,
			BeadVec::iterator &beadTau);
	void updateVar(double var);
	double drawNormal();
};

#endif /* RANDOM_WALKS_H_ */
