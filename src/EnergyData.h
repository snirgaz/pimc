//
//  EnergyData.h
//  WORM_AL_PT
//
//  Created by Snir Gazit on 8/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifndef WORM_AL_PT_EnergyData_h
#define WORM_AL_PT_EnergyData_h
#include "HelpLibs/def.h"
#include "HelpLibs/hdf5RW.h"
#include <string>
using namespace std;
struct EnergyData {
	// Total energy
	double energy_;
	// Green Energy
	double energyV_;
	// Force Energy
	double energyF_;
	EnergyData();
	void update(EnergyData ED);
	void zero();
	EnergyData mul(double c);
	EnergyData diff(EnergyData ED);
	double getEnergy() const;
	double getEnergyV() const;
	double getEnergyF() const;
	void setEnergy(double energy);
	void setEnergyV(double energy);
	void setEnergyF(double energy);
	void calcEnergy(double constV, double constF);
	void swapData(EnergyData &ED);
	void dumpToFile(string &fileName);
	void readFromFile(string &fileName);
#ifdef PYTHON
	boost::python::dict toPython() const;
#endif
};

struct WLEnergyData: EnergyData {
	double energyK_;
	WLEnergyData();
	double getEnergyK() const;
	void setEnergyK(double energy);
	void updateEnergyK(double energy);
	void swapData(WLEnergyData &ED);
	void setEnergyP(EnergyData &ED);
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(string &filename);
#ifdef PYTHON
	boost::python::dict toPython() const;
#endif
};

#endif
