/*
 * meausre.h
 *
 *  Created on: Apr 20, 2010
 *      Author: snirgaz
 */

#ifndef MEAUSRE_H_
#define MEAUSRE_H_

#include "HelpLibs/def.h"
#include "HelpLibs/fftwPlan.h"
#include "WorldLines.h"
#include "HelpLibs/RandomGen.h"
#include "HelpLibs/ArrayMultiD.h"
#include "HelpLibs/QmcParameters.h"
#include "SimParams.h"
#include "HelpLibs/Observable.h"
#include <algorithm>
#include <complex>
#include "boost/math/constants/constants.hpp"

typedef std::chrono::system_clock::time_point TimePoint;
typedef std::chrono::duration<double, std::ratio<3600> > Hour;
void autocoor(const Array<double,1> &v1,const Array<double,1> &v2,Array<double,1> &res);

enum MeasureType {
	OPEN_MEASURE, CLOSED_MEASURE
};

class ProgressBar {
	int totalNumberOfMeasure_;
	ofstream *outFile_;
	double simTime_;
	TimePoint startTime_, lastTime_;
	Hour deltaDump_;
	int moves_, deltamoves_;
	SimulationParameters *simParams_;
public:
	void init(int totalNumberOfMeasure, int minNumOfMeasure, double simTime,
			ofstream *outFile, QmcParameters qmcParameters,
			SimulationParameters *simParams);
	void update(int numOfMeasure, bool &endTime, bool &dumpFlag);
	void updateMinNumOfMeasure(int minNumOfMeasure);
	int getTotalNumberOfMeasure();
	double getTime() const;
};

class Measure {
protected:
	enum MeasureType measureType_;
	WorldLines *wl_;
public:
	Measure();
	virtual void DoMeasurement()=0;
	virtual bool checkDone() const =0;
	virtual void dumpToFile(hdf5RW &hdf5File) const=0;
	virtual int getNumOfMeasure() const=0;
	virtual void readFromFile(hdf5RW &hdf5File)=0;
	MeasureType getMeasureType() const;
	virtual ~Measure();
};

class EkMeasure: public Measure {
	typedef Observable<ScalarObs<double> > Obs;
private:
	Obs ek_;
	double ratio_, shift_, beta_;
public:
	EkMeasure(WorldLines *wl, QmcParameters &qmcParameters);
	~EkMeasure();
	void DoMeasurement();
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(hdf5RW &hdf5File);
	bool checkDone() const;
	int getNumOfMeasure() const;
};
class GreenMeasure: public Measure {
	typedef ArrayObservable<Observable<ScalarObs<double> >, SIM_DIM> ObsArray;
	typedef ArrayObservable<Observable<ScalarObs<double> >, SIM_DIM>::IterIndex ObsArrayIterIndex;
private:
	ArrayObservable<Observable<ScalarObs<double> >, SIM_DIM> green_;
public:
	GreenMeasure(WorldLines *wl, QmcParameters &qmcParameters);
	~GreenMeasure();
	void DoMeasurement();
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(hdf5RW &hdf5File);
	bool checkDone() const;
	int getNumOfMeasure() const;
};
class Winding: public Measure {
	typedef Observable<ScalarObs<double> > Obs;
private:
	Obs wind_;
public:
	Winding(WorldLines *wl, QmcParameters &qmcParameters);
	~Winding();
	void DoMeasurement();
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(hdf5RW &hdf5File);
	bool checkDone() const;
	int getNumOfMeasure() const;
};

class EnergyMeasure: public Measure {
	typedef Observable<ScalarObs<double> > Obs;
private:
	Obs ev_, ef_, e_, ek_, ep_;
	double av_, eps_, lambda_, ratiof_;
public:
	EnergyMeasure(WorldLines *wl, QmcParameters &qmcParameters);
	~EnergyMeasure();
	void DoMeasurement();
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(hdf5RW &hdf5File);
	bool checkDone() const;
	int getNumOfMeasure() const;
};

// Equal time density Density
class DensityDensityETMeasure: public Measure {
	typedef ArrayObservable<Observable<ScalarObs<double> >, SIM_DIM> ObsArray;
	typedef ArrayObservable<Observable<ScalarObs<double> >, SIM_DIM>::IterIndex ObsArrayIterIndex;
private:
	ObsArray sk_;
public:
	DensityDensityETMeasure(WorldLines *wl, QmcParameters &qmcParameters);
	~DensityDensityETMeasure();
	void DoMeasurement();
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(hdf5RW &hdf5File);
	bool checkDone() const;
	int getNumOfMeasure() const;
};

// Dynamical density Density
class DensityDensityDynamicalMeasure: public Measure {
	typedef ArrayObservable<Observable<VectorObs<double> >, 1> ObsArray;
	typedef ArrayObservable<Observable<VectorObs<double> >, 1>::iterator ObsArrayIter;
private:
	vector<PhononBranch> pb_;
	fftwPlan fftc2c_, fftr2c_;
	// 1 - \Delta tau 2- 2 \times \Delta \tau due to chin potential
	int dtRatio_,numOfSlices_;
public:
	DensityDensityDynamicalMeasure(WorldLines *wl,
			QmcParameters &qmcParameters);
	~DensityDensityDynamicalMeasure();
	void DoMeasurement();
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(hdf5RW &hdf5File);
	bool checkDone() const;
	int getNumOfMeasure() const;
};
// Four Point density Density
class FourPointDynamicalMeasure: public Measure {
	typedef array<int, SIM_DIM> KVec;
	typedef std::pair<int,int> BraggPair;
	typedef vector<BraggPair> BraggPairVec;
	typedef Observable<VectorObs<double> > ObsVec;
private:
	fftwPlan fft_;
	Array<complex<double>,2> weightG_;
	Array<double,2> weightGnorm_;
	Array<double,1> corr_,corrTemp_;
	ObsVec ortG1G2_,nonortG1G2_,same_;
	vector<KVec> BraggVecs_;
	BraggPairVec ortPairs_,nonOrtPairs_;
	int num_of_Bragg_;
public:
	FourPointDynamicalMeasure(WorldLines *wl,
			QmcParameters &qmcParameters);
	~FourPointDynamicalMeasure();
	void DoMeasurement();
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(hdf5RW &hdf5File);
	bool checkDone() const;
	int getNumOfMeasure() const;
};
// Dynamical density Density
class DensityDensitySquareDynamicalMeasure: public Measure {
	typedef ArrayObservable<Observable<VectorObs<double> >, 1> ObsArray;
	typedef ArrayObservable<Observable<VectorObs<double> >, 1>::iterator ObsArrayIter;

private:
	vector<PhononBranch> pb_;
	fftwPlan fft_;
public:
	DensityDensitySquareDynamicalMeasure(WorldLines *wl,
			QmcParameters &qmcParameters);
	~DensityDensitySquareDynamicalMeasure();
	void DoMeasurement();
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(hdf5RW &hdf5File);
	bool checkDone() const;
	int getNumOfMeasure() const;
};
class Measurements {
	typedef boost::ptr_vector<Measure> MesureVector;
	typedef MesureVector::iterator MesureVectorIter;
	//int numOfBins_;
private:
	MesureVector measures_;
	WorldLines *wl_;
	ProgressBar progBar_;
	int rateOpen_, rateClosed_;
	bool allDone_;
	ofstream *outFile_;
public:
	Measurements();
	~Measurements();
	void init(WorldLines *wl, QmcParameters &qmcParameters,
			SimulationParameters *simParams, ofstream *outFile);
	void DoMeasurement(bool &endTime, bool &dumpTime);
	bool checkNotDone() const;
	void dump(string outPath, bool final) const;
	void dump(string fileName) const;
	void read(string outPath);
	void updateMinNumOfMeasure();
	void updateClosedRate(int const closedRate);
	int getClosedRate() const;
	void updateOpenRate(int const openRate);
	int getOpenRate() const;
};

#endif /* MEAUSRE_H_ */
