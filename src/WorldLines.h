/*
 * worldlines.h
 *
 *  Created on: Jan 21, 2010
 *      Author: snirgaz
 */

#ifndef WORLDLINES_H_
#define WORLDLINES_H_


#ifdef PARALLEL_OPENMP
#define BZ_THREADSAFE
#endif

#include "HelpLibs/def.h"
//#include "xml_rw.h"
#include "ParticlePos.h"
#include "HelpLibs/hdf5RW.h"
#include "EnergyData.h"
#include "SimParams.h"
#include "HelpLibs/ArrayMultiD.h"
#include "HelpLibs/RandomGen.h"
#include <algorithm>
#include "blitz/array.h"

// Cyclic coordinate array class

using namespace blitz;

typedef Array<ParticlePos, 2> ConfData;
typedef Array<Force, 2> ForceData;

typedef Array<ParticlePos, 1> ConfSlice;
typedef Array<Force, 1> ForceSlice;

void cyclic_vec(double vec, int size);
bool isInRange(double *vec, int size, double lim);

struct WorldLinesForce {

	// Total |F|^2 Energy for each Slice
	vector<double> totalForce_;
	ForceSlice jumpForceData_;
	double totalForceJump_;
	ForceData forceData_;
	void init(int m, int n);
	void resize(int m, int n);
	double calcTotalForce(int m);
	double calcTotalForceJump();
	void dumpToFile(string fileName) const;
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(string fileName);
	void operator=(WorldLinesForce & wlForce);
#ifdef PYTHON
	boost::python::dict toPython() const;
#endif
};

struct WorldLinesWorm {
	ConfData confData_; // 2D array (slice , particle number)
	vector<int> loops_;
	// Particle number of worm jump
	int wormNum_;
	// Time Slice of Worm Jumo
	int wormSlice_;
	// Position of "jump" particle
	ParticlePos jumpPos_;
	// Delta of the jump
	ParticlePos delta_;
	// Open or Closed
	OC isOpenClosed_;
	void init(int M, int numP);
	void swapLoops(int n, int m);
	int findPreviousLoop(int n) const;
	double calcEKDiff(BeadVec const & beads) const;
	// IO
	void dumpToFile(string fileName) const;
	void dumpToFile(hdf5RW &hdf5File) const;
	void readFromFile(string FileName);
	void swapData(WorldLinesWorm &WL2);
#ifdef PYTHON
	boost::python::dict toPython() const;
#endif

};

struct WorldLines {
	WorldLinesWorm wlData_;
	WorldLinesForce wlForce_;
	WLEnergyData wlEnergy_;
	int numP_;
	int M_;
	void init(int m, int n);
	// Get "next" particle after beta
	// Numer of beads - NumP*M*dim
	void dumpToFile(string fileName) const;
	void readFromFile(string fileName);
	// Energy
	double calcEK();
	// Initialize
	void initializeRandom(MTGen *rndEng);
	void initializeSolid(string outPath);
	void initializeSolidFixed(string outPath);
	void initializeSolidBCC(ofstream &outFile);
	void initializeSolidHCP(string outPath, string latFileInit);
//	void initializeSolidBCCVacancies(int numOfVac, string outPath,
//			QmcRnd &qmcRnd);
//	void initializeSolidBCCInterstitials(int numOfInter, string outPath,
//			QmcRnd &qmcRnd);
	void initializeSolidBCCFixedGB(string outPath, string latFileInit,
			int &numOfFixed);
	void initializeSolidBCCFixedGBFromFile(string initPath, string outPath,
			int &numOfFixed);
	void initializeFromFile(string outPath);
	// Debugs
	bool loopCheck(int step, string out_put);bool isValidConf(string out_path);
	WLEnergyData& getEnergyData();
#ifdef PYTHON
	boost::python::dict toPython() const;
#endif
///	void updateMove(MoveParams const &moveParams,
//			WorldLinesForce const &forceUpdate, EnergyData const &energyDiff,
//			OC isOpenClosed);
//	void updateSwapMove(MoveParams const &moveParams,
//			WorldLinesForce const &forceUpdate, EnergyData energyDiff);
};


#endif /* WorldLINES_H_ */
