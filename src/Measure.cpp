///*
// * Measure.cpp
// *
// *  Created on: Apr 20, 2010
// *      Author: snirgaz
// */
//

#include "Measure.h"

// Measurement Class
Measure::Measure() {
	// TODO Auto-generated destructor stub
}
Measure::~Measure() {
	// TODO Auto-generated destructor stub
}

MeasureType Measure::getMeasureType() const {
	return measureType_;
}

Measurements::Measurements() {
	// TODO Auto-generated destructor stub
}
Measurements::~Measurements() {
	// TODO Auto-generated destructor stub
}
void Measurements::init(WorldLines *wl, QmcParameters &qmcParameters,
		SimulationParameters *simParams, ofstream *outFile) {
// Init Measure
	allDone_ = false;
	wl_ = wl;
	outFile_ = outFile;
	vector<string> mesVec = qmcParameters.toMeasure.getVal();
	for (vector<string>::iterator it_m = mesVec.begin(); it_m != mesVec.end();
			it_m++) {
//	if (toMeasure["SUSEPTIBILITY"])
//		measures_.push_back(
//				new SuseptibilityMeasure(lat, qmcParameters, boltzRatio));

		if ((*it_m) == "green")
			measures_.push_back(new GreenMeasure(wl_, qmcParameters));
		if ((*it_m) == "ek")
			measures_.push_back(new EkMeasure(wl_, qmcParameters));
		if ((*it_m) == "energy")
			measures_.push_back(new EnergyMeasure(wl_, qmcParameters));
		if ((*it_m) == "wind")
			measures_.push_back(new Winding(wl_, qmcParameters));
		if ((*it_m) == "sk")
			measures_.push_back(
					new DensityDensityETMeasure(wl_, qmcParameters));
		if ((*it_m) == "dyn_sk")
			measures_.push_back(
					new DensityDensityDynamicalMeasure(wl_, qmcParameters));
		if ((*it_m) == "dyn_sk_square")
			measures_.push_back(
					new DensityDensitySquareDynamicalMeasure(wl_,
							qmcParameters));
		if ((*it_m) == "four_point")
			measures_.push_back(
					new FourPointDynamicalMeasure(wl_, qmcParameters));

	}

	int totalNumberOfMeasure = (int) pow(
			(double) qmcParameters.binSize.getVal(),
			qmcParameters.numOfBins.getVal());
	rateClosed_ = qmcParameters.rateClosed.getVal();
	rateOpen_ = qmcParameters.rateOpen.getVal();
	progBar_.init(totalNumberOfMeasure, 0, qmcParameters.endTime.getVal(),
			outFile, qmcParameters, simParams);

}
void Measurements::DoMeasurement(bool &endTime, bool &dumpTime) {
	static int stepClosed = 0;
	static int stepOpen = 0;
	endTime = dumpTime = false;
	bool openflag = false, closedflag = false;
	if (wl_->wlData_.isOpenClosed_ == CLOSED) {
		if (stepClosed == rateClosed_) {
			closedflag = true;
			stepClosed = 0;
		} else
			stepClosed++;
	}
	if (wl_->wlData_.isOpenClosed_ == OPEN) {
		if (stepOpen == rateOpen_) {
			openflag = true;
			stepOpen = 0;
		} else
			stepOpen++;
	}
	if (openflag || closedflag) {
		allDone_ = true;
		vector<int> numOfMeasure;
		for (MesureVectorIter itm = measures_.begin(); itm != measures_.end();
				itm++) {
			bool done = itm->checkDone();
			allDone_ = allDone_ && done;
			if ((itm->getMeasureType() == OPEN_MEASURE) && openflag && (!done))
				itm->DoMeasurement();
			if ((itm->getMeasureType() == CLOSED_MEASURE) && closedflag
					&& (!done))
				itm->DoMeasurement();
			int cNumOfMeasure = itm->getNumOfMeasure();
			numOfMeasure.push_back(cNumOfMeasure);
		}
		int minNumOfMeasure = *(std::min_element(numOfMeasure.begin(),
				numOfMeasure.end()));
		progBar_.update(minNumOfMeasure, endTime, dumpTime);
		if (allDone_)
			cout << minNumOfMeasure;
	}
}
void Measurements::updateMinNumOfMeasure() {
	vector<int> numOfMeasure;
	//bool endTime = false, dumpTime = false;
	for (MesureVectorIter itm = measures_.begin(); itm != measures_.end();
			itm++) {
		int cNumOfMeasure = itm->getNumOfMeasure();
		numOfMeasure.push_back(cNumOfMeasure);
	}
	int minNumOfMeasure = *(std::min_element(numOfMeasure.begin(),
			numOfMeasure.end()));
	progBar_.updateMinNumOfMeasure(minNumOfMeasure);
}
void Measurements::dump(string outPath, bool final) const {
	string fileName;
	if (final) {
		fileName = outPath + "/Measure.h5";
		dump(fileName);
	}
	fileName = outPath + "/MeasureTemp.h5";
	dump(fileName);
}
void Measurements::dump(string fileName) const {
	hdf5RW hdf5File;
	hdf5File.initNew(fileName);
	for (MesureVector::const_iterator it = measures_.begin();
			it != measures_.end(); it++)
		it->dumpToFile(hdf5File);
}
void Measurements::read(string outPath) {
	string fileName = outPath + "/Measure.h5";
	hdf5RW hdf5File;
	hdf5File.initRead(fileName);
	for (MesureVector::iterator it = measures_.begin(); it != measures_.end();
			it++)
		it->readFromFile(hdf5File);
	updateMinNumOfMeasure();
}
bool Measurements::checkNotDone() const {
	if (allDone_) {
		(*outFile_) << "All Measurements Done" << endl;
		(*outFile_) << "Simulation Time : " << progBar_.getTime() << endl;
		return false;
	} else {
		return true;
	}
}
void Measurements::updateClosedRate(int const closedRate) {
	rateClosed_ = closedRate;
}
int Measurements::getClosedRate() const {
	return rateClosed_;
}
void Measurements::updateOpenRate(int const openRate) {
	rateOpen_ = openRate;
}
int Measurements::getOpenRate() const {
	return rateOpen_;
}
// Progress Bar

void ProgressBar::init(int totalNumberOfMeasure, int minNumOfMeasure,
		double simTime, ofstream *outFile, QmcParameters qmcParameters,
		SimulationParameters *simParams) {
	startTime_ = std::chrono::system_clock::now();
	lastTime_ = startTime_;
	simTime_ = simTime;
	totalNumberOfMeasure_ = totalNumberOfMeasure;
	outFile_ = outFile;
	deltaDump_ = Hour(qmcParameters.deltaDump.getVal());
	updateMinNumOfMeasure(0.);
	moves_ = 10;
	deltamoves_ = 10;
	simParams_ = simParams;
}
void ProgressBar::update(int numOfMeasure, bool &endTime, bool &dumpFlag) {
	double p = (double(numOfMeasure) / double(totalNumberOfMeasure_));
	if (numOfMeasure > moves_) {
		dumpFlag = true;
		TimePoint now = std::chrono::system_clock::now();
		Hour diffLast = now - lastTime_;
		Hour diffStart = now - startTime_;
		lastTime_ = now;
		double ratio = double(deltaDump_.count()) / double(diffLast.count());
		ratio = ratio > 2. ? 2. : ratio;
		deltamoves_ = max(10, int(double(deltamoves_) * ratio));
		moves_ += deltamoves_;
		if (diffStart.count() > simTime_)
			endTime = true;
		(*outFile_) << "Finished " << p * 100 << " % , Time - "
				<< diffStart.count() << " Hours , Moves " << simParams_->step
				<< " ,Moves per second "
				<< double(simParams_->step) / (3600. * double(diffLast.count()))
				<< endl;
	}
}
double ProgressBar::getTime() const {
	TimePoint now = std::chrono::system_clock::now();
	Hour diffStart = now - startTime_;
	return diffStart.count();
}
void ProgressBar::updateMinNumOfMeasure(int minNumOfMeasure) {
	moves_ = minNumOfMeasure + 10;
	double p = (double(minNumOfMeasure) / double(totalNumberOfMeasure_));
	(*outFile_) << "Finished Till Now" << p * 100 << " % " << endl;
}
int ProgressBar::getTotalNumberOfMeasure() {
	return totalNumberOfMeasure_;
}

// Measure Green

GreenMeasure::~GreenMeasure() {

}

GreenMeasure::GreenMeasure(WorldLines *wl, QmcParameters &qmcParameters) {
	wl_ = wl;
	ScalarObs<double>::InitData initDataScalar = {
			qmcParameters.numOfBins.getVal(), qmcParameters.binSize.getVal() };
	array<int, SIM_DIM> sizes;
	fill(sizes.begin(), sizes.end(), qmcParameters.numGreenPoints.getVal());
	green_.init(sizes, initDataScalar);
	measureType_ = OPEN_MEASURE;
}
void GreenMeasure::DoMeasurement() {
	ObsArrayIterIndex it_g;
	ObsArray::IndexType pos;
	ParticlePos delta = wl_->wlData_.delta_;
	double pi = boost::math::constants::pi<double>();
	for (it_g = green_.beginIndex(); it_g != green_.endIndex(); it_g++) {
		pos = it_g.getIndex();
		array<double, SIM_DIM> posd;
		for (int i = 0; i < posd.size(); i++)
			posd[i] = double(pos[i]);
		double weight = cos(
				2. * pi
						* inner_product(posd.begin(), posd.end(), delta.begin(),
								0.));
		(*it_g) << weight;
	}
}

bool GreenMeasure::checkDone() const {
	return green_.checkDone();
}
void GreenMeasure::dumpToFile(hdf5RW &hdf5File) const {
	green_.dumpToFile(hdf5File, "Green");

}
void GreenMeasure::readFromFile(hdf5RW &hdf5File) {
	green_.readFromFile(hdf5File, "Green");

}
int GreenMeasure::getNumOfMeasure() const {
	return green_.getNumOfMeasure();
}
// Measure EK

EkMeasure::~EkMeasure() {

}

EkMeasure::EkMeasure(WorldLines *wl, QmcParameters &qmcParameters) {
	wl_ = wl;
	ScalarObs<double>::InitData initDataScalar = {
			qmcParameters.numOfBins.getVal(), qmcParameters.binSize.getVal() };
	ek_.init(initDataScalar);
	double tau = qmcParameters.beta.getVal() / double(qmcParameters.M.getVal());
	ratio_ = 1.
			/ (tau * tau * qmcParameters.lambda.getVal()
					* double(
							4 * qmcParameters.NumP.getVal()
									* qmcParameters.M.getVal()));
	shift_ = double(SIM_DIM) / (2. * tau);
	beta_ = qmcParameters.beta.getVal();
	measureType_ = CLOSED_MEASURE;
}
void EkMeasure::DoMeasurement() {
	double ekfluc = (wl_->wlEnergy_.energyK_ * ratio_);
	ek_ << beta_ * (shift_ - ekfluc);
}

bool EkMeasure::checkDone() const {
	return ek_.checkDone();
}
void EkMeasure::dumpToFile(hdf5RW &hdf5File) const {
	ek_.dumpToFile(hdf5File, "Ek");

}
void EkMeasure::readFromFile(hdf5RW &hdf5File) {
	ek_.readFromFile(hdf5File, "Ek");

}
int EkMeasure::getNumOfMeasure() const {
	return ek_.getNumOfMeasure();
}

// Measure Winding

Winding::~Winding() {

}

Winding::Winding(WorldLines *wl, QmcParameters &qmcParameters) {
	wl_ = wl;
	ScalarObs<double>::InitData initDataScalar = {
			qmcParameters.numOfBins.getVal(), qmcParameters.binSize.getVal() };
	wind_.init(initDataScalar);
	measureType_ = CLOSED_MEASURE;
}
void Winding::DoMeasurement() {
	array<int, 3> winds;
	winds.fill(0);
	for (int n = 0; n < wl_->numP_; n++)
		for (int m = 0; m < wl_->M_; m++) {
			for (int d = 0; d < SIM_DIM; d++) {
				double diff;
				if (m != wl_->M_ - 1)
					diff = wl_->wlData_.confData_(m + 1, n)[d]
							- wl_->wlData_.confData_(m, n)[d];
				else
					diff = wl_->wlData_.confData_(0, wl_->wlData_.loops_[n])[d]
							- wl_->wlData_.confData_(m, n)[d];
				if (diff > .5)
					winds[d]++;
				if (diff < -.5)
					winds[d]--;
			}
		}
	double wind = 0.;
	for (int d = 0; d < SIM_DIM; d++)
		wind += double(winds[d] * winds[d]) / double(SIM_DIM);
	wind_ << wind;
}

bool Winding::checkDone() const {
	return wind_.checkDone();
}
void Winding::dumpToFile(hdf5RW &hdf5File) const {
	wind_.dumpToFile(hdf5File, "WIND");

}
void Winding::readFromFile(hdf5RW &hdf5File) {
	wind_.readFromFile(hdf5File, "WIND");

}
int Winding::getNumOfMeasure() const {
	return wind_.getNumOfMeasure();
}

// Measure Energy

EnergyMeasure::~EnergyMeasure() {

}

EnergyMeasure::EnergyMeasure(WorldLines *wl, QmcParameters &qmcParameters) {
	wl_ = wl;
	ScalarObs<double>::InitData initDataScalar = {
			qmcParameters.numOfBins.getVal(), qmcParameters.binSize.getVal() };
	e_.init(initDataScalar);
	ef_.init(initDataScalar);
	ev_.init(initDataScalar);
	ek_.init(initDataScalar);
	ep_.init(initDataScalar);
	measureType_ = CLOSED_MEASURE;
	av_ = double(wl_->numP_ * wl_->M_);
	eps_ = qmcParameters.beta.getVal() / double(qmcParameters.M.getVal());
	lambda_ = qmcParameters.lambda.getVal();
	ratiof_ = (6. / 9.) * pow(eps_, 2) * (lambda_ / av_);
}
void EnergyMeasure::DoMeasurement() {
	double total, ek, ev, ef, et, ep;
	ek = wl_->wlEnergy_.energyK_;
	ev = wl_->wlEnergy_.energyV_;
	ef = wl_->wlEnergy_.energyF_;
	ek = (double(SIM_DIM) / (2. * eps_))
			- (1. / (av_ * 4. * lambda_ * pow(eps_, 2))) * ek;
#ifdef FORCE
	ev = (2. * ev) / (3. * av_);
	ef = ef * ratiof_;
	ef_ << ef;
	ep = ev + (2. / 3.) * ef;

	et = ek + ev + ef;
#else
	ev = ev / av_;
	et = ek + ev;
	ep = ev;
#endif
	ek_ << ek;
	ev_ << ev;
	ef_ << ef;
	e_ << et;
	ep_ << ep;
}

bool EnergyMeasure::checkDone() const {
	return e_.checkDone();
}
void EnergyMeasure::dumpToFile(hdf5RW &hdf5File) const {
	e_.dumpToFile(hdf5File, "ENERGY");
	ek_.dumpToFile(hdf5File, "EK");
	ef_.dumpToFile(hdf5File, "EF");
	ev_.dumpToFile(hdf5File, "EV");
	ep_.dumpToFile(hdf5File, "EP");

}
void EnergyMeasure::readFromFile(hdf5RW &hdf5File) {
	e_.readFromFile(hdf5File, "ENERGY");
	ek_.readFromFile(hdf5File, "EK");
	ef_.readFromFile(hdf5File, "EF");
	ev_.readFromFile(hdf5File, "EV");
	ep_.readFromFile(hdf5File, "EP");

}
int EnergyMeasure::getNumOfMeasure() const {
	return e_.getNumOfMeasure();
}

// Density Density

DensityDensityETMeasure::~DensityDensityETMeasure() {

}

DensityDensityETMeasure::DensityDensityETMeasure(WorldLines *wl,
		QmcParameters &qmcParameters) {
	wl_ = wl;
	ScalarObs<double>::InitData initDataScalar = {
			qmcParameters.numOfBins.getVal(), qmcParameters.binSize.getVal() };
	array<int, SIM_DIM> sizes;
	fill(sizes.begin(), sizes.end(), qmcParameters.numGreenPoints.getVal());
	sk_.init(sizes, initDataScalar);
	measureType_ = CLOSED_MEASURE;
}
void DensityDensityETMeasure::DoMeasurement() {
	ObsArrayIterIndex it_g;
	ObsArray::IndexType kpos;
	ParticlePos pos;
	double av = double(wl_->M_ * wl_->numP_);
	complex<double> weight_m;
	double weight;
	double pi = boost::math::constants::pi<double>();
	for (it_g = sk_.beginIndex(); it_g != sk_.endIndex(); it_g++) {
		kpos = it_g.getIndex();
		weight = 0.;
		for (int m = 0; m < wl_->M_; m++) {
			weight_m = complex<double>(0, 0);
			for (int n = 0; n < wl_->numP_; n++) {
				pos = wl_->wlData_.confData_(m, n);
				double product = 0.;
				for (int d = 0; d < SIM_DIM; d++)
					product += double(kpos[d]) * (pos[d]);
				weight_m += exp(complex<double>(0., 2. * pi * product));
			}
			weight += norm(weight_m);
		}
		(*it_g) << weight / av;
	}
}

bool DensityDensityETMeasure::checkDone() const {
	return sk_.checkDone();
}
void DensityDensityETMeasure::dumpToFile(hdf5RW &hdf5File) const {
	sk_.dumpToFile(hdf5File, "SK");

}
void DensityDensityETMeasure::readFromFile(hdf5RW &hdf5File) {
	sk_.readFromFile(hdf5File, "SK");

}
int DensityDensityETMeasure::getNumOfMeasure() const {
	return sk_.getNumOfMeasure();
}

// Density Density

DensityDensityDynamicalMeasure::~DensityDensityDynamicalMeasure() {

}

DensityDensityDynamicalMeasure::DensityDensityDynamicalMeasure(WorldLines *wl,
		QmcParameters &qmcParameters) {
	wl_ = wl;
	dtRatio_=qmcParameters.dtRatio.getVal() ? 2 : 1;
	numOfSlices_=wl_->M_/dtRatio_;
	pb_ = qmcParameters.phononBranch.getVal();
	vector<PhononBranch>::iterator it_pb;
	VectorObs<double>::InitData initDataScalar = {
			qmcParameters.numOfBins.getVal(), (numOfSlices_ / 2 + 1),
			qmcParameters.binSize.getVal() };
	for (vector<PhononBranch>::iterator it_b = pb_.begin(); it_b != pb_.end();
			it_b++) {
		array<int, 1> sizes;
		sizes[0] = it_b->ks_.size();
		it_b->skw_.init(sizes, initDataScalar);
		it_b->skwSqr_.init(sizes, initDataScalar);
		it_b->weightM_.resize(it_b->ks_.size());
		it_b->weight_.resize(it_b->ks_.size());
		it_b->weightMSqr_.resize(it_b->ks_.size());
		it_b->weightSqr_.resize(it_b->ks_.size());
		for (int s = 0; s < it_b->ks_.size(); s++) {
			it_b->weightM_[s].resize(numOfSlices_);
			it_b->weightMSqr_[s].resize(numOfSlices_);
			it_b->weight_[s].resize((numOfSlices_) / 2 + 1);
			it_b->weightSqr_[s].resize((numOfSlices_) / 2 + 1);
			assert(!(wl_->M_ % 2));
		}
	}
	fftc2c_.init(&(pb_[0].weightM_[0][0]), &(pb_[0].weightM_[0][0]),
			numOfSlices_);
	fftr2c_.init(&(pb_[0].weightMSqr_[0][0]), &(pb_[0].weightM_[0][0]),
			numOfSlices_);
	measureType_ = CLOSED_MEASURE;
}
void DensityDensityDynamicalMeasure::DoMeasurement() {
	double av = double((numOfSlices_) * wl_->numP_);
	double pi = boost::math::constants::pi<double>();
	// Single k point
	for (vector<PhononBranch>::iterator it_b = pb_.begin(); it_b != pb_.end();
			it_b++) {
		int numofk = it_b->skw_.totalSize();
#pragma omp parallel for
		for (int s = 0; s < numofk; s++) {
			array<int, SIM_DIM> kpos;
			ParticlePos pos;
			vector<vector<array<int, SIM_DIM> > >::iterator it_kp =
					it_b->ks_.begin() + s;
			ObsArrayIter it_o = it_b->skw_.begin() + s;
			ObsArrayIter it_osqr = it_b->skwSqr_.begin() + s;
			int nums = it_kp->size();
			for (int m = 0; m < (numOfSlices_/2+1); m++) {
				it_b->weight_[s][m] = 0.;
				it_b->weightSqr_[s][m] = 0.;
			}
			for (vector<array<int, SIM_DIM> >::iterator it_ks = it_kp->begin();
					it_ks != it_kp->end(); it_ks++) {
				for (int m = 0; m < numOfSlices_; m++) {
					complex<double> weight = complex<double>(0, 0);
					for (int n = 0; n < wl_->numP_; n++) {
						pos = wl_->wlData_.confData_((dtRatio_ * m), n);
						// Run over symmetry
						kpos = *it_ks;
						double product = 0.;
						for (int d = 0; d < SIM_DIM; d++)
							product += double(kpos[d]) * (pos[d]);
						weight += complex<double>(cos(2. * pi * product),
								sin(2. * pi * product));
					}
					it_b->weightM_[s][m] = weight;
					it_b->weightMSqr_[s][m] = norm(weight);
				}
				fftc2c_.executeNewArray(&(it_b->weightM_[s][0]),
						&(it_b->weightM_[s][0]));
				for (int m = 0; m < (numOfSlices_ / 2 + 1); m++) {
					it_b->weight_[s][m] += (norm(it_b->weightM_[s][m]))
							/ (av * double(nums));
				}
				fftr2c_.executeNewArray(&(it_b->weightMSqr_[s][0]),
						&(it_b->weightM_[s][0]));
				for (int m = 0; m < (numOfSlices_ / 2 + 1); m++) {
					it_b->weightSqr_[s][m] += (norm(it_b->weightM_[s][m]))
							/ (av * double(nums));
				}
			}
			(*it_o) << it_b->weight_[s];
			(*it_osqr) << it_b->weightSqr_[s];
		}
	}
}
bool DensityDensityDynamicalMeasure::checkDone() const {
	return pb_[0].skw_.checkDone();
}
void DensityDensityDynamicalMeasure::dumpToFile(hdf5RW &hdf5File) const {
	vector<PhononBranch>::const_iterator it_b = pb_.begin();
	for (; it_b != pb_.end(); it_b++) {
		it_b->skw_.dumpToFile(hdf5File, "SKW_" + it_b->name_);
		it_b->skwSqr_.dumpToFile(hdf5File, "SKWSQR_" + it_b->name_);
	}

}
void DensityDensityDynamicalMeasure::readFromFile(hdf5RW &hdf5File) {
	for (vector<PhononBranch>::iterator it_b = pb_.begin(); it_b != pb_.end();
			it_b++) {
		it_b->skw_.readFromFile(hdf5File, "SKW_" + it_b->name_);
		it_b->skwSqr_.readFromFile(hdf5File, "SKWSQR_" + it_b->name_);
	}
}
int DensityDensityDynamicalMeasure::getNumOfMeasure() const {
	return pb_[0].skw_.getNumOfMeasure();
}

// Density Density

DensityDensitySquareDynamicalMeasure::~DensityDensitySquareDynamicalMeasure() {

}

DensityDensitySquareDynamicalMeasure::DensityDensitySquareDynamicalMeasure(
		WorldLines *wl, QmcParameters &qmcParameters) {
	wl_ = wl;
	pb_ = qmcParameters.phononBranch.getVal();
	vector<PhononBranch>::iterator it_pb;
	VectorObs<double>::InitData initDataScalar = {
			qmcParameters.numOfBins.getVal(), (wl_->M_ / 2) / 2 + 1,
			qmcParameters.binSize.getVal() };
	for (vector<PhononBranch>::iterator it_b = pb_.begin(); it_b != pb_.end();
			it_b++) {
		array<int, 1> sizes;
		sizes[0] = it_b->ks_.size();
		it_b->skw_.init(sizes, initDataScalar);
		it_b->weightM_.resize(it_b->ks_.size());
		it_b->weight_.resize(it_b->ks_.size());
		for (int s = 0; s < it_b->ks_.size(); s++) {
			it_b->weightM_[s].resize((wl_->M_ / 2) / 2 + 1);
			it_b->weight_[s].resize((wl_->M_ / 2));
			assert(!(wl_->M_ % 2));
		}
	}
	fft_.init(&(pb_[0].weight_[0][0]), &(pb_[0].weightM_[0][0]), wl_->M_ / 2);
	measureType_ = CLOSED_MEASURE;
}
void DensityDensitySquareDynamicalMeasure::DoMeasurement() {
	double av = double((wl_->M_ / 2) * wl_->numP_);
	double pi = boost::math::constants::pi<double>();
	// Branch
	for (vector<PhononBranch>::iterator it_b = pb_.begin(); it_b != pb_.end();
			it_b++) {
		int numofk = it_b->skw_.totalSize();
#pragma omp parallel for
		for (int s = 0; s < numofk; s++) {
			PhononBranch::KVec kpos;
			ParticlePos pos;
			PhononBranch::BranchKVec::iterator it_kp = it_b->ks_.begin() + s;
			ObsArrayIter it_o = it_b->skw_.begin() + s;
			int nums = it_kp->size();
			vector<double> weightnorm;
			weightnorm.resize((wl_->M_ / 2) / 2 + 1);
			for (int m = 0; m < (wl_->M_ / 2) / 2 + 1; m++)
				weightnorm[m] = 0.;
			for (PhononBranch::SymKVec::iterator it_ks = it_kp->begin();
					it_ks != it_kp->end(); it_ks++) {
				kpos = *it_ks;
				for (int m = 0; m < (wl_->M_ / 2) / 2 + 1; m++)
					it_b->weight_[s][m] = 0.;
				for (int m = 0; m < wl_->M_ / 2; m++) {
					complex<double> weight = complex<double>(0, 0);
					for (int n = 0; n < wl_->numP_; n++) {
						pos = wl_->wlData_.confData_((2 * m), n);
						double product = 0.;
						for (int d = 0; d < SIM_DIM; d++)
							product += double(kpos[d]) * (pos[d]);
						weight += complex<double>(cos(2. * pi * product),
								sin(2. * pi * product));
					}
					it_b->weight_[s][m] = norm(weight);
				}
				fft_.executeNewArray(&(it_b->weight_[s][0]),
						&(it_b->weightM_[s][0]));
				for (int m = 0; m < (wl_->M_ / 2) / 2 + 1; m++)
					weightnorm[m] += (norm(it_b->weightM_[s][m]))
							/ (av * double(nums));
			}
			(*it_o) << weightnorm;
		}

	}
}

bool DensityDensitySquareDynamicalMeasure::checkDone() const {
	return pb_[0].skw_.checkDone();
}
void DensityDensitySquareDynamicalMeasure::dumpToFile(hdf5RW &hdf5File) const {
	vector<PhononBranch>::const_iterator it_b = pb_.begin();
	for (; it_b != pb_.end(); it_b++)
		it_b->skw_.dumpToFile(hdf5File, "SKW_SQ" + it_b->name_);

}
void DensityDensitySquareDynamicalMeasure::readFromFile(hdf5RW &hdf5File) {
	for (vector<PhononBranch>::iterator it_b = pb_.begin(); it_b != pb_.end();
			it_b++)
		it_b->skw_.readFromFile(hdf5File, "SKW_SQ" + it_b->name_);
}
int DensityDensitySquareDynamicalMeasure::getNumOfMeasure() const {
	return pb_[0].skw_.getNumOfMeasure();
}

// Density Density

void autocoor(const Array<double, 1> &v1, const Array<double, 1> &v2,
		Array<double, 1> &res) {
	int size = res.size();
	res = 0.;
	for (int tau = 0; tau < size; tau++) {
		for (int taup = 0; taup < size; taup++) {
			res(tau) += v1(taup) * v2((taup + tau) % size);
		}
	}
	res /= double(size);
}

FourPointDynamicalMeasure::~FourPointDynamicalMeasure() {

}

FourPointDynamicalMeasure::FourPointDynamicalMeasure(WorldLines *wl,
		QmcParameters &qmcParameters) {
	wl_ = wl;
	VectorObs<double>::InitData initDataVector = {
			qmcParameters.numOfBins.getVal(), wl_->M_,
			qmcParameters.binSize.getVal() };
	num_of_Bragg_ = qmcParameters.num_of_Bragg.getVal();
	weightG_.resize(num_of_Bragg_, wl_->M_);
	weightGnorm_.resize(num_of_Bragg_, wl_->M_);
	corrTemp_.resize(wl_->M_);
	corr_.resize(wl_->M_);
	/// Only for BCC !!!!
	int BraggSize = qmcParameters.BraggSize.getVal();
	BraggVecs_= { { {0,BraggSize,BraggSize}}, { {0,BraggSize,-BraggSize}}
		, {	{	BraggSize,0,BraggSize}}, { {BraggSize,0,-BraggSize}}
		, {	{	BraggSize,BraggSize,0}}, { {BraggSize,-BraggSize,0}}};
	corr_.resize(wl_->M_);
	ortPairs_ = {BraggPair(0,1),BraggPair(2,3),BraggPair(4,5)};
	nonOrtPairs_ = {BraggPair(0,2),BraggPair(0,3),BraggPair(0,4),BraggPair(0,5),
		BraggPair(1,2),BraggPair(1,3),BraggPair(1,4),BraggPair(1,5),
		BraggPair(2,4),BraggPair(2,5),
		BraggPair(3,4),BraggPair(3,5)};
	ortG1G2_.init(initDataVector);
	nonortG1G2_.init(initDataVector);
	same_.init(initDataVector);
	// BCC Lattice vectors
	measureType_ = CLOSED_MEASURE;

}
void FourPointDynamicalMeasure::DoMeasurement() {
	double pi = boost::math::constants::pi<double>();
	weightG_ = complex<double>(0., 0.);
	weightGnorm_ = 0.;
	for (int m = 0; m < wl_->M_; m++) {
		for (int n = 0; n < wl_->numP_; n++) {
			ParticlePos pos = wl_->wlData_.confData_(m, n);
			for (int G = 0; G < num_of_Bragg_; G++) {
				double product = 0.;
				for (int d = 0; d < SIM_DIM; d++)
					product += double(BraggVecs_[G][d]) * (pos[d]);
				weightG_(G, m) += complex<double>(cos(2. * pi * product),
						sin(2. * pi * product));
			}
		}
		for (int G = 0; G < num_of_Bragg_; G++)
			weightGnorm_(G, m) = norm(weightG_(G, m));
	}
	corr_ = 0.;
	for (auto it_ort = ortPairs_.begin(); it_ort != ortPairs_.end(); it_ort++) {
		autocoor(weightGnorm_(it_ort->first, Range::all()),
				weightGnorm_(it_ort->second, Range::all()), corrTemp_);
		corr_ = corr_ + corrTemp_ / double(ortPairs_.size());
	}
	ortG1G2_ << corr_;
	corr_ = 0.;
	for (auto it_nonort = nonOrtPairs_.begin(); it_nonort != nonOrtPairs_.end();
			it_nonort++) {
		autocoor(weightGnorm_(it_nonort->first, Range::all()),
				weightGnorm_(it_nonort->second, Range::all()), corrTemp_);
		corr_ = corr_ + corrTemp_ / double(nonOrtPairs_.size());
	}
	nonortG1G2_ << corr_;
	corr_ = 0.;
	for (int i=0;i<6;i++) {
		autocoor(weightGnorm_(i, Range::all()),
				weightGnorm_(i, Range::all()), corrTemp_);
		corr_ = corr_ + corrTemp_ / double(6);
	}
	same_ << corr_;

}

bool FourPointDynamicalMeasure::checkDone() const {
	return ortG1G2_.checkDone();
}
void FourPointDynamicalMeasure::dumpToFile(hdf5RW &hdf5File) const {
	ortG1G2_.dumpToFile(hdf5File, "ORT_FOUR_POINT");
	nonortG1G2_.dumpToFile(hdf5File, "NON_ORT_FOUR_POINT");
	same_.dumpToFile(hdf5File, "SAME");

}

void FourPointDynamicalMeasure::readFromFile(hdf5RW &hdf5File) {
	ortG1G2_.readFromFile(hdf5File, "ORT_FOUR_POINT");
	nonortG1G2_.readFromFile(hdf5File, "NON_ORT_FOUR_POINT");
	same_.readFromFile(hdf5File, "SAME");
}
int FourPointDynamicalMeasure::getNumOfMeasure() const {
	return ortG1G2_.getNumOfMeasure();
}

