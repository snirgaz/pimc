//
//  SimParams.h
//  WORM_ALPT
//
//  Created by Snir Gazit on 8/9/11.
//  Copyright 2011 __MyCompanyName_. All rights reserved.
//

#ifndef WORM_AL_PT_SimParamsh
#define WORM_AL_PT_SimParamsh

#include "HelpLibs/def.h"
#include "ParticlePos.h"
//#include "xml_rw.h"
#include "HelpLibs/hdf5RW.h"
#include "HelpLibs/ArrayMultiD.h"
struct toMeasure {
	bool SF;
	bool Density;
	bool Rho;
	bool Winding;
	bool JqJq;
	bool Energy;
	bool Angle;
	bool DensityPhonon;
	bool DensitySym;
};
struct potentials {
	bool Free;
	bool Harmonic;
	bool Log;
	double Omega;
};

class PTParams {
public:
	vector<double> RSS;
	vector<string> outDirs, initDirs;
	string outPathPT;
	int numOfConf;
	string PTOutFile;
	int numAdjustLambda;
	int numAdjustRuns;
	int numAdjustPar;
	int maxDelta;
	int numSplit;
	int numPar;
	double runTime;
	// PT run number for split runs
	int runNum;
	string initPath;
	string initType;
	int coreNum;
	int numRunsPrint;
};

template<int size>
class Histogram {
private:
	array<acc, size> samps_;
public:
	void init() {
		samps_.fill(acc());
	}
	;
	void update(int pos) {
		for (int p = 0; p < size; p++)
			if (p == pos)
				samps_[p](1.);
			else
				samps_[p](0.);
	}
	double getPrecentage(int pos) {
		return ba::mean(samps_[pos]);
	}
};

struct statistics {
	// Prob Blotz
	array<acc, NUM_OF_MOVES> Boltzman;
	// Prob Rho
	acc Rho;
#ifdef SWAP
	// Prob Swap
	acc Swap;
	// try to swap on the same world line
	acc swapSame;
#endif
#ifdef CENTER_OF_MASS_MOVE
	acc sameCOM;
#endif
	Histogram<NUM_OF_MOVES> moves;
	void init() {
		Boltzman.fill(acc());
		Rho = acc();
#ifdef SWAP
		Swap = acc();
		swapSame = acc();
#endif
		moves.init();
	}
	void dumpToFile(string fileName) {
		ofstream fs;
		try {
			fs.open(fileName.c_str());
		} catch (exception &e) {
			cout << e.what();
			exit(1);
		}
		YAML::Node out;

		for (int i = 0; i < NUM_OF_MOVES; i++) {
			string move_type;
			switch (i) {
			case 0:
				move_type = "OPEN";
				break;
			case 1:
				move_type = "CLOSE";
				break;
			case 2:
				move_type = "WIGGLE";
				break;
#ifdef SWAP
			case 3:
				move_type = "SWAP";
				break;
#endif
#ifdef CENTER_OF_MASS_MOVE
			case 4:
				move_type = "COM";
				break;
#endif
			}
			out[move_type]["move_ratio"] = moves.getPrecentage(i);
			out[move_type]["blz_accept"] = ba::mean(Boltzman[i]);
		}
		out["RHO"] = ba::mean(Rho);
#ifdef SWAP
		out["SWAP_RATIO"] = ba::mean(Swap);
		out["SWAP_SAME"] = ba::mean(swapSame);
#endif
#ifdef CENTER_OF_MASS_MOVE
		out["COM_SAME"] = ba::mean(sameCOM);
#endif
		fs << YAML::Dump(out);
	}
};
#endif
