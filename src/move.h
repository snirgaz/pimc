/*
 * move.h
 *
 *  Created on: Apr 16, 2013
 *      Author: snirgaz
 */

#ifndef MOVE_H_
#define MOVE_H_

#include "HelpLibs/def.h"
#include "Bisection.h"
#include "HelpLibs/RandomGen.h"
#include "HelpLibs/QmcParameters.h"
#include "WorldLines.h"
#include "boost/lambda/lambda.hpp"
#include "boost/lambda/bind.hpp"

class Move {
protected:
	WorldLines *wl_;
	Bisection *bisection_;
	UniformReal uniZeroOne_;
	MoveParams *moveParams_;
	int M_;
	// Qmc Parameters
	SimulationParameters *simParams_;
public:
	Move();
	bool accept(double p);
};

class OpenMove: public Move {
private:
	// Draws position of the "Jump Move"
	UniformInt posOpen_;
	// Draws Starting Time Slice
	UniformInt mStart_;
	// Draws PArticle Number
	UniformInt particleNum_;
public:
	void init(QmcParameters qmcParameters, SimulationParameters *simParams,
			WorldLines * wl, Bisection * bisection, MTGen *rngGen,
			MoveParams *moveParams);
	bool makeMove();
	void updateConf();
	// Calculate Open closed Ratio
	double rhoOpenClosed();
};

class CloseMove: public Move {
private:
	// Draws position of the "Jump Move"
	UniformInt posClose_;
public:
	void init(QmcParameters qmcParameters, SimulationParameters *simParams,
			WorldLines * wl, Bisection * bisection, MTGen *rngGen,
			MoveParams *moveParams);
	bool makeMove();
	void updateConf();
	// Calculate Open closed Ratio
	double rhoOpenClosed();
};

class WiggleMove: public Move {
private:
	// Draws position of the "Jump Move"
	UniformInt posWiggle_;
public:
	void init(QmcParameters qmcParameters, SimulationParameters *simParams,
			WorldLines * wl, Bisection * bisection, MTGen *rngGen,
			MoveParams *moveParams);
	bool makeMove();
	void updateConf();
};
#ifdef SWAP
class SwapMove: public Move {
private:
	int wlOriginalBegin_, wlOriginalEnd_, wlSwapToBegin_, wlSwapToEnd_;
	double swapRatio_;
	vector<double> rho_;
	int numP_;
public:
	void init(QmcParameters qmcParameters, SimulationParameters *simParams,
			WorldLines * wl, Bisection * bisection, MTGen *rngGen,
			MoveParams *moveParams);
	bool makeMove();
	bool rhoSwap();
	// Claculates Kinetic energy from original
	void sigmaSwap(int original);
	void updateConf();
};
#endif
#ifdef CENTER_OF_MASS_MOVE
class CenterOfMassMove: public Move {
private:
	UniformInt partNum_;
	int moveWL_;
	ParticlePos dr_;
	Normal rnddr_;
	int numP_;
public:
	void init(QmcParameters qmcParameters, SimulationParameters *simParams,
			WorldLines * wl, Bisection * bisection, MTGen *rngGen,
			MoveParams *moveParams);
	bool makeMove();
	bool buildMoveWL();
	void updateConf();
};
#endif
#endif /* MOVE_H_ */
