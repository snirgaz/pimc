/*
 * pt.h
 *
 *  Created on: Nov 20, 2010
 *      Author: snirgaz
 */

#ifndef PT_H_
#define PT_H_
#include "HelpLibs/def.h"
#include "Qmc.h"
#include "SimParams.h"

typedef boost::ptr_vector<Qmc> confVec;
enum confDirection {
	UP, DOWN
};

struct confData {
	int confNum;
	confDirection confDir;
};

class ParallelTempring: public QmcParams, public PTParams {
private:
	// All Configurations
	confVec confs_;
	IntVec histUp_, histDown_;
	std::ofstream OutPutfilePT_;
	DoubleVec Lambdas_, Force_Factor, Sigmas_, EKs_;
	std::vector<unsigned long int> ptRate_;
	UniSmallVariable jump_, upDown_, conf_;
	UniRealVariable uniZeroOne_;
	double ptTime_, measureTime_;
	int mhz_;
	bool stopMeasureEnd_, stopTimeEnd_;
	timeval startAll_;
	RanGen ptStream_;
	std::vector<confData> confsData_;
public:
	ParallelTempring(QmcParams &SP, PTParams &PP);
	~ParallelTempring();
	void thermalize();
	void run();
	void runN(int n);
	void swapConf(int conf1, int conf2);
	void swapConfMove();
	void ptMeasure();
	bool ptMeasureDone();
	int getMaxRate();
	double calcDS(int conf1, int conf2);
	bool accept(double p);
	void initializeFromFile();
	void initializeSolid();
	void dump();
	void dumpConf();
	void printConf();
	int countClosed();
	void updateConfVec(int conf1, int conf2);
	void updateConfHistogram();
	void initializeConfHistogram();
	int getNumMeasure();
	void printStatus();
	void updateLambdas();
	void adjustLamabdas();
	void dumpHistograms(int i);
	void maxEnergyDiff();
	void initializeWLDataFromFile();
};
#endif /* PT_H_ */

