/*
 * hdf5_rw.cpp
 *
 *  Created on: Apr 7, 2011
 *      Author: snirgaz
 */

#include "hdf5RW.h"

hdf5RW::hdf5RW() {

}
template<>
DataType hdf5RW::getHDF5DataType<double>() {
	return PredType::NATIVE_DOUBLE;
}
template<>
DataType hdf5RW::getHDF5DataType<char>() {
	return PredType::NATIVE_CHAR;
}
template<>
DataType hdf5RW::getHDF5DataType<int>() {
	return PredType::NATIVE_INT;
}
template<>
DataType hdf5RW::getHDF5DataType<long int>() {
	return PredType::NATIVE_LONG;
}
template<>
DataType hdf5RW::getHDF5DataType<bool>() {
	return PredType::NATIVE_UCHAR;
}
void hdf5RW::initNew(string fileName) {
	try {
		Exception::dontPrint();
		file_ = H5File(fileName.c_str(), H5F_ACC_TRUNC);
	} catch (FileIException error) {
		error.printError();
		exit(-1);
	}
}
void hdf5RW::initAppend(string fileName) {
	try {
		Exception::dontPrint();
		file_ = H5File(fileName.c_str(), H5F_ACC_RDWR);
	} catch (FileIException error) {
		error.printError();
		exit(-1);
	}
}
void hdf5RW::initRead(string fileName) {
	try {
		Exception::dontPrint();
		file_ = H5File(fileName.c_str(), H5F_ACC_RDONLY);
	} catch (FileIException error) {
		error.printError();
		exit(-1);
	}
}
DataSet hdf5RW::openDataSet(string const &dsName) {
	try {
		Exception::dontPrint();
		return file_.openDataSet(dsName.c_str());
	} catch (DataSpaceIException &error) {
		error.printError();
		exit(-1);
	}
}
DataSet hdf5RW::newDataSet(string const &dsName, DataType const &type,
		DataSpace const&space) {
	try {
		Exception::dontPrint();
		return file_.createDataSet(dsName.c_str(), type, space);

	} catch (FileIException &error) {
		error.printError();
		exit(-1);
	} catch (DataSetIException &error) {
		error.printError();
		exit(-1);
	} catch (DataSpaceIException &error) {
		error.printError();
		exit(-1);
	}
}
void hdf5RW::closeFile() {
	file_.close();
}
Group hdf5RW::createGroup(string const &groupName) {
	return file_.createGroup(groupName.c_str());
}
hdf5RW::~hdf5RW() {

}

