/*
 * input_file.cpp
 *
 *  Created on: May 12, 2010
 *      Author: snirgaz
 */

#include "InputFile.h"


template<>
vector<string> YamlReader::read<vector<string>>(string param) {
	if (node_[param.c_str()]) {
		vector<string> data;
		YAML::Node node = node_[param.c_str()];
		for (YAML::Node::iterator it_n = node.begin(); it_n != node.end();
				it_n++)
			data.push_back(it_n->as<string>());
		return data;
	} else
		throw KeyErrorExceptions(string("Missing Param ") + param);

};

template<>
std::vector<PhononBranch> YamlReader::read<std::vector<PhononBranch> >(string param) {
	vector<PhononBranch> vpb;
	if (node_[param.c_str()]) {
		YAML::Node node = node_[param.c_str()];
		for (YAML::Node::iterator it_b = node.begin(); it_b != node.end();
				it_b++) {
			vector<vector<array<int, SIM_DIM> > > data;
			YAML::Node nodeks = (*it_b)["ks"];
			array<int, SIM_DIM> kvec;
			// Differnt brags
			for (YAML::Node::iterator it_n = nodeks.begin();
					it_n != nodeks.end(); it_n++) {
				vector<array<int, SIM_DIM> > data_s;
				// Symmetries
				for (YAML::Node::iterator it_s = it_n->begin();
						it_s != it_n->end(); it_s++) {
					int d = 0;
					for (YAML::Node::iterator it_k = it_s->begin();
							it_k != it_s->end(); it_k++) {
						kvec[d] = it_k->as<int>();
						d++;
					}
					data_s.push_back(kvec);
				}
				data.push_back(data_s);
			}
			PhononBranch pb;
			pb.ks_ = data;
			pb.name_ = (*it_b)["name"].as<string>();
			vpb.push_back(pb);
		}
		return vpb;
	} else
		throw KeyErrorExceptions(string("Missing Param ") + param);

};

template<>
vector<vector<array<int, SIM_DIM> > > YamlReader::read<vector<vector<array<int, SIM_DIM> > > >(string param) {
	if (node_[param.c_str()]) {
		vector<vector<array<int, SIM_DIM> > > data;
		YAML::Node node = node_[param.c_str()];
		array<int, SIM_DIM> kvec;
		// Differnt brags
		for (YAML::Node::iterator it_n = node.begin(); it_n != node.end();
				it_n++) {
			vector<array<int, SIM_DIM> > data_s;
			// Symmetries
			for (YAML::Node::iterator it_s = it_n->begin(); it_s != it_n->end();
					it_s++) {
				int d = 0;
				for (YAML::Node::iterator it_k = it_s->begin();
						it_k != it_s->end(); it_k++) {
					kvec[d] = it_k->as<int>();
					d++;
				}
				data_s.push_back(kvec);
			}
			data.push_back(data_s);
		}

		return data;
	} else
		throw KeyErrorExceptions(string("Missing Param ") + param);
};

#ifdef PYTHON

template<>
vector<string> PythonReader::read<vector<string>>(string param) {
	if (params_.has_key(param.c_str())) {
		vector<string> vecstr;
		boost::python::list node = boost::python::extract<boost::python::list>(
				params_[param.c_str()]);
		int d = 0;
		for (; d < boost::python::len(node); d++)
		vecstr.push_back(boost::python::extract<string>(node[d]));
		return vecstr;
	} else
	throw KeyErrorExceptions(string("Missing Param ") + param);

}

template<>
vector<PhononBranch> PythonReader::read<vector<PhononBranch>>(string param) {
	vector<PhononBranch> vpb;
	if (params_.has_key(param.c_str())) {
		boost::python::list node = boost::python::extract<boost::python::list>(
				params_[param.c_str()]);
		int len=boost::python::len(node);
		for (int i=0; i<len;i++) {
			vector<vector<array<int, SIM_DIM> > > data;
			boost::python::dict node_dict=boost::python::extract<boost::python::dict>(node[i]);
			boost::python::list node_ks = boost::python::extract<boost::python::list>(node_dict["ks"]);
			array<int, SIM_DIM> kvec;
			int len_ks=boost::python::len(node_ks);
			// Differnt brags
			for (int j=0;
					j<len_ks; j++) {
				vector<array<int, SIM_DIM> > data_s;
				boost::python::list node_sym=boost::python::extract<boost::python::list>(node_ks[j]);
				// Symmetries
				int len_sim=boost::python::len(node_sym);
				for (int k = 0;k<len_sim; k++) {
					boost::python::list node_dim=boost::python::extract<boost::python::list>(node_sym[k]);
					int len_dim=boost::python::len(node_dim);
					for (int d = 0;d<len_dim;d++) {
						kvec[d] = boost::python::extract<int>(node_dim[d]);

					}
					data_s.push_back(kvec);
				}
				data.push_back(data_s);
			}
			PhononBranch pb;
			pb.ks_ = data;
			pb.name_ = boost::python::extract<string>(node_dict["name"]);
			vpb.push_back(pb);
		}
		return vpb;
	} else
	throw KeyErrorExceptions(string("Missing Param ") + param);

}

template<>
vector<vector<array<int, SIM_DIM> > > PythonReader::read<vector<vector<array<int, SIM_DIM> > >>(string param) {
	if (params_.has_key(param.c_str())) {
		vector<vector<array<int, SIM_DIM> > > data;
		boost::python::dict node_dict=boost::python::extract<boost::python::dict>(params_[param.c_str()]);
		boost::python::list node_ks = boost::python::extract<boost::python::list>(node_dict["ks"]);
		array<int, SIM_DIM> kvec;
		int len_ks=boost::python::len(node_ks);
		// Differnt brags
		for (int j=0;j<len_ks; j++) {
			vector<array<int, SIM_DIM> > data_s;
			boost::python::list node_sym=boost::python::extract<boost::python::list>(node_ks[j]);
			// Symmetries
			int len_sim=boost::python::len(node_sym);
			for (int k = 0;k<len_sim; k++) {
				boost::python::list node_dim=boost::python::extract<boost::python::list>(node_sym[k]);
				int len_dim=boost::python::len(node_dim);
				for (int d = 0;d<len_dim;d++) {
					kvec[d] = boost::python::extract<int>(node_dim[d]);

				}
				data_s.push_back(kvec);
			}
			data.push_back(data_s);
		}

		return data;
	} else
	throw KeyErrorExceptions(string("Missing Param ") + param);
}
#endif
