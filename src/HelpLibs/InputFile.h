/*
 * input.h
 *
 *  Created on: May 12, 2010
 *      Author: snirgaz
 */

#ifndef INPUT_H_
#define INPUT_H_
#include "def.h"
#include "QmcParameters.h"
#include "boost/lexical_cast.hpp"
#include "yaml-cpp/yaml.h"

class KeyErrorExceptions: public std::exception {
	string missingkey_;
public:
	KeyErrorExceptions(string missingkey) :
			missingkey_(missingkey) {
	}
	virtual const char* what() const throw () {
		return (string("Missing Key") + missingkey_).c_str();
	}
	virtual ~KeyErrorExceptions() throw () {
	}
};
class YamlReader {
public:
	YamlReader(string fileName) :
			fileName_(fileName) {
		try {
			node_ = YAML::LoadFile(fileName_);

		} catch (YAML::Exception &result) {
			cout << "Error description: " << result.what() << "\n";
			exit(1);
		}
	}
	template<typename T>
	T read(string param) {
		if (node_[param.c_str()]) {
			return node_[param.c_str()].as<T>();
		} else
			throw KeyErrorExceptions(string("Missing Param ") + param);
	}
	template<typename T>
	T readOrDefault(string param, T val) {
		if (node_[param.c_str()]) {
			return node_[param.c_str()].as<T>();
		} else
			return val;
	}
private:
	string fileName_;
	YAML::Node node_;

};
template<>
vector<string> YamlReader::read<vector<string>>(string param);
template<>
std::vector<PhononBranch> YamlReader::read<std::vector<PhononBranch>>(string param);

#ifdef PYTHON
#include "boost/python.hpp"
class PythonReader {
public:
	PythonReader(boost::python::dict params) :
			params_(params) {
	}
	template<typename T>
	T read(string param) {
		if (params_.has_key(param.c_str())) {
			T data = boost::python::extract<T>(params_[param.c_str()]);
			return boost::python::extract<T>(params_[param.c_str()]);
		} else
			throw KeyErrorExceptions(string("Missing Param ") + param);
	}
	template<typename T>
	T readOrDefault(string param, T val) {
		if (params_.has_key(param.c_str())) {
			return boost::python::extract<T>(params_[param.c_str()]);
		} else
			return val;
	}
private:
	boost::python::dict params_;

};
#endif
template<typename ReaderType>
class Reader {
private:
	ReaderType reader_;
	QmcParameters qmcParameters_;
public:
	Reader(ReaderType reader);
	void read();
	QmcParameters& getQmcParams();
};
template<typename ReaderType>
Reader<ReaderType>::Reader(ReaderType reader) :
		reader_(reader) {
}
template<typename ReaderType>
void Reader<ReaderType>::read() {
	try {

		// Num of Particles
		qmcParameters_.NumP = reader_.template read<int>("N");
		// Num Of Time Slices
		qmcParameters_.M = reader_.template read<int>("M");
		// To init ?
		qmcParameters_.init = reader_.template read<bool>("init");
		// Out Path
		qmcParameters_.outPath = reader_.template read<string>("out_path_c");
		// Num of Cores
		qmcParameters_.numOfCores = reader_.template read<int>("num_of_cores");
		// Seed
		qmcParameters_.seed = reader_.template read<int>("seed");
		// Beta
		qmcParameters_.beta = reader_.template read<double>("beta");
		// Lambda (Ceperley's paper)
		qmcParameters_.lambda = reader_.template read<double>("lambda");
		// mbar
		qmcParameters_.mbar = reader_.template read<double>("mbar");
		// num thermal
		qmcParameters_.numThermal = reader_.template read<int>("num_thermal");
		// bin size
		qmcParameters_.binSize = reader_.template read<int>("bin_size");
		// num_of_bins
		qmcParameters_.numOfBins = reader_.template read<int>("num_of_bins");
		// num of green points
		qmcParameters_.numGreenPoints = reader_.template read<int>(
				"num_green_points");
		// Delta Time
		qmcParameters_.deltaDump = reader_.template read<double>("delta_dump");
		// End Time
		qmcParameters_.endTime = reader_.template read<double>("end_time");
		// rate
		qmcParameters_.rateOpen = reader_.template read<int>("rate_open");
		// rate
		qmcParameters_.rateClosed = reader_.template read<int>("rate_closed");
#ifdef POT_AZIZ
		// rZero
		qmcParameters_.rm = reader_.template read<double>("rm");
#endif
		// gamma
		qmcParameters_.gamma = reader_.template read<double>("gamma");
		// init type
		qmcParameters_.initialType = reader_.template read<string>("init_type");
		// delta tau ratio
		qmcParameters_.dtRatio = reader_.template read<bool>("dt_ratio");
		// To Measure
		qmcParameters_.toMeasure = reader_.template read<vector<string> >(
				"measurements");
		vector<string> meas = qmcParameters_.toMeasure.getVal();
		// Check if Dynamical
		if ((find(meas.begin(), meas.end(), "dyn_sk") != meas.end())
				or (find(meas.begin(), meas.end(), "dyn_sk_square")
						!= meas.end())) {
			typedef vector<PhononBranch> vecBranch;
			qmcParameters_.phononBranch = reader_.template read<vecBranch>(
					"ks_sf");

	}
#ifdef CENTER_OF_MASS_MOVE
		qmcParameters_.COMdr = reader_.template read<double>("com_dr");
#endif
		if (find(meas.begin(), meas.end(), "four_point") != meas.end())
		{
		qmcParameters_.num_of_Bragg = reader_.template read<int>(
				"num_of_bragg");
		qmcParameters_.BraggSize = reader_.template read<int>("bragg_size");
		}

	} catch (KeyErrorExceptions & e) {
		cout << e.what() << endl;
		exit(1);
	}
}
template<typename ReaderType>
QmcParameters& Reader<ReaderType>::getQmcParams() {
	return qmcParameters_;
}

#endif /* INPUT_H_ */
