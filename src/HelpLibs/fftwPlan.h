/*
 * fftwPlane.h
 *
 *  Created on: Apr 4, 2012
 *      Author: snirgaz
 */

#ifndef FFTWPLANE_H_
#define FFTWPLANE_H_

#include "def.h"
#include "fftw3.h"
//#include "mkl_dfti.h"

class fftwPlan {
private:
	fftw_plan plan_;
	bool init_;
public:
	fftwPlan();
	~fftwPlan();
	void init(double *source, std::complex<double> *target, int rank,
			int * size);
	void init(std::complex<double> *source, std::complex<double> *target, int rank,
				int * size);
	void init(std::complex<double> *source, std::complex<double> *target, int  size);
	void init(double *source, std::complex<double> *target, int  size);
	void execute();
	void executeNewArray(double *source, std::complex<double> *target);
	void executeNewArray(std::complex<double> *source, std::complex<double> *target);
};

class fftwPlanMulti {
private:
	fftw_plan plan_;
	bool init_;
public:
	fftwPlanMulti();
	~fftwPlanMulti();
	void init(double *source, std::complex<double> *target, int rank,
			int * size, int repeat);
	void execute();
};
//class fftMKL {
//private:
//	DFTI_DESCRIPTOR_HANDLE hand_;
//	bool init_;
//public:
//	fftMKL();
//	~fftMKL();
//	void init(int size);
//	void execute(double *source, std::complex<double> *target);
//};
#endif /* FFTWPLANE_H_ */
