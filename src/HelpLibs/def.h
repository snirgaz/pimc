/*
 * helper_functions.h
 *
 *  Created on: May 12, 2010
 *      Author: snirgaz
 */

#ifndef HELPER_FUNCTIONS_H_
#define HELPER_FUNCTIONS_H_

#define PI 3.14159265358979323846264338327950288
#ifdef POT_AZIZ
#define LAMBDA_ZERO 6.0596
#define R_ZERO 2.9673
#endif
#define SIN_T 0.8660254037
#define COS_T 0.5
#define INV_TAN_T -0.5773502691896257645091487805019
#define INV_SIN_T 1.1547005383792517
#define MKL_Complex16 complex<double>
#if defined(SWAP) and defined(CENTER_OF_MASS_MOVE)
#define NUM_OF_MOVES 5
#elif defined(SWAP)
#define NUM_OF_MOVES 4
#else
#define NUM_OF_MOVES 3
#endif
// #define NDEBUG
// #define BOOST_DISABLE_ASSERTS

#include <yaml-cpp/yaml.h>
#include <boost/assign/std/vector.hpp>
#include <array>
#include <chrono>
#include <vector>
#include <list>
#include <string>
#include <algorithm>
#include <numeric>
#include <complex>
#include <iostream>
#include <fstream>
//#include "mathimf.h"
#include <limits.h>
#ifdef PARALLEL_OPENMP
#define BZ_THREADSAFE
#endif
#include <blitz/array.h>
#include <cassert>
#include "boost/progress.hpp"
#include "boost/lexical_cast.hpp"
#include "H5Cpp.h"
#ifdef PYTHON
#include "boost/numpy.hpp"
#include <boost/python.hpp>
#include <boost/python/numeric.hpp>
#include <boost/python/tuple.hpp>
#endif
enum OC {
	OPEN, CLOSED
};

#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/ptr_container/ptr_list.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/assign/list_inserter.hpp>
#include "boost/tuple/tuple.hpp"
//#include <boost/foreach.hpp>
///////////////////////////////////////////////////////
// Typdefs

typedef std::chrono::system_clock::time_point TimePoint;
typedef std::chrono::duration<double, std::ratio<3600> > Hour;

// Accumulator
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/moment.hpp>

namespace ba = boost::accumulators;
typedef ba::accumulator_set<double, ba::stats<ba::tag::mean> > acc;

bool isInfinite(const double pV);
#endif /* HELPER_FUNCTIONS_H_ */

