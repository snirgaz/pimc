/*
 * hdf5_rw.h
 *
 *  Created on: Apr 7, 2011
 *      Author: snirgaz
 */
#ifndef HDF5_RW_H_
#define HDF5_RW_H_
#include "def.h"
#include <string>
using namespace std;
using namespace H5;
typedef std::vector<hsize_t> DimsHdf5;
class hdf5RW {
private:
	// File_id
	H5File file_;
public:
	hdf5RW();
	~hdf5RW();
	void initNew(string fileName);
	void initAppend(string fileName);
	void initRead(string fileName);
	void closeFile();
	template<class T>
	void writeComp(string dsName, DimsHdf5 dims, T const *data);
	template<class T>
	void read(string dsName, T *data);
	template<class T>
	void writeSingle(string dsName, T const data);
	template<class T>
	static DataType getHDF5DataType();
	DataSet openDataSet(string const & dsName);
	DataSet newDataSet(string const &dsName, DataType const &type,
			DataSpace const &space);
	Group createGroup(string const &dsName);
	template <typename T,int D>
	void dumpArray(string dsName,blitz::Array<T,D> array);
	template <typename T,int D>
	void dumpPosArray(string dsName,blitz::Array<T,D> array);
	template <typename T>
	void dumpVector(string dsName,vector<T> array);
};


template<typename T,int D>
void hdf5RW::dumpArray(string dsName,typename blitz::Array<T,D> array) {
	DimsHdf5 dims(D);
	for (int d=0;d<D;d++) dims[d]=(array.shape())[d];
	writeComp<T>(dsName,dims,array.data());
}

template<typename T,int D>
void hdf5RW::dumpPosArray(string dsName,typename blitz::Array<T,D> array) {
	DimsHdf5 dims(D+1);
	for (int d=0;d<D;d++) dims[d]=(array.shape())[d];
	dims[D]=SIM_DIM;
	writeComp<double>(dsName,dims,&(array.begin()->operator[](0)));
}
template<typename T>
void hdf5RW::dumpVector(string dsName,vector<T> array) {
	DimsHdf5 dims(1);
	dims[0]=array.size();
	writeComp<T>(dsName,dims,&(array[0]));
}


template<typename T>
void hdf5RW::writeSingle(string dsName, T const data) {
	DimsHdf5 dims(1, 1);
	T temp = data;
	this->writeComp<T>(dsName, dims, &temp);
}
template<typename T>
void hdf5RW::writeComp(string dsName, DimsHdf5 dims, T const *data) {
	try {
		Exception::dontPrint();
		DataType type = hdf5RW::getHDF5DataType<T>();
		// Dimensions
		int rank = dims.size();
		hsize_t *dimsP = &(dims[0]);
//		DimsHdf5 cdims(dims.size(), 1);
//		cdims.back() = dims.back();
//		hsize_t *cdimsP = &(cdims[0]);
		// Property List
		int fillvalue = 0; /* Fill value for the dataset */
		DSetCreatPropList plist = DSetCreatPropList(H5P_DATASET_CREATE);
		plist.setFillValue(type, &fillvalue);
		plist.setChunk(rank, dimsP);
		plist.setDeflate(9);
		// Data Space
		DataSpace space(rank, dimsP);
		DataSet dataset = DataSet(
				file_.createDataSet(dsName.c_str(), type, space, plist));
		dataset.write(data, type, space, space);
	} // end of try block
	  // catch failure caused by the DataSet operations
	catch (DataSetIException &error) {
		error.printError();
		exit(-1);
	}
	// catch failure caused by the DataSpace operations
	catch (DataSpaceIException &error) {
		error.printError();
		exit(-1);
	}
}
template<typename T>
void hdf5RW::read(string dsName, T *data) {
	try {
		DataType type = hdf5RW::getHDF5DataType<T>();
		DataSet dataset = file_.openDataSet(dsName.c_str());
		DataSpace dataspace = dataset.getSpace();
		dataset.read(data, type, dataspace, dataspace);
		// catch failure caused by the DataSet operations
	} catch (DataSetIException &error) {
		error.printError();
		exit(-1);
	}
	// catch failure caused by the DataSpace operations
	catch (DataSpaceIException &error) {
		error.printError();
		exit(-1);
	}

}



#endif // HDF5_RW
