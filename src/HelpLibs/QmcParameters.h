/*
 * QmcParameters.h
 *
 *  Created on:
 *      Author: snirgazit
 */

#ifndef QMCPARAMETERS_H_
#define QMCPARAMETERS_H_
#include "def.h"
#include "../SimParams.h"
#include "Observable.h"

struct PhononBranch {
	typedef array<int, SIM_DIM> KVec;
	typedef vector<KVec> SymKVec;
	typedef vector<SymKVec> BranchKVec;
	BranchKVec ks_;
	string name_;
	ArrayObservable<Observable<VectorObs<double> >, 1> skw_;
	ArrayObservable<Observable<VectorObs<double> >, 1> skwSqr_;
	vector<vector<complex<double> > > weightM_;
	vector<vector<double> > weight_;
	vector<vector<double> > weightMSqr_;
	vector<vector<double> > weightSqr_;
};

#include <map>
using namespace std;
template<typename T>
class Param {
	bool init_;
	T val_;
public:
	Param() {
		init_ = false;
	}
	;
	void setVal(T val) {
		val_ = val;
		init_ = true;
	}
	T getVal() const {
		try {
			if (!init_)
				throw string("Uninitialized Params");
			return val_;
		} catch (string &e) {
			std::cout << e;
			exit(-1);
		}
	}
	void operator=(T val) {
		setVal(val);
	}
};

typedef map<string, bool> ToMeasure;

struct QmcParameters {
	Param<int> NumP; // Number of Particles
	Param<int> M; // Number of Time slices
	Param<int> mbar; // Time window
	Param<int> dim; // Number of Dimensions
	Param<int> mbarSwap;
	Param<int> mbarOpen;
	Param<int> mbarWiggle;
	Param<int> NumPMoving; // Number of Particles
	Param<int> NumPFixed; // Number of Particles
	Param<int> numThermal; // Number of Runs till Thermal
	Param<double> beta; // Inverse Temperature
	Param<double> lambda; // particles "mass"
	Param<double> epsilon; // length of imaginary basis time segment
	Param<double> sigma; // Var of RW in a single imaginary time interval
	Param<string> outPath;
	Param<string> initPath;
	Param<bool> init;
	Param<vector<string> > toMeasure;
	Param<double> swapProb;
	Param<int> minNumOfMeasure;
	Param<int> seed;
	Param<double> openProb;
	Param<double> sweepRateFactor;
	Param<int> rndBufferSize;
	Param<int> numJqJqPoints;
	Param<double> LAngs, rZeroFactor;
	Param<double> rZero;
	// PT simulation number
	Param<int> confNum;
	Param<bool> flag;
	Param<double> greenFactorF, greenFactorV;
	Param<double> gamma;
	Param<int> step;
	Param<int> rateOpen;
	Param<int> rateClosed;
	Param<double> rZeroScale;
	Param<double> latticeStrength;
	Param<double> period;
	Param<int> numOfVacancies;
	Param<int> numOfInterstitals;
	Param<double> disXi;
	Param<double> disStrength;
	Param<string> latFileInit;
	Param<string> initialType;
	Param<double> dumpTime;
	Param<double> finishTime;
	Param<int> numOfCores;
	Param<int> gridResExtPot;
	Param<int> numOfBins;
	Param<int> numOfKvecSym;
	Param<int> binSize;
	Param<double> endTime;
	Param<double> deltaDump;
	Param<int> numGreenPoints;
	Param<double> rm;
	Param<vector<PhononBranch> > phononBranch;
	Param<double> COMdr;
	Param<int> num_of_Bragg;
	Param<int> BraggSize;
	// True - 2\times \Delta \tau False- \Delta \tau
	Param<bool> dtRatio;

};


struct SimulationParameters {
	double sigma; // RW variance
	double gamma; // Open Close Ratio
	int mbarOpen;
	int mbarSwap;
	int mbarClose;
	int mbarWiggle;
	int step;
	statistics stats_;
};

#endif /* QMCPARAMETERS_H_ */
