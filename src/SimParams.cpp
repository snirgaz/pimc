//
//  SimParams.cpp
//  WORM_AL_PT
//
//  Created by Snir Gazit on 8/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "SimParams.h"
//void MoveParams::dumpBeads(String fileName) {
//	String ds_name;
//	hdf5RW hdf5File;
//	hdf5File.initNew(fileName.c_str());
//	int rank = 2;
//	int m = 0;
//	if (beads.size() > 0) {
//		Array2D<double> RW(SIM_DIM, beads.size());
//		std::for_each(beads.begin(), beads.end(),
//				[&](Bead bead)
//				{	for (int d=0;d<SIM_DIM;d++) RW(d,m)=bead.beadPos_.getD(d); m++;});
//		// Open File
//		// WL
//		DimsHdf5 wlDims = boost::assign::list_of(SIM_DIM)(beads.size());
//		vector<double>::iterator pos = RW.data();
//		hdf5File.writeComp<double>("/RW", wlDims, &(*pos));
//	}
//	// Delta
//	BeadList::iterator it_b = std::find_if(beads.begin(), beads.end(),
//			Bead::isJump);
//	if (it_b != beads.end()) {
//		double jumpPos[SIM_DIM];
//		for (int d = 0; d < SIM_DIM; d++)
//			jumpPos[d] = it_b->jumpPos_.getD(d);
//		DimsHdf5 wlDims;
//		wlDims.push_back(SIM_DIM);
//		hdf5File.writeComp<double>("/JumpPos", wlDims, jumpPos);
//	}
//	hdf5File.writeSingle<int>("/Tau", tau + 1);
//	hdf5File.writeSingle<int>("/WLStart", wlStart + 1);
//	hdf5File.writeSingle<int>("/WLEnd", wlEnd + 1);
//	hdf5File.writeSingle<int>("/mStart", mStart + 1);
//	hdf5File.writeSingle<int>("/mEnd", mEnd + 1);
//	hdf5File.writeSingle<int>("/wlSwapedStart", wlSwapedStart + 1);
//	hdf5File.writeSingle<int>("/wlSwapedEnd", wlSwapedEnd + 1);
//}
