#include "MoveParams.h"


void MoveParams::init(QmcParameters qmcParameters) {
	beads.resize(qmcParameters.mbar.getVal());
#ifdef CENTER_OF_MASS_MOVE
	forecUpdate.init(qmcParameters.M.getVal(),qmcParameters.NumP.getVal());
#else
	forecUpdate.init(qmcParameters.mbar.getVal(),qmcParameters.NumP.getVal());
#endif
}

void MoveParams::dumpBeads(string fileName) {
}
#ifdef PYTHON
boost::python::object MoveParams::toPython() {
	namespace bp= boost::python;
	bp::dict moveparams;
	moveparams["mStart"]=mStart;
	moveparams["mEnd"]=mEnd;
	moveparams["wlStart"]=wlStart;
	moveparams["wlEnd"]=wlEnd;
	moveparams["tau"]=tau;
	moveparams["mbar"]=mbar;
	moveparams["moveType"]=moveType;
	moveparams["accepted"]=accepted;
	moveparams["force"]=forecUpdate.toPython();
	bp::list beads_list;
	for (BeadVec::iterator it_b=beads.begin();it_b!=beads.end();it_b++){
		beads_list.append(it_b->toPython());
	}
	moveparams["beads"]=beads_list;
	return moveparams;

}
#endif
