/*
 * move.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: snirgaz
 */

#include "move.h"

Move::Move() {
	// TODO Auto-generated constructor stub

}

bool Move::accept(double p) {
	if (p > 1.)
		return true;
	if (uniZeroOne_.draw() < p)
		return true;
	return false;
}
/*
 *
 *
 *  Open Move
 *
 *
 *
 */

void OpenMove::init(QmcParameters qmcParams, SimulationParameters *simParams,
		WorldLines * wl, Bisection * bisection, SimGen *rngGen,
		MoveParams *moveParams) {
	// Initialize Random Numbers
	// Jump Position 0 --> mbar Open
	posOpen_.init(rngGen, UniformInt::param_type(1, simParams->mbarOpen - 2));
	// Starting Tau 0 --> M
	mStart_.init(rngGen, UniformInt::param_type(0, qmcParams.M.getVal() - 1));
	// Particle Number  0 --> N
	particleNum_.init(rngGen,
			UniformInt::param_type(0, qmcParams.NumP.getVal() - 1));
	// Acceptance Prob 0..1
	uniZeroOne_.init(rngGen, UniformReal::param_type(0., 1.));
	// Set initial mbar
	// Needed ???  moveParams_.mbar = simParams->mbarOpen.getVal();
	simParams_ = simParams;
	bisection_ = bisection;
	wl_ = wl;
	moveParams_ = moveParams;
	M_ = qmcParams.M.getVal();
}
bool OpenMove::makeMove() {
	double p;
///////////////////////
// Set Move Parameters
//////////////////////
	moveParams_->allBeads.clear();
// 	Mbar
	moveParams_->mbar = simParams_->mbarOpen;
// Energy Diff
	moveParams_->energyDiff.zero();
// Draw Starting Time
	moveParams_->mStart = mStart_.draw();
// Draw Particle Number
	moveParams_->wlStart = particleNum_.draw();
// Set End time slice
	moveParams_->mEnd = ((moveParams_->mStart + moveParams_->mbar - 1) % M_);
// Set End World Line
	moveParams_->wlEnd =
			(moveParams_->mEnd > moveParams_->mStart) ?
					(moveParams_->wlStart) :
					(wl_->wlData_.loops_[moveParams_->wlStart]);
// Sets thes
	moveParams_->beads.resize(moveParams_->mbar);
///////////////////////////////
// Rho Open Closed
///////////////////////////////
	double rhoOpenClose = rhoOpenClosed();
	p = 1 / (rhoOpenClose * simParams_->gamma);
	if (!accept(p)) {
		simParams_->stats_.Rho(0.);
		return false;
	} else {
		simParams_->stats_.Rho(1.);
	}

///////////////////////////////
// Set Start End and Jump Beads
///////////////////////////////

//Start Bead
	Bead &beadStart = moveParams_->beads[0];
	beadStart.set(moveParams_->wlStart, 0, moveParams_->mStart);
	beadStart.beadPos_ = wl_->wlData_.confData_(beadStart.WLSlice_,
			beadStart.n_);
	moveParams_->allBeads.push_back(0);
// End Bead
	// Check if Beads vector is long enough
	assert(moveParams_->mbar <= moveParams_->beads.size());
	Bead &beadEnd = moveParams_->beads[moveParams_->mbar - 1];
	beadEnd.set(moveParams_->wlEnd, moveParams_->mbar - 1, moveParams_->mEnd);
	beadEnd.beadPos_ = wl_->wlData_.confData_(beadEnd.WLSlice_, beadEnd.n_);
	moveParams_->allBeads.push_back(moveParams_->mbar - 1);
// Tau Bead
	moveParams_->tau = posOpen_.draw();
	Bead &beadTau = moveParams_->beads[moveParams_->tau];
	beadTau.RWSlice_ = moveParams_->tau;
	beadTau.WLSlice_ = (moveParams_->mStart + beadTau.RWSlice_) % M_;
	beadTau.n_ =
			(beadTau.WLSlice_ > moveParams_->mStart) ?
					(moveParams_->wlStart) : (moveParams_->wlEnd);
	moveParams_->allBeads.insert(++moveParams_->allBeads.begin(),
			moveParams_->tau);
	assert(
			((beadTau.WLSlice_ != beadEnd.WLSlice_) && (beadTau.WLSlice_ != beadStart.WLSlice_)));
	assert(
			(beadTau.RWSlice_ > 0) && (beadTau.RWSlice_ < (moveParams_->mbar - 1)));
	bool isAccepted = bisection_->generateRWDMove();
	if (isAccepted) {
		simParams_->stats_.Boltzman[moveParams_->moveType](1.);
		updateConf();
	} else {
		simParams_->stats_.Boltzman[moveParams_->moveType](0.);
	}
	return isAccepted;
}

double OpenMove::rhoOpenClosed() {
	double sigmaMove = simParams_->sigma * sqrt(double(moveParams_->mbar - 1));
	double C = -0.5 / (sigmaMove * sigmaMove);
	ParticlePos start, end, delta;
	start = wl_->wlData_.confData_(moveParams_->mStart, moveParams_->wlStart);
	end = wl_->wlData_.confData_(moveParams_->mEnd, moveParams_->wlEnd);
	delta = end - start;
	double drSqr = delta.sumOfSqures();
	return exp(C * drSqr);
}
void OpenMove::updateConf() {
	for (vector<Bead>::iterator it_b = moveParams_->beads.begin();
			boost::next(it_b) != moveParams_->beads.end(); it_b++) {
		ParticlePos deltaO, deltaP;
		vector<Bead>::iterator it_b_next = boost::next(it_b);
		if (it_b->originalJump_ && wl_->wlData_.isOpenClosed_ == OPEN)
			deltaO = wl_->wlData_.confData_(it_b_next->WLSlice_, it_b_next->n_)
					- wl_->wlData_.jumpPos_;
		else
			deltaO = wl_->wlData_.confData_(it_b_next->WLSlice_, it_b_next->n_)
					- wl_->wlData_.confData_(it_b->WLSlice_, it_b->n_);
		if (it_b->proposedJump_)
			deltaP = it_b_next->beadPos_ - it_b->jumpPos_;
		else
			deltaP = it_b_next->beadPos_ - it_b->beadPos_;
		wl_->wlEnergy_.energyK_ += deltaP.sumOfSqures() - deltaO.sumOfSqures();
	}
	for (vector<Bead>::iterator it_b = boost::next(moveParams_->beads.begin());
			boost::next(it_b) != moveParams_->beads.end(); it_b++) {
		Bead &cBead = *it_b;
		// Update wl
		wl_->wlData_.confData_(cBead.WLSlice_, cBead.n_) = cBead.beadPos_;
#ifndef POT_FREE
#ifdef FORCE
		if (cBead.isOdd) {
			// Update Force
			wl_->wlForce_.forceData_(cBead.WLSlice_, Range::all()) =
					(moveParams_->forecUpdate.forceData_(cBead.RWSlice_,
							Range::all())).copy();
			wl_->wlForce_.totalForce_[cBead.WLSlice_] =
					moveParams_->forecUpdate.totalForce_[cBead.RWSlice_];
		}
#endif
#endif
		// Jump?
		if (cBead.proposedJump_) {
			wl_->wlData_.jumpPos_ = cBead.jumpPos_;
#ifndef POT_FREE
#ifdef FORCE
			if (cBead.isOdd) {
				// Update Force
				wl_->wlForce_.jumpForceData_ =
						moveParams_->forecUpdate.jumpForceData_.copy();
				wl_->wlForce_.totalForceJump_ =
						moveParams_->forecUpdate.totalForceJump_;
			}
#endif
#endif
			// Update Jump Data
			wl_->wlData_.delta_ = cBead.jumpPos_ - cBead.beadPos_;
			wl_->wlData_.wormSlice_ = cBead.WLSlice_;
			wl_->wlData_.wormNum_ = cBead.n_;
		}
	}
	wl_->wlData_.isOpenClosed_ = OPEN;
	// Update Energy
	wl_->wlEnergy_.update(moveParams_->energyDiff);
}
/*
 *
 *
 *  Wiggle Move
 *
 *
 *
 */

void WiggleMove::init(QmcParameters qmcParameters,
		SimulationParameters *simParams, WorldLines * wl, Bisection * bisection,
		SimGen *rngGen, MoveParams *moveParams) {
	// Initialize Random Numbers
	// Jump Position 0 --> mbar Open
	posWiggle_.init(rngGen, UniformInt::param_type(1, simParams->mbarOpen - 2));
	// Acceptance Prob 0..1
	uniZeroOne_.init(rngGen, UniformReal::param_type(0., 1.));
	// Set initial mbar
	// Needed ???  moveParams_.mbar = qmcParams->mbarOpen.getVal();
	simParams_ = simParams;
	moveParams_ = moveParams;
	wl_ = wl;
	bisection_ = bisection;
	M_ = qmcParameters.M.getVal();

}
bool WiggleMove::makeMove() {
	double p;
	/////////////////////////
	//// Set Move Parameters
	////////////////////////
	moveParams_->energyDiff.zero();
	moveParams_->allBeads.clear();
	moveParams_->mbar = simParams_->mbarWiggle;
	moveParams_->beads.resize(moveParams_->mbar);
	moveParams_->wlStart = wl_->wlData_.wormNum_;
	moveParams_->mStart = wl_->wlData_.wormSlice_ - posWiggle_.draw();
	moveParams_->mStart =
			(moveParams_->mStart >= 0) ?
					(moveParams_->mStart) : (M_ + moveParams_->mStart);
	if (moveParams_->mStart > wl_->wlData_.wormSlice_)
		moveParams_->wlStart = wl_->wlData_.findPreviousLoop(
				moveParams_->wlStart);
	moveParams_->mEnd = ((moveParams_->mStart + moveParams_->mbar - 1) % M_);
	moveParams_->wlEnd =
			(moveParams_->mEnd > moveParams_->mStart) ?
					(moveParams_->wlStart) :
					(wl_->wlData_.loops_[moveParams_->wlStart]);
	/////////////////////////////////
	//// Set Start End and Jump Beads
	/////////////////////////////////
	//Start Bead
	Bead &beadStart = moveParams_->beads[0];
	beadStart.set(moveParams_->wlStart, 0, moveParams_->mStart);
	beadStart.beadPos_ = wl_->wlData_.confData_(beadStart.WLSlice_,
			beadStart.n_);
	moveParams_->allBeads.push_back(0);
	// End Bead
	// Check if Beads vector is long enough
	assert(moveParams_->mbar <= moveParams_->beads.size());
	Bead &beadEnd = moveParams_->beads[moveParams_->mbar - 1];
	beadEnd.set(moveParams_->wlEnd, moveParams_->mbar - 1, moveParams_->mEnd);
	beadEnd.beadPos_ = wl_->wlData_.confData_(beadEnd.WLSlice_, beadEnd.n_);
	moveParams_->allBeads.push_back(moveParams_->mbar - 1);
	// Tau Bead
	moveParams_->tau = posWiggle_.draw();
	Bead &beadTau = moveParams_->beads[moveParams_->tau];
	beadTau.RWSlice_ = moveParams_->tau;
	beadTau.WLSlice_ = (moveParams_->mStart + beadTau.RWSlice_) % M_;
	beadTau.n_ =
			(beadTau.WLSlice_ > moveParams_->mStart) ?
					(moveParams_->wlStart) : (moveParams_->wlEnd);
	moveParams_->allBeads.insert(++moveParams_->allBeads.begin(),
			moveParams_->tau);
	assert(
			(beadTau.WLSlice_ != beadEnd.WLSlice_) && (beadTau.WLSlice_ != beadStart.WLSlice_));
	bool isAccepted = bisection_->generateRWDMove();
	if (isAccepted) {
		updateConf();
		simParams_->stats_.Boltzman[moveParams_->moveType](1.);
	} else {
		simParams_->stats_.Boltzman[moveParams_->moveType](0.);
	}
	return isAccepted;

}

void WiggleMove::updateConf() {
	assert(
			count_if(moveParams_->beads.begin(), moveParams_->beads.end(), (boost::lambda::bind(&Bead::originalJump_, boost::lambda::_1))) <= 1);
	assert(
			count_if(moveParams_->beads.begin(), moveParams_->beads.end(), (boost::lambda::bind(&Bead::proposedJump_, boost::lambda::_1))) <= 1);
	for (vector<Bead>::iterator it_b = moveParams_->beads.begin();
			boost::next(it_b) != moveParams_->beads.end(); it_b++) {
		ParticlePos deltaO, deltaP;
		vector<Bead>::iterator it_b_next = boost::next(it_b);
		if (it_b->originalJump_ && wl_->wlData_.isOpenClosed_ == OPEN)
			deltaO = wl_->wlData_.confData_(it_b_next->WLSlice_, it_b_next->n_)
					- wl_->wlData_.jumpPos_;
		else
			deltaO = wl_->wlData_.confData_(it_b_next->WLSlice_, it_b_next->n_)
					- wl_->wlData_.confData_(it_b->WLSlice_, it_b->n_);
		if (it_b->proposedJump_)
			deltaP = it_b_next->beadPos_ - it_b->jumpPos_;
		else
			deltaP = it_b_next->beadPos_ - it_b->beadPos_;
		wl_->wlEnergy_.energyK_ += deltaP.sumOfSqures() - deltaO.sumOfSqures();
	}
//	cout
//			<< wl_->wlData_.confData_(wl_->wlData_.wormSlice_,
//					wl_->wlData_.wormNum_) << endl;
//	cout << wl_->wlData_.jumpPos_ << endl;

	for (vector<Bead>::iterator it_b = boost::next(moveParams_->beads.begin());
			boost::next(it_b) != moveParams_->beads.end(); it_b++) {
		Bead &cBead = *it_b;
		// Update wl
		wl_->wlData_.confData_(cBead.WLSlice_, cBead.n_) = cBead.beadPos_;
#ifndef POT_FREE
#ifdef FORCE
		if (cBead.isOdd) {
			// Update Force
			wl_->wlForce_.forceData_(cBead.WLSlice_, Range::all()) =
					moveParams_->forecUpdate.forceData_(cBead.RWSlice_,
							Range::all()).copy();
			wl_->wlForce_.totalForce_[cBead.WLSlice_] =
					moveParams_->forecUpdate.totalForce_[cBead.RWSlice_];
		}
#endif
#endif
		// Jump?
		if (cBead.proposedJump_) {
			wl_->wlData_.jumpPos_ = cBead.jumpPos_;
#ifndef POT_FREE
#ifdef FORCE
			if (cBead.isOdd) {
				// Update Force
				wl_->wlForce_.jumpForceData_ =
						moveParams_->forecUpdate.jumpForceData_.copy();
				wl_->wlForce_.totalForceJump_ =
						moveParams_->forecUpdate.totalForceJump_;
			}
#endif
#endif
			// Update Jump Data
			wl_->wlData_.delta_ = cBead.jumpPos_ - cBead.beadPos_;
			wl_->wlData_.wormSlice_ = cBead.WLSlice_;
			wl_->wlData_.wormNum_ = cBead.n_;
		}
	}
//	wl_->wlData_.isOpenClosed_ = OPEN;
	// Update Energy
	wl_->wlEnergy_.update(moveParams_->energyDiff);
//	cout
//			<< wl_->wlData_.confData_(wl_->wlData_.wormSlice_,
//					wl_->wlData_.wormNum_) << endl;
//	cout << wl_->wlData_.jumpPos_ << endl;

}

#ifdef SWAP
/*
 *
 *
 *  Swap Move
 *
 *
 *
 */

void SwapMove::init(QmcParameters qmcParameters,
		SimulationParameters *simParams, WorldLines * wl, Bisection * bisection,
		SimGen *rngGen, MoveParams *moveParams) {
	// Initialize Random Numbers
	// Jump Position 0 --> mbar Open
	// Needed ???  moveParams_.mbar = qmcParams->mbarOpen.getVal();
	simParams_ = simParams;
	moveParams_ = moveParams;
	wl_ = wl;
	bisection_ = bisection;
	numP_ = wl_->numP_;
	M_ = qmcParameters.M.getVal();
	rho_.resize(wl_->numP_);
	uniZeroOne_.init(rngGen, UniformReal::param_type(0., 1.));

}

bool SwapMove::makeMove() {
	double p;
//	int temp_wlStart, temp_wlEnd;
	moveParams_->energyDiff.zero();
	moveParams_->allBeads.clear();
	moveParams_->mbar = simParams_->mbarSwap;
	moveParams_->beads.resize(moveParams_->mbar);
	wlOriginalBegin_ = wl_->wlData_.wormNum_;
	moveParams_->mStart = wl_->wlData_.wormSlice_;
	moveParams_->mEnd = ((moveParams_->mStart + moveParams_->mbar - 1) % M_);
	wlOriginalEnd_ =
			(moveParams_->mEnd > moveParams_->mStart) ?
					(wlOriginalBegin_) :
					(wl_->wlData_.loops_[wlOriginalBegin_]);
	// Check if Swap Same and Draw wl_swap calculate swap ration
	bool swapSame = rhoSwap();
	if (swapSame) {
		simParams_->stats_.swapSame(0.);
		//stats_.swapSame++;
		return false;
	}
	simParams_->stats_.swapSame(1.);
	// The worm's world line was chosen for swap
	p = swapRatio_;
	if (!accept(p)) {
		simParams_->stats_.Swap(0.);
		return false;
	}
	simParams_->stats_.Swap(1.);

	bool isAcceptedSwap = bisection_->calcSwap(wlOriginalBegin_,
			wlSwapToBegin_);
	if (!isAcceptedSwap)
		return false;
	///////////////////////////////
	// Set Start and End Beads
	///////////////////////////////
	//Start Bead
	Bead &beadStart = moveParams_->beads[0];
	// Should update The swapped WL
	beadStart.set(moveParams_->wlStart, 0, moveParams_->mStart);
	// Take starting position from original WL !!!
	beadStart.beadPos_ = wl_->wlData_.confData_(beadStart.WLSlice_,
			wlOriginalBegin_);
	moveParams_->allBeads.push_back(0);
	// End Bead
	// Check if Beads vector is long enough
	assert(moveParams_->mbar <= moveParams_->beads.size());
	Bead &beadEnd = moveParams_->beads[moveParams_->mbar - 1];
	// Should update Swapped WL
	beadEnd.set(moveParams_->wlEnd, moveParams_->mbar - 1, moveParams_->mEnd);
	// Taking last bead from swapped line (updated in rhoSwap())
	beadEnd.beadPos_ = wl_->wlData_.confData_(beadEnd.WLSlice_, beadEnd.n_);
	moveParams_->allBeads.push_back(moveParams_->mbar - 1);
	bool isAccepted = bisection_->generateBBMove();
	if (isAccepted) {
		updateConf();
		simParams_->stats_.Boltzman[moveParams_->moveType](1.);
	} else {
		simParams_->stats_.Boltzman[moveParams_->moveType](0.);
	}
	return isAccepted;

}

bool SwapMove::rhoSwap() {
	assert(rho_.size() == wl_->numP_);
	double normOriginal, normProposed, draw, sum;
	int wlSwapToEnd;
// Calculate Sigma Swap Original
	sigmaSwap(wlOriginalBegin_);
// Total for nomalization
	normOriginal = std::accumulate(rho_.begin(), rho_.end(), 0.,
			std::plus<double>());
// Draw Swap WL
	draw = uniZeroOne_.draw();
	draw *= normOriginal;
// self swap check
	if (rho_[wlOriginalEnd_] > draw)
		return true;
// rest of world lines
	sum = rho_[wlOriginalEnd_];
	rho_[wlOriginalEnd_] = 0.;
	for (wlSwapToEnd = 0; wlSwapToEnd < numP_; wlSwapToEnd++) {
		sum += rho_[wlSwapToEnd];
		if (sum > draw)
			break;
	}
// Find wl swap start and end
	wlSwapToEnd_ = wlSwapToEnd;
	wlSwapToBegin_ =
			(moveParams_->mEnd > moveParams_->mStart) ?
					(wlSwapToEnd) :
					(wl_->wlData_.findPreviousLoop(wlSwapToEnd));
	moveParams_->wlStart = wlSwapToBegin_;
	moveParams_->wlEnd = wlSwapToEnd_;
//	assert();
// Calculate Sigma Swap Proposed
	this->sigmaSwap(moveParams_->wlStart);
	normProposed = std::accumulate(rho_.begin(), rho_.end(), 0.,
			std::plus<double>());
	swapRatio_ = (normOriginal / normProposed);
	return false;
}
void SwapMove::sigmaSwap(int original) {
	assert(moveParams_->mbar == simParams_->mbarSwap);
	double sigmaMove = simParams_->sigma * sqrt(double(moveParams_->mbar - 1));
	double C = -0.5 * pow(sigmaMove, -2);
	ParticlePos originalBead = wl_->wlData_.confData_(moveParams_->mStart,
			original);
	for (int n = 0; n < rho_.size(); n++) {
		ParticlePos delta = wl_->wlData_.confData_(moveParams_->mEnd, n)
				- originalBead;
		rho_[n] = exp(C * delta.sumOfSqures());
	}
}
void SwapMove::updateConf() {
	for (vector<Bead>::iterator it_b = moveParams_->beads.begin();
			boost::next(it_b) != moveParams_->beads.end(); it_b++) {
		ParticlePos deltaO, deltaP;
		vector<Bead>::iterator it_b_next = boost::next(it_b);
		deltaO = wl_->wlData_.confData_(it_b_next->WLSlice_, it_b_next->n_)
				- wl_->wlData_.confData_(it_b->WLSlice_, it_b->n_);
		deltaP = it_b_next->beadPos_ - it_b->beadPos_;
		wl_->wlEnergy_.energyK_ += deltaP.sumOfSqures() - deltaO.sumOfSqures();
	}
	// Update Beads
	if (moveParams_->mEnd > moveParams_->mStart) {
		// Case I - do not cross beta
		for (vector<Bead>::iterator it_b = boost::next(
				moveParams_->beads.begin());
				boost::next(it_b) != moveParams_->beads.end(); it_b++) {
			Bead &cBead = *it_b;
			// Update WL
			// copy original ( never cross beta always wlSwapToBegin_!!!)
			wl_->wlData_.confData_(cBead.WLSlice_, wlSwapToBegin_) =
					wl_->wlData_.confData_(cBead.WLSlice_, wlOriginalBegin_);
			// update original
			wl_->wlData_.confData_(cBead.WLSlice_, wlOriginalBegin_) =
					cBead.beadPos_;
#ifndef POT_FREE
#ifdef FORCE
			if (cBead.isOdd) {
				// Update Force
				wl_->wlForce_.forceData_(cBead.WLSlice_, Range::all()) =
						moveParams_->forecUpdate.forceData_(cBead.RWSlice_,
								Range::all()).copy();
				// Swap Forces
				swap(wl_->wlForce_.forceData_(cBead.WLSlice_, wlOriginalBegin_),
						wl_->wlForce_.forceData_(cBead.WLSlice_,
								wlSwapToBegin_));
				wl_->wlForce_.totalForce_[cBead.WLSlice_] =
						moveParams_->forecUpdate.totalForce_[cBead.RWSlice_];
			}
#endif
#endif
		}
		// Swap Till beta
		for (int i = moveParams_->mEnd; i < M_; i++) {
			// Update WL
			// swap
			swap(wl_->wlData_.confData_(i, wlSwapToBegin_),
					wl_->wlData_.confData_(i, wlOriginalBegin_));
#ifndef POT_FREE
#ifdef FORCE
			if (i % 2) {
				// Update Force
				// Swap Forces
				swap(wl_->wlForce_.forceData_(i, wlOriginalBegin_),
						wl_->wlForce_.forceData_(i, wlSwapToBegin_));
			}
#endif
#endif
		}
	} else {
		// Case II - cross beta
		// before crossing
		for (int i = 1; i < moveParams_->mbar - 1; i++) {
			Bead &cBead = moveParams_->beads[i];
			// Before beta cross
			if (cBead.WLSlice_ > moveParams_->mStart) {
				// Update WL
				// copy original ( never cross beta always wlSwapToBegin_!!!)
				wl_->wlData_.confData_(cBead.WLSlice_, wlSwapToBegin_) =
						wl_->wlData_.confData_(cBead.WLSlice_,
								wlOriginalBegin_);
				// update original
				wl_->wlData_.confData_(cBead.WLSlice_, wlOriginalBegin_) =
						cBead.beadPos_;
#ifndef POT_FREE
#ifdef FORCE
				if (cBead.isOdd) {
					// Update Force
					wl_->wlForce_.forceData_(cBead.WLSlice_, Range::all()) =
							moveParams_->forecUpdate.forceData_(cBead.RWSlice_,
									Range::all()).copy();
					// Swap Forces
					swap(
							wl_->wlForce_.forceData_(cBead.WLSlice_,
									wlOriginalBegin_),
							wl_->wlForce_.forceData_(cBead.WLSlice_,
									wlSwapToBegin_));
					wl_->wlForce_.totalForce_[cBead.WLSlice_] =
							moveParams_->forecUpdate.totalForce_[i];
				}
#endif
#endif
			} else
			// After beta Cross
			{
				// Update WL
				// update original
				// crossed beta hence wlOriginalEnd_
				wl_->wlData_.confData_(cBead.WLSlice_, wlSwapToEnd_) =
						cBead.beadPos_;
#ifndef POT_FREE
#ifdef FORCE
				if (cBead.isOdd) {
					// Update Force
					wl_->wlForce_.forceData_(cBead.WLSlice_, Range::all()) =
							moveParams_->forecUpdate.forceData_(cBead.RWSlice_,
									Range::all()).copy();
					wl_->wlForce_.totalForce_[cBead.WLSlice_] =
							moveParams_->forecUpdate.totalForce_[i];
				}
#endif
#endif

			}
		}
	}
	// Update Jump Force
	if (moveParams_->mStart % 2) {
		wl_->wlForce_.jumpForceData_ =
				moveParams_->forecUpdate.jumpForceData_.copy();
		wl_->wlForce_.totalForceJump_ =
				moveParams_->forecUpdate.totalForceJump_;
	}

	// Update Loops
	swap(wl_->wlData_.loops_[wlOriginalBegin_],
			wl_->wlData_.loops_[wlSwapToBegin_]);
	// Update Jump Position
	wl_->wlData_.wormNum_ = moveParams_->wlStart;
	wl_->wlData_.delta_ = wl_->wlData_.jumpPos_
			- wl_->wlData_.confData_(wl_->wlData_.wormSlice_,
					wl_->wlData_.wormNum_);
	// Update Energy
	// Update Energy
	wl_->wlEnergy_.update(moveParams_->energyDiff);
}
#endif
/*
 *
 *
 *  Close Move
 *
 *
 *
 */
void CloseMove::init(QmcParameters qmcParameters,
		SimulationParameters *simParams, WorldLines * wl, Bisection * bisection,
		SimGen *rngGen, MoveParams *moveParams) {
	wl_ = wl;
	bisection_ = bisection;
	// Initialize Random Numbers
	// Jump Position 0 --> mbar Open
	posClose_.init(rngGen, UniformInt::param_type(1, simParams->mbarOpen - 2));
	// Acceptance Prob 0..1
	uniZeroOne_.init(rngGen, UniformReal::param_type(0., 1.));
	// Set initial mbar
	// Needed ???  moveParams_.mbar = simParams->mbarOpen.getVal();
	simParams_ = simParams;
	moveParams_ = moveParams;
	M_ = qmcParameters.M.getVal();
}
bool CloseMove::makeMove() {
	double p;
///////////////////////
// Set Move Parameters
//////////////////////
	moveParams_->allBeads.clear();
	moveParams_->energyDiff.zero();
	moveParams_->mbar = simParams_->mbarOpen;
	moveParams_->beads.resize(moveParams_->mbar);
	moveParams_->wlStart = wl_->wlData_.wormNum_;
	moveParams_->mStart = wl_->wlData_.wormSlice_ - posClose_.draw();
	moveParams_->mStart =
			(moveParams_->mStart >= 0) ?
					(moveParams_->mStart) : (M_ + moveParams_->mStart);
	if (moveParams_->mStart > wl_->wlData_.wormSlice_)
		moveParams_->wlStart = wl_->wlData_.findPreviousLoop(
				moveParams_->wlStart);
	moveParams_->mEnd = ((moveParams_->mStart + moveParams_->mbar - 1) % M_);
	moveParams_->wlEnd =
			(moveParams_->mEnd > moveParams_->mStart) ?
					(moveParams_->wlStart) :
					(wl_->wlData_.loops_[moveParams_->wlStart]);
///////////////////////////////
// Rho Open Closed
///////////////////////////////
	double rhoOpenClose = rhoOpenClosed();
	// Inverse to Open Check!!!
	p = (rhoOpenClose * simParams_->gamma);
	if (!accept(p)) {
		simParams_->stats_.Rho(0.);
		return false;
	} else {
		simParams_->stats_.Rho(1.);
	}
///////////////////////////////
// Set Start End and Jump Beads
///////////////////////////////

//Start Bead
	Bead &beadStart = moveParams_->beads[0];
	beadStart.set(moveParams_->wlStart, 0, moveParams_->mStart);
	beadStart.beadPos_ = wl_->wlData_.confData_(beadStart.WLSlice_,
			beadStart.n_);
	moveParams_->allBeads.push_back(0);
// End Bead
	// Check if Beads vector is long enough
	assert(moveParams_->mbar <= moveParams_->beads.size());
	Bead &beadEnd = moveParams_->beads[moveParams_->mbar - 1];
	beadEnd.set(moveParams_->wlEnd, moveParams_->mbar - 1, moveParams_->mEnd);
	beadEnd.beadPos_ = wl_->wlData_.confData_(beadEnd.WLSlice_, beadEnd.n_);
	moveParams_->allBeads.push_back(moveParams_->mbar - 1);
	bool isAccepted = bisection_->generateBBMove();
	if (isAccepted) {
		updateConf();
		simParams_->stats_.Boltzman[moveParams_->moveType](1.);
	} else {
		simParams_->stats_.Boltzman[moveParams_->moveType](0.);
	}
	return isAccepted;
}

double CloseMove::rhoOpenClosed() {
	double sigmaMove = simParams_->sigma * sqrt(double(moveParams_->mbar - 1));
	double C = -0.5 / (sigmaMove * sigmaMove);
	ParticlePos start, end, delta;
	start = wl_->wlData_.confData_(moveParams_->mStart, moveParams_->wlStart);
	end = wl_->wlData_.confData_(moveParams_->mEnd, moveParams_->wlEnd);
	delta = end - start;
	double drSqr = delta.sumOfSqures();
	return exp(C * drSqr);
}
void CloseMove::updateConf() {
	for (vector<Bead>::iterator it_b = moveParams_->beads.begin();
			boost::next(it_b) != moveParams_->beads.end(); it_b++) {
		ParticlePos deltaO, deltaP;
		vector<Bead>::iterator it_b_next = boost::next(it_b);
		if (it_b->originalJump_ && wl_->wlData_.isOpenClosed_ == OPEN)
			deltaO = wl_->wlData_.confData_(it_b_next->WLSlice_, it_b_next->n_)
					- wl_->wlData_.jumpPos_;
		else
			deltaO = wl_->wlData_.confData_(it_b_next->WLSlice_, it_b_next->n_)
					- wl_->wlData_.confData_(it_b->WLSlice_, it_b->n_);
		if (it_b->proposedJump_)
			deltaP = it_b_next->beadPos_ - it_b->jumpPos_;
		else
			deltaP = it_b_next->beadPos_ - it_b->beadPos_;
		wl_->wlEnergy_.energyK_ += deltaP.sumOfSqures() - deltaO.sumOfSqures();
	}
	for (vector<Bead>::iterator it_b = boost::next(moveParams_->beads.begin());
			boost::next(it_b) != moveParams_->beads.end(); it_b++) {
		Bead &cBead = *it_b;
		// Update wl
		wl_->wlData_.confData_(cBead.WLSlice_, cBead.n_) = cBead.beadPos_;
#ifndef POT_FREE
#ifdef FORCE
		if (cBead.isOdd) {
			// Update Force
			wl_->wlForce_.forceData_(cBead.WLSlice_, Range::all()) =
					moveParams_->forecUpdate.forceData_(cBead.RWSlice_,
							Range::all()).copy();
			wl_->wlForce_.totalForce_[cBead.WLSlice_] =
					moveParams_->forecUpdate.totalForce_[cBead.RWSlice_];
		}
#endif
#endif
	}
	wl_->wlData_.isOpenClosed_ = CLOSED;
	fill(wl_->wlData_.delta_.begin(), wl_->wlData_.delta_.end(), 0.);
	// Update Energy
	wl_->wlEnergy_.update(moveParams_->energyDiff);
}
#ifdef CENTER_OF_MASS_MOVE
/*
 *
 *
 *  Center of mass move
 *
 *
 *
 */
void CenterOfMassMove::init(QmcParameters qmcParameters,
		SimulationParameters *simParams, WorldLines * wl, Bisection * bisection,
		SimGen *rngGen, MoveParams *moveParams) {
	wl_ = wl;
	bisection_ = bisection;
	// Initialize Random Numbers
	// Jump Position 0 --> mbar Open
	rnddr_.init(rngGen, Normal::param_type(0, qmcParameters.COMdr.getVal()));
	partNum_.init(rngGen,
			UniformInt::param_type(0, qmcParameters.NumP.getVal() - 1));

	// Acceptance Prob 0..1
	uniZeroOne_.init(rngGen, UniformReal::param_type(0., 1.));
	// Set initial mbar
	// Needed ???  moveParams_.mbar = simParams->mbarOpen.getVal();
	simParams_ = simParams;
	moveParams_ = moveParams;
	M_ = qmcParameters.M.getVal();
}

bool CenterOfMassMove::makeMove() {
	double p;
	bool sameWL = buildMoveWL();
	bool isAccepted;
	ParticlePos dr;
	moveParams_->energyDiff.zero();
	if (sameWL) {
		for (int d = 0; d < SIM_DIM; d++) {
			dr_[d] = rnddr_.draw();
		}
		isAccepted = bisection_->COMMove(moveWL_, dr_);
		simParams_->stats_.sameCOM(1.);
	} else {
		simParams_->stats_.sameCOM(0.);
		return false;
	}
	if (isAccepted) {
		updateConf();
		simParams_->stats_.Boltzman[moveParams_->moveType](1.);
	} else {
		simParams_->stats_.Boltzman[moveParams_->moveType](0.);
	}
	return isAccepted;
}

bool CenterOfMassMove::buildMoveWL() {
	moveWL_ = partNum_.draw();
	return (wl_->wlData_.loops_[moveWL_] == moveWL_);
}
void CenterOfMassMove::updateConf() {
	for (int m = 0; m < M_; m++) {
		// Update wl
		wl_->wlData_.confData_(m, moveWL_) += dr_;
#ifndef POT_FREE
#ifdef FORCE
		if (m % 2) {
			// Update Force
			wl_->wlForce_.forceData_(m, Range::all()) =
					(moveParams_->forecUpdate.forceData_(m, Range::all())).copy();
			wl_->wlForce_.totalForce_[m] =
					moveParams_->forecUpdate.totalForce_[m];
		}
#endif
#endif
		// Jump?
		if ((wl_->wlData_.isOpenClosed_ == OPEN)
				and (m == wl_->wlData_.wormSlice_)) {
			if (moveWL_ == wl_->wlData_.wormNum_)
				wl_->wlData_.jumpPos_ += dr_;
#ifndef POT_FREE
#ifdef FORCE
			if (m % 2) {
				// Update Force
				wl_->wlForce_.jumpForceData_ =
						moveParams_->forecUpdate.jumpForceData_.copy();
				wl_->wlForce_.totalForceJump_ =
						moveParams_->forecUpdate.totalForceJump_;
			}
#endif
#endif
		}
	}
	// Update Energy
	wl_->wlEnergy_.update(moveParams_->energyDiff);
}
#endif
