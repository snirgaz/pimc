//
//  EnergyData.cpp
//  WORM_AL_PT
//
//  Created by Snir Gazit on 8/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "EnergyData.h"

EnergyData::EnergyData() :
		energy_(0.), energyV_(0.), energyF_(0.) {
}

void EnergyData::update(EnergyData ED) {
	energy_ += ED.energy_;
	energyV_ += ED.energyV_;
	energyF_ += ED.energyF_;
}
void EnergyData::zero() {
	energy_ = 0.;
	energyV_ = 0.;
	energyF_ = 0.;
}
EnergyData EnergyData::mul(double c) {
	EnergyData ED_T;
	ED_T.energy_ = energy_ * c;
	ED_T.energyV_ = energyV_ * c;
	ED_T.energyF_ = energyF_ * c;
	return ED_T;
}
EnergyData EnergyData::diff(EnergyData ED) {
	EnergyData ED_T;
	ED_T.energy_ = this->energy_ - ED.energy_;
	ED_T.energyV_ = this->energyV_ - ED.energyV_;
	ED_T.energyF_ = this->energyF_ - ED.energyF_;
	return ED_T;
}
double EnergyData::getEnergy() const {
	return energy_;
}
double EnergyData::getEnergyV() const {
	return energyV_;
}
double EnergyData::getEnergyF() const {
	return energyF_;
}
void EnergyData::setEnergy(double energy) {
	energy_ = energy;
}
void EnergyData::setEnergyV(double energy) {
	energyV_ = energy;
}
void EnergyData::setEnergyF(double energy) {
	energyF_ = energy;
}
void EnergyData::calcEnergy(double constV, double constF) {
	energy_ = constV * energyV_ + constF * energyF_;
}
void EnergyData::swapData(EnergyData &ED) {
	std::swap(*this, ED);
}
#ifdef PYTHON
boost::python::dict EnergyData::toPython() const {
	boost::python::dict energyData;
	energyData["ev"]=energyV_;
	energyData["ef"]=energyF_;
	energyData["e"]=energy_;
	return energyData;
}
#endif
//////////////

// WL Energy Data

/////////////

WLEnergyData::WLEnergyData() :
		energyK_(0.) {
}
;
double WLEnergyData::getEnergyK() const {
	return energyK_;
}
void WLEnergyData::setEnergyK(double energy) {
	energyK_ = energy;
}
void WLEnergyData::swapData(WLEnergyData &ED) {
	std::swap(*this, ED);
}

void WLEnergyData::updateEnergyK(double energy) {
	energyK_ += energy;
}
void WLEnergyData::setEnergyP(EnergyData &ED) {
	EnergyData &E = static_cast<EnergyData&>(*this);
	E = ED;
}

void EnergyData::dumpToFile(string& fileName) {
}

void EnergyData::readFromFile(string& fileName) {
}

void WLEnergyData::dumpToFile(hdf5RW &hdf5File) const {
	string groupName = "/ENERGY", dsNameTemp;
	hdf5File.createGroup(groupName);
	// Total Energy
	dsNameTemp = groupName + "/E";
	hdf5File.writeSingle<double>(dsNameTemp, energy_);
	// Total EnergyV
	dsNameTemp = groupName + "/EV";
	hdf5File.writeSingle<double>(dsNameTemp, energyV_);

	// Total EnergyF
	dsNameTemp = groupName + "/EF";
	hdf5File.writeSingle<double>(dsNameTemp, energyF_);
	// Total EnergyEK
	dsNameTemp = groupName + "/EK";
	hdf5File.writeSingle<double>(dsNameTemp, energyK_);
}

void WLEnergyData::readFromFile(string &filename) {
	hdf5RW hdf5File;
	hdf5File.initRead(filename);
	string groupName = "/ENERGY", dsNameTemp;
	// Total Energy
	dsNameTemp = groupName + "/E";
	hdf5File.read<double>(dsNameTemp, &energy_);
	// Total EnergyV
	dsNameTemp = groupName + "/EV";
	hdf5File.read<double>(dsNameTemp, &energyV_);

	// Total EnergyF
	dsNameTemp = groupName + "/EF";
	hdf5File.read<double>(dsNameTemp, &energyF_);
	// Total EnergyEK
	dsNameTemp = groupName + "/EK";
	hdf5File.read<double>(dsNameTemp, &energyK_);
}
#ifdef PYTHON
boost::python::dict WLEnergyData::toPython() const {
	boost::python::dict energyData;
	energyData["ek"]=energyK_;
	energyData["ev"]=energyV_;
	energyData["ef"]=energyF_;
	energyData["e"]=energy_;
	return energyData;
}
#endif
