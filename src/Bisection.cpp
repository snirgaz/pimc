/*
 * Bisection.cpp
 *
 *  Created on: Aug 26, 2010
 *      Author: snirgaz
 */

#include "Bisection.h"

void Bisection::init(WorldLines *wl, EnergyCalc * energyCalc,
		SimulationParameters *simParams, MoveParams *moveParams,
		MTGen *RndGen) {
	wl_ = wl;
	moveParams_ = moveParams;
	simParams_ = simParams;
	M_ = wl_->M_;
	uniZeroOne_.init(RndGen, UniformReal::param_type(0., 1.));
	randomWalks_.init(M_, simParams_, RndGen, moveParams);
	energyCalc_ = energyCalc;
}

bool Bisection::generateBBMove() {
	if (this->generateBB())
		return true;
	else
		return false;
}
bool Bisection::generateBB() {
	bool flag;
	// Iterate Levels
	flag = true;
	assert(moveParams_->allBeads.size() <= moveParams_->mbar);
	while ((moveParams_->allBeads.size() < moveParams_->mbar) && (flag)) {
		flag = this->nextLevel();
		list<int>::iterator it_mid = --moveParams_->allBeads.end();
		copy(moveParams_->currentBeads.begin(), moveParams_->currentBeads.end(),
				back_inserter(moveParams_->allBeads));
		inplace_merge(moveParams_->allBeads.begin(), ++it_mid,
				moveParams_->allBeads.end());
//		merge(moveParams_->allBeads.begin(), moveParams_->allBeads.end(),
//				moveParams_->currentBeads.end(),
//				moveParams_->currentBeads.end(), back_inserter(moveParams_->allBeads));
	}
	return flag;
}
bool Bisection::generateRWDMove() {
	double p;
	bool flag;
	EnergyData ED;
	moveParams_->currentBeads.clear();
	moveParams_->calcBeads.clear();
	moveParams_->deltas.clear();
	// Generate Jump
	BeadVec::const_iterator beadStart = moveParams_->beads.begin();
	BeadVec::const_iterator beadEnd = --moveParams_->beads.end();
	BeadVec::iterator beadTau = boost::next(moveParams_->beads.begin(),
			moveParams_->tau);
	// Add Jump Bead
	randomWalks_.generateJumpBeadPos(beadStart, beadEnd, beadTau);
	// Check If Bead Tau is a jump
	beadTau->proposedJump_ = true;
	if ((beadTau->WLSlice_ == wl_->wlData_.wormSlice_)
			&& (wl_->wlData_.isOpenClosed_ == OPEN)) {
		beadTau->originalJump_ = true;

		moveParams_->calcBeads.push_back(
				CalcBead(moveParams_->tau, ORIGINAL, true));

	} else
		beadTau->originalJump_ = false;
	beadTau->isOdd = beadTau->WLSlice_ % 2;
	moveParams_->currentBeads.push_back(moveParams_->tau);
	moveParams_->calcBeads.push_back(
			CalcBead(moveParams_->tau, ORIGINAL, false));
	moveParams_->calcBeads.push_back(
			CalcBead(moveParams_->tau, PROPOSED, false));
	moveParams_->calcBeads.push_back(
			CalcBead(moveParams_->tau, PROPOSED, true));
	assert(
			(beadTau->RWSlice_ > 0) && (beadTau->RWSlice_ < (moveParams_->mbar - 1)));
	// Energy Calc of Jump Bead
	ED = energyCalc_->calcBeadsEnergy();
	p = exp(-ED.getEnergy());
	assert(isfinite(ED.energy_));
	if (!(accept(p)))
		return false;
	moveParams_->energyDiff.update(ED);
	// Left and right parts
	// Left
	flag = this->generateBB();
	return flag;
}
void Bisection::nextLevelBeadPos() {
	list<int>::iterator itb, itb_next;
	int deltaPos;
	double ratio;
	ParticlePos meanNewPos, start, end, delta;
	// Clear Current beads Data
	moveParams_->currentBeads.clear();
	moveParams_->calcBeads.clear();
	moveParams_->deltas.clear();
	itb = moveParams_->allBeads.begin();
	// Generate New Locations
	while (boost::next(itb) != moveParams_->allBeads.end()) {
		// FirstBed
		Bead &aBead = moveParams_->beads[*itb];
		// Second Bead
		Bead &bBead = moveParams_->beads[*boost::next(itb)];
		deltaPos = bBead.RWSlice_ - aBead.RWSlice_;
		// Check if adjucent beads
		if (deltaPos != 1) {
			// New Bead
			Bead &beadCurrent =
					moveParams_->beads[aBead.RWSlice_ + deltaPos / 2];
			beadCurrent.RWSlice_ = aBead.RWSlice_ + deltaPos / 2;
			beadCurrent.WLSlice_ = (aBead.WLSlice_ + deltaPos / 2) % M_;
			beadCurrent.n_ =
					(beadCurrent.WLSlice_ > moveParams_->mStart) ?
							(moveParams_->wlStart) : (moveParams_->wlEnd);
			beadCurrent.isOdd = beadCurrent.WLSlice_ % 2;
			if ((wl_->wlData_.isOpenClosed_ == OPEN)
					&& (beadCurrent.WLSlice_ == wl_->wlData_.wormSlice_)) {
				beadCurrent.originalJump_ = true;
				moveParams_->calcBeads.push_back(
						CalcBead(aBead.RWSlice_ + deltaPos / 2, ORIGINAL,
								true));
			} else
				beadCurrent.originalJump_ = false;
			beadCurrent.proposedJump_ = false;
			// Set Bead Mean pos
			if (aBead.proposedJump_)
				start = aBead.jumpPos_;
			else
				start = aBead.beadPos_;
			end = bBead.beadPos_;
			delta = end - start;
			//end.subBeadPos(delta, start);
			ratio = double(deltaPos / 2) / double(deltaPos);
			delta = ratio * delta;
			//delta.mulBeadPos(ratio);
			meanNewPos = start + delta;
			//start.addBeadPos(meanNewPos, delta);
			beadCurrent.beadPos_ = meanNewPos;
			// Delta List
			moveParams_->deltas.push_back(deltaPos);
			moveParams_->currentBeads.push_back(aBead.RWSlice_ + deltaPos / 2);
			moveParams_->calcBeads.push_back(
					CalcBead(aBead.RWSlice_ + deltaPos / 2, ORIGINAL, false));
			moveParams_->calcBeads.push_back(
					CalcBead(aBead.RWSlice_ + deltaPos / 2, PROPOSED, false));
		}
		itb++;
	}

}
void Bisection::nextLevelRW() {
	randomWalks_.generateLevelPath(moveParams_);
}
bool Bisection::nextLevel() {
	double p;
	EnergyData energy;
	this->nextLevelBeadPos();
	this->nextLevelRW();
	energy = energyCalc_->calcBeadsEnergy();
	p = exp(-energy.getEnergy());
	assert((isfinite(energy.energy_)));
	if (accept(p)) {
		moveParams_->energyDiff.update(energy);
		return true;
	} else
		return false;
}

bool Bisection::calcSwap(int originalWL, int swapToWL) {
	EnergyData energySwap = energyCalc_->calcEnergySwap(originalWL, swapToWL);
	double p = exp(-energySwap.getEnergy());
	assert(isfinite(energySwap.energy_));
	if (accept(p)) {
		moveParams_->energyDiff.update(energySwap);
		return true;
	} else
		return false;
}

bool Bisection::accept(double p) {
	if (p > 1.)
		return true;
	if (uniZeroOne_.draw() < p)
		return true;
	return false;
}
#ifdef CENTER_OF_MASS_MOVE
bool Bisection::COMMove(int moveWL, ParticlePos &dr) {
	EnergyData energy;

	energy = energyCalc_->calcEnergyCOM(moveWL, dr);
	double p = exp(-energy.getEnergy());
	assert(not std::isnan(energy.energy_));
	if (accept(p)) {
		moveParams_->energyDiff.update(energy);
		return true;
	} else
		return false;
}
#endif
