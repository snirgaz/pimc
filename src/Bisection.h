/*
 * bisection.h
 *
 *  Created on: Aug 26, 2010
 *      Author: snirgaz
 */

#ifndef BISECTION_H_
#define BISECTION_H_

#include "HelpLibs/def.h"
#include "HelpLibs/RandomGen.h"
#include "WorldLines.h"
#include "EnergyCalc.h"
#include "RandomWalks.h"
#include "SimParams.h"
#include "MoveParams.h"
#include "HelpLibs/ArrayMultiD.h"
#include <iterator>

enum LeftRight {
	LEFT, RIGHT
};

class Bisection {
private:
	// World Lines Data
	WorldLines *wl_;
	// energy Calculation
	EnergyCalc *energyCalc_;
	// Draw Random Walk
	RandomWalks randomWalks_;
	// Move Params
	MoveParams *moveParams_;
	// SimParams
	SimulationParameters *simParams_;
	EnergyData energyDiff_;
	int M_;
	// Accept move
	UniformReal uniZeroOne_;
	//
public:
	void init(WorldLines *wl, EnergyCalc * energyCalc,
			SimulationParameters *simParams, MoveParams *moveParams,
			MTGen *RndGen);
	bool generateBB();
	// Generate a Brownian Motion
	bool generateBBMove();
	// Generate a Random Walk with a Jump
	bool generateRWDMove();
	// Generate Next Level in Levi construction
	bool nextLevel();
	// Generate Next Level Beads
	void nextLevelBeadPos();
	// Generate Next Level random Walk
	void nextLevelRW();
	// calc Swap Move Start Ratio
	bool calcSwap(int originalWL, int swapToWL);
#ifdef CENTER_OF_MASS_MOVE
	// COM Move
	bool COMMove(int moveWL, ParticlePos& dr);
#endif
	// Reject or Accept Move
	bool accept(double p);
};

#endif /* BISECTION_H_ */

