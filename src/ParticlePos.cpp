/*
 * BeadPos->cpp
 *
 *  Created on: Jan 26, 2011
 *      Author: snirgaz
 */

#include "ParticlePos.h"

// DoublePoint
//DoublePoint::DoublePoint() {
//	data_.fill(0);
//}
//DoublePoint::DoublePoint(double v) {
//	data_.fill(v);
//}
//const double* DoublePoint::getDataConst() const {
//	return data_.cbegin();
//}
//double& DoublePoint::operator [](unsigned p) {
//	return data_[p];
//}
//double DoublePoint::operator [](unsigned p) const {
//	return data_[p];
//}
//DoubleArray::iterator DoublePoint::getData() {
//	return data_.begin();
//}
//DoubleArray::iterator DoublePoint::begin() {
//	return data_.begin();
//}
//DoubleArray::iterator DoublePoint::end() {
//	return data_.end();
//}
//DoubleArray::const_iterator DoublePoint::cbegin() const {
//	return data_.cbegin();
//}
//DoubleArray::const_iterator DoublePoint::cend() const {
//	return data_.cend();
//}
//BeadPos

//void BeadPos::setD(int d, double p_d) {
//	(this->operator[])(d) = p_d;
//}
//
//double BeadPos::getD(int d) const {
//	return (*this)[d];
//}
//void ParticlePos::addBeadPos(BeadPos &bead_pos_to,
//		BeadPos const& bead_pos_2) const {
//	for (int d = 0; d < SIM_DIM; d++)
//		bead_pos_to.setD(d, (*this)[d] + bead_pos_2.getD(d));
//	bead_pos_to.cyclic();
//}
//void ParticlePos::subBeadPos(BeadPos &bead_pos_to,
//		BeadPos const& bead_pos_2) const {
//	for (int d = 0; d < SIM_DIM; d++)
//		bead_pos_to.setD(d, (*this)[d] - bead_pos_2.getD(d));
//	bead_pos_to.cyclic();
//}
//void ParticlePos::mulBeadPos(double C) {
//	for (int d = 0; d < SIM_DIM; d++)
//		(*this)[d] *= C;
//	this->cyclic();
//}
//void BeadPos::setVal(double v) {
//	for (int d = 0; d < SIM_DIM; d++)
//		this->setD(d, v);
//}

//void ParticlePos::addBeadPos(BeadPos const& p) {
//	for (int d = 0; d < SIM_DIM; d++)
//		(*this)[d] += p[d];
//	this->cyclic();
//}
//void ParticlePos::subBeadPos(BeadPos const& p) {
//	for (int d = 0; d < SIM_DIM; d++)
//		(*this)[d] -= p[d];
//	this->cyclic();
//}

//void ParticlePos::swap(BeadPos &swapPos) {
//	double T;
//	for (int d = 0; d < SIM_DIM; d++) {
//		T = (*this)[d];
//		(*this)[d] = swapPos.getD(d);
//		swapPos.setD(d, T);
//	}
//}
//void ParticlePos::addD(int d, double p_d) {
//	(*this)[d] += p_d;
//}
//void ParticlePos::subD(int d, double p_d) {
//	(*this)[d] -= p_d;
//}

//void ParticlePos::linearComb(std::vector<BeadPos> const &vList,
//		std::vector<double> const &jumpList, BeadPos const &Base) {
//	this->setVal(0);
//	for (int j = 0; j < SIM_DIM; j++) {
//		for (int d = 0; d < SIM_DIM; d++)
//			(*this)[d] += Base[d] + jumpList[j] * vList[j][d];
//	}
//	this->cyclic();
//}
std::ostream& operator<<(std::ostream& os, ParticlePos const &bead) {
	os << "{";
	for (int d = 0; d < SIM_DIM; d++) {
		os << bead[d];
		if (d != SIM_DIM - 1)
			os << ",";
	}
	os << "}";
	return os;
}
// ForceData
