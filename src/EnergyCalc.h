/*
 * energy_calc.h
 *
 *  Created on: Feb 17, 2010
 *      Author: snirgaz
 */

#ifndef ENERGY_CALC_H_
#define ENERGY_CALC_H_

#include "HelpLibs/def.h"
#include "MoveParams.h"
#include "WorldLines.h"
#include "ParticlePos.h"
#include "HelpLibs/QmcParameters.h"
#include <iterator>     // std::advance

#ifdef POT_AZIZ
void calcGreenForce(ParticlePos & dr, double rZero, double &v, double &f);
void calcGreen(ParticlePos & dr, double rZero, double &v);
#endif

typedef vector<BeadVec::iterator> BeadIterVec;

class EnergyCalc {
private:
	double rm_;
	// Forces Temporary
	ForceData resFProposed_, resFOriginal_;
	// Jump Forces Temporary
	ForceSlice resFProposedJump_, resFOriginalJump_;
	MoveParams *moveParams_;
	QmcParameters *qmcParameters_;
	WorldLines *wl_;
	double greenFac_, forceFac_;
	EnergyData energyOriginal_, energyProposed_;
	int numP_, M_;

	// Array for calculations,
public:
	// Initialization
	void init(QmcParameters *qmcParameters, MoveParams *moveParams,
			WorldLines *wl);
	// Energy Calculations
	EnergyData calcBeadsEnergy();
	void energyCalcSingleTimeSlice(ConfSlice const & wlTimeSlice,
			ParticlePos const & testParticlePos, int testParticleN, bool isOdd,
			ForceSlice & forceTimeSlice, double &totalv);
	double energyCalcAllSlice(CalcBead &calcBead);
	double calcTotalFSingleSlice(ForceSlice const &allO, ForceSlice &allP,
			ForceSlice const &singleO, ForceSlice const & singleP,
			int particleNum);
	void calcTotalFallSlices();
	void countSlices();
	// Swap Energy Calculation
	void energyCalcSingleTimeSliceSwap(ConfSlice const & wlTimeSlice,
			ParticlePos const & testParticlePos,
			const ParticlePos& jumpParticlePos, int swapToWL, int originalWL,
			bool isOdd, ForceSlice & forceTimeSlice, double &totalv);
	EnergyData calcEnergySwap(int originalWL, int swapWL);
#ifdef CENTER_OF_MASS_MOVE
	EnergyData calcEnergyCOM(int moveWL, ParticlePos& dr);
	double calcEnergyCOMOriginalSlice(int moveWL, int m);
	double calcEnergyCOMProposedSlice(int moveWL, ParticlePos &dr, int m);
	void calcEnergyCOMFAll(int moveWL);
#endif
	// Calculates Total configuration Energy
	void calcConfigurationEnergy(bool update, EnergyData &ED,
			WorldLinesForce &force);
	EnergyData calcConfigurationEnergy(bool update);
	void calcTotalFSwap(int wlOriginal, int wlSwap);
	void calcExtPot(BeadIterVec const &currentBeads);
	void calcExtPotSwap(int wlOriginal, int wlSwap);
	// Calculates Greens function
	void calcGreen(ParticlePos const &dr, double &v);
	void calcGreenForce(ParticlePos const &dr, double &v, double &f);
	// Debug	void test_green(int res);
	void calcTotalVSwap();

};

#endif /* ENERGY_CALC_H_ */

