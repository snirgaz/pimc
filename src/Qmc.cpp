/*
 * Qmc.cpp
 *
 *  Created on: Jan 24, 2010
 *      Author: snirgaz
 */

#include "Qmc.h"

void Qmc::readParamsFromFile(string fileName) {
	Reader<YamlReader> in(fileName);
		in.read();
		qmcParams_ = in.getQmcParams();
}

void Qmc::energytest() {
	try {
		bool ek = EKcheck();
		bool ev = energy_diff();
		if (!(ek && ev))
			throw "Energy Failed";
	} catch (string &e) {
		cout << e;
		exit(1);
	}
}

void Qmc::init() {

#ifdef PARALLEL_OPENMP
	omp_set_num_threads(qmcParams_.numOfCores.getVal());
#endif
	rndEng_.seed(qmcParams_.seed.getVal());
#ifdef PT
	String OutFile = qmcParams_.outPath;
	OutFile.append("/Conf").append(
			boost::lexical_cast<string>(qmcParams_.confNum)).append(".out");
#else
	string outFileName = qmcParams_.outPath.getVal() + "/Conf.out";
#endif
	outFile_.open(outFileName.c_str(), ios::out);
	outFile_ << "Num Of Cores :" << qmcParams_.numOfCores.getVal() << endl;
	outFile_ << "Seed Number :" << qmcParams_.seed.getVal() << endl;

//	qmcParams.sigmaJumpAccept = sqrt(
//			qmcParams.jumpAcceptFactor * qmcParams.mbar) * qmcParams.sigma;

// d \tau
	qmcParams_.epsilon = qmcParams_.beta.getVal()
			/ double(qmcParams_.M.getVal());
	// sigma propagator
	simParams_.sigma = sqrt(
			2. * qmcParams_.lambda.getVal() * qmcParams_.epsilon.getVal());
	// mbar
	assert(qmcParams_.mbar.getVal() < qmcParams_.M.getVal());
	simParams_.mbarOpen = simParams_.mbarSwap = simParams_.mbarWiggle =
			simParams_.mbarClose = qmcParams_.mbar.getVal();
	// Gamma
	simParams_.gamma = qmcParams_.gamma.getVal();
	// Initialize Configuration
	wl_.init(qmcParams_.M.getVal(), qmcParams_.NumP.getVal());
	// Move Params
	moveParams_.init(qmcParams_);
	// Moves
	openMove_.init(qmcParams_, &simParams_, &wl_, &bisection_, &rndEng_,
			&moveParams_);
	closeMove_.init(qmcParams_, &simParams_, &wl_, &bisection_, &rndEng_,
			&moveParams_);
#ifdef SWAP
	swapMove_.init(qmcParams_, &simParams_, &wl_, &bisection_, &rndEng_,
			&moveParams_);
#endif
#ifdef CENTER_OF_MASS_MOVE
	centerOfMassMove_.init(qmcParams_, &simParams_, &wl_, &bisection_, &rndEng_,
			&moveParams_);
#endif
	wiggleMove_.init(qmcParams_, &simParams_, &wl_, &bisection_, &rndEng_,
			&moveParams_);
	// Energy Cala
	energyCalc_.init(&qmcParams_, &moveParams_, &wl_);
	// Bisection
	bisection_.init(&wl_, &energyCalc_, &simParams_, &moveParams_, &rndEng_);
	// Next Move
#if defined(SWAP) and defined(CENTER_OF_MASS_MOVE)
	nextMove_.init(&rndEng_, UniIntParams(1, 4));
#elif defined(SWAP)
	nextMove_.init(&rndEng_, UniIntParams(1, 3));
#else
	nextMove_.init(&rndEng_, UniIntParams(1, 2));
#endif
	// Measuremetns
	measurements_.init(&wl_, qmcParams_, &simParams_, &outFile_);
	initConf();
	if (not qmcParams_.init.getVal())
		energyCalc_.calcConfigurationEnergy(true);
	wl_.wlEnergy_.energyK_ = wl_.calcEK();
}
void Qmc::initConf() {
	if (qmcParams_.init.getVal())
		this->initializeFromFile();
	else {
#ifdef POT_AZIZ
		string init_type = qmcParams_.initialType.getVal();
#endif
#ifdef POT_LOG
//		WorldLines::initializeSolid(qmcParams.outPath);
#endif
		if (init_type == "regular")
			wl_.initializeRandom(&rndEng_);
		if (init_type == "bcc")
			wl_.initializeSolidBCC(outFile_);
//		this->printEnergyDiff();
	}
}

void Qmc::initializeWLDataFromFile(string fileName) {
	wl_.readFromFile(fileName);
	energytest();
}
void Qmc::initializeFromFile() {
	string dir(qmcParams_.outPath.getVal()), fileName;
	outFile_ << "Initialize From File" << endl;
	// Read  WL data
	initializeWLDataFromFile(dir + "/WL.h5");
	// Load stream Data
	rndEng_.read(fileName);
	// Print initial configuration
	string OutFile = qmcParams_.outPath.getVal();
	measurements_.read(dir);
	outFile_ << "Initialize From File Successful" << endl;
}
void Qmc::thermalize() {
// Thermalize
	outFile_ << "Thermalization of " << qmcParams_.numThermal.getVal()
			<< " :  \n" << std::endl;
	runN(qmcParams_.numThermal.getVal());
// Adjust gamma
//	this->adjustGamma();
// Check energy difference
	//this->printEnergyDiff();
	this->dump(false);
	outFile_ << "Finished Thermalization " << std::endl;
}
void Qmc::runN(int n) {
	for (simParams_.step = 1; (simParams_.step < n); simParams_.step++) {
		this->nextMove();

	}

}
bool Qmc::run() {
	if (!(qmcParams_.init.getVal())) {
		this->thermalize();
	}
	//outFile_ << "Sweep Rate" << qmcParams_.rate.getVal() << std::endl;
	bool endTime, dumpFlag, done;
	simParams_.step = 0;
	while (measurements_.checkNotDone()) {
		nextMove();
		measurements_.DoMeasurement(endTime, dumpFlag);
		if (dumpFlag) {
			dump(false);
		}
		if (endTime) {
			break;
		}
	}
	dump(true);
	energytest();
	return !endTime;

}
bool Qmc::nextMove() {
#ifndef NDEBUG
	assert(EKcheck());
	assert(energy_diff());
#endif
	int next;
	if (wl_.wlData_.isOpenClosed_ == CLOSED)
		next = 0; // Set to Open
	else {
		// draw next move
		next = nextMove_.draw();
	}
	moveParams_.moveType = next;
	simParams_.stats_.moves.update(next);
	moveParams_.accepted = false;
// Prepare next move
	switch (moveParams_.moveType) {
	case 0: //OPEN
		moveParams_.accepted = openMove_.makeMove();
		break;
	case 1: //CLOSE
		moveParams_.accepted = closeMove_.makeMove();
		break;
	case 2: //WIGGLE
		moveParams_.accepted = wiggleMove_.makeMove();
		break;
#ifdef SWAP
	case 3: //SWAP
		moveParams_.accepted = swapMove_.makeMove();
		break;
#endif
#ifdef CENTER_OF_MASS_MOVE
	case 4: //SWAP
		moveParams_.accepted = centerOfMassMove_.makeMove();
		break;
#endif
	}

//	cout << "C++ EK " << wl_.calcEK() << endl;
	simParams_.step++;
	return moveParams_.accepted;

}

void Qmc::dump(bool final) {
	string dir(qmcParams_.outPath.getVal()), fileName, param;
// Dump World Lines
	fileName = dir;
	fileName.append("/WL.h5");
	wl_.dumpToFile(fileName);
	fileName = dir;
// Dump Beads
	fileName.append("/RW.h5");
	moveParams_.dumpBeads(fileName);
// Dump Stats
	fileName = dir;
	fileName = dir + "/Stats.xml";
	simParams_.stats_.dumpToFile(fileName);
// Save stream position
	rndEng_.dump(dir);
// Dump Measurements.
	if (final)
		measurements_.dump(dir, true);
	else
		measurements_.dump(dir, false);
}

void Qmc::updateRS(double rs) {
#ifdef ROT
	qmcParams_.lambda = 1
	/ (PI * double(qmcParams.NumP) * pow(rs, 2) / (sqrt(3) / 2.));
#else
	qmcParams_.lambda = 1
			/ (PI * double(qmcParams_.NumP.getVal()) * pow(rs, 2));
#endif
	this->updateLambda(qmcParams_.lambda.getVal());
	wl_.wlEnergy_.calcEnergy(qmcParams_.greenFactorV.getVal(),
			qmcParams_.greenFactorF.getVal());
}
void Qmc::updateLambda(double lambda) {
	qmcParams_.lambda = lambda;
	qmcParams_.sigma = sqrt(
			2 * qmcParams_.lambda.getVal() * qmcParams_.epsilon.getVal());
	simParams_.sigma = qmcParams_.sigma.getVal();

}

bool Qmc::energy_diff() {
	EnergyData ED1, ED2;
	ED1 = static_cast<EnergyData&>(wl_.wlEnergy_);
	ED2 = energyCalc_.calcConfigurationEnergy(false);
	double diff = fabs(ED1.getEnergy() - ED2.getEnergy());
//ED2 = WorldLines::getEnergyData();
	if (diff > 1E-5) {
		cout << " Energy Diff " << diff << endl;
		cout << " Ev Diff " << fabs(ED1.getEnergyV() - ED2.getEnergyV())
				<< endl;
		cout << " Ef Diff " << fabs(ED1.getEnergyF() - ED2.getEnergyF())
				<< endl;
		cout << " Step " << simParams_.step << endl;
		cout << " Move Type " << moveParams_.moveType << endl;
		cout << moveParams_.mStart << endl;
		return false;
	}
	return true;
}
void Qmc::printEnergyDiff() {
	EnergyData ED1, ED2;
	ED1 = static_cast<EnergyData&>(wl_.wlEnergy_);
	ED2 = energyCalc_.calcConfigurationEnergy(false);
	outFile_ << "\n Energy Difference " << ED1.getEnergy() - ED2.getEnergy()
			<< "\n" << std::endl;
	outFile_ << "\n Energy " << ED1.getEnergy() << "   " << ED2.getEnergy()
			<< "\n" << std::endl;
	outFile_ << "\n Energy V Difference " << ED1.getEnergyV() - ED2.getEnergyV()
			<< "\n" << std::endl;
	outFile_ << "\n Energy V " << ED1.getEnergyV() << "   " << ED2.getEnergyV()
			<< "\n" << std::endl;
	outFile_ << "\n Energy F Difference " << ED1.getEnergyF() - ED2.getEnergyF()
			<< "\n" << std::endl;
	outFile_ << "\n Energy F " << ED1.getEnergyF() << "   " << ED2.getEnergyF()
			<< "\n" << std::endl;

}

bool Qmc::EKcheck() {
	double ek1, ek2;
	ek1 = wl_.wlEnergy_.energyK_;
	ek2 = wl_.calcEK();
	if (fabs(ek1 - ek2) > 0.00001) {
		cout << "From Data : " << ek1 << endl;
		cout << "From Calc : " << ek2 << endl;
		cout << "Step : " << simParams_.step << endl;
		return false;
	} else
		return true;
}
#ifdef PYTHON
boost::python::object Qmc::getConfPython() {
	boost::python::dict conf;
	conf["wl"] = wl_.toPython();
	conf["move"] = moveParams_.toPython();
	conf["step"] = simParams_.step;
	return conf;
}


void Qmc::getParamsFromPython(boost::python::dict qmcParams) {
	Reader<PythonReader> in(qmcParams);
	in.read();
	qmcParams_ = in.getQmcParams();
}

boost::python::dict Qmc::calcTotalEnergy() {
	EnergyData ED;
	WorldLinesForce force;
	energyCalc_.calcConfigurationEnergy(false, ED, force);
	boost::python::dict ed;
	ed["energy"] = ED.toPython();
	ed["force_data"] = force.toPython();
	return ed;
}
#endif
