/*
 * run2.cpp
 *
 *  Created on: Nov 15, 2010
 *      Author: snirgaz
 */

#include "HelpLibs/def.h"
#include "Qmc.h"
//#include "ParallelTempring.h"
#include "HelpLibs/InputFile.h"
#include "HelpLibs/ArrayMultiD.h"
//#include "Test.h"

using namespace std;

void run(char * in_file) {
	string file_name = in_file;
#ifdef PT
	ParallelTempring sim(in.getQmcParams(), in.getPTParams());
	omp_set_num_threads(in.getPTParams().coreNum);
	// ippSetNumThreads(1);
	sim.run();
	sim.dunum_measure_tempmp();
#else
	Qmc sim;
	sim.readParamsFromFile(file_name);
	sim.init();
	bool done=sim.run();
	//sim.dump();
#endif
	if (done)
		cout << "yes";
	else
		cout << "no";
}
int main(int argc, char* argv[]) {
	assert(argc == 2);
	char * in_file;
	in_file = argv[1];
//	Test test;
//	test.VecObsTest();
	run(in_file);
	return 0;
}
