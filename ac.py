#! /opt/anaconda/bin/python2.7
"""
Created on Sun Nov  3 11:26:56 2013

@author: snirgaz
"""
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import sys
import numpy as np
from maxentparams import params
sys.path.append("/u/snirgaz/Code/analyticcontinuation/trunk/pyscript") 
from analytic_cont import analytic_cont
sys.path.append("/u/snirgaz/Code/Scripts/PythonScripts")
import readdata as rd
import glob,os,argparse
from multiprocessing import Pool

class KeyboardInterruptError(Exception): pass

def listDirs(in_dir):  
    dirs = [d for d in glob.glob(in_dir + '/???') if os.path.isdir(d)]
    return dirs

def skw_read(simfile,err):
    return {"M":rd.readYAMLparam(simfile['yaml_file'],"M"),
            "skw":rd.readHDF5(simfile['hdf5_file'],[{"obs_name":"skw_long","hdf5_name":"/SKW_SQblong"},
						    {"obs_name":"skw_t1","hdf5_name":"/SKW_SQt1"},
						    {"obs_name":"skw_t2","hdf5_name":"/SKW_SQt2"}],err),
            }
class run(object):
    def __init__(self, skmean,skvar,dumpdir,params):
        self.skvar = skvar
        self.skmean = skmean
        self.dumpdir =dumpdir
        self.params= params
    def __call__(self, i):
    	try:
		self.params["dump_dir"]=self.dumpdir+'/sim_'+str(i)
		if not os.path.exists(self.params["dump_dir"]):
		    os.makedirs(self.params["dump_dir"])
		data={"chi_var":self.skvar[i][:,:]
		,"chi":self.skmean[i][:]}
		normdata=np.linalg.norm(data["chi"])
		normmat=(1./normdata)*np.eye(data["chi"].shape[0])
		data["chi"]=np.dot(normmat,data["chi"])
		data["chi_var"]=np.dot(np.dot(normmat,data["chi_var"]),normmat)
		self.params["dof"]=data["chi"].size
		self.ac=analytic_cont(self.params,data)
		resLap=self.ac.runLAp()
		resMaxEnt=self.ac.runMaxEnt()
		return {"omega":resLap["mxData"]["omegaP"],"lap":resLap["anData"]["A_maxCurve"],"MaxEnt":resMaxEnt["anData"]["A_maxCurve"]}
    	except KeyboardInterrupt:
            raise KeyboardInterruptError()
if __name__ == "__main__":
      # arguments
    parser = argparse.ArgumentParser(description='MaxEnt')
    parser.add_argument('-dir', action='store', required=True)
    parser.add_argument('-dump', action='store',type=int, required=True)
    parser.add_argument('-err', action='store', type=int, required=True)
    parser.add_argument('-sims', action='store', type=int, required=False)
    parser.add_argument('-L', action='store', type=int, required=False,default=1)
    args = parser.parse_args()
    path=args.dir
    paths=listDirs(path)
    p=Pool(10)
    sks=map(lambda x:skw_read(x,args.err),sorted(rd.getSimFiles(path)))
    maxomega=60
    for sk,cdir in zip(sks,paths):
        skvar=sk["skw"]["Var"]
        skmean=sk["skw"]["Mean"]
        dumpdir=cdir+'/dump_'+str(args.dump)
        if not os.path.exists(dumpdir):
                os.makedirs(dumpdir)
        try:          
            allres=p.map(run(skmean,skvar,dumpdir,params),range(skmean.shape[0]))
            p.close()
            print 'pool map complete'
        except KeyboardInterrupt:
            print 'got ^C while pool mapping, terminating the pool'
            p.terminate()
            print 'pool is terminated'
        except Exception, e:
            print 'got exception: %r, terminating the pool' % (e,)
            p.terminate()
            print 'pool is terminated'
        fig=plt.figure()
        ax = fig.gca(projection='3d')
        for i,res in enumerate(allres):
            x=float(i+1)*np.ones_like(res["MaxEnt"])[np.where(res["omega"]<maxomega)]
            y=res["omega"][np.where(res["omega"]<maxomega)]
            z=res["MaxEnt"][np.where(res["omega"]<maxomega)]
            ax.plot(x, y, z)
            ax.set_ylim(0,maxomega)
            ax.set_zlim(0,10)
        fig.savefig(dumpdir+"/allMaxEnt.pdf")
        fig=plt.figure()
        ax = fig.gca(projection='3d')
        for i,res in enumerate(allres):
            x=float(i+1)*np.ones_like(res["lap"])[np.where(res["omega"]<maxomega)]
            y=res["omega"][np.where(res["omega"]<maxomega)]
            z=res["lap"][np.where(res["omega"]<maxomega)]
            ax.plot(x, y, z)
            ax.set_ylim(0,maxomega)
            ax.set_zlim(0,10)
        fig.savefig(dumpdir+"/allLap.pdf")

    
        
