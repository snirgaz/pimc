import sys
import matplotlib.pyplot as plt
sys.path.append('/home/snirgaz/Code/pimc/python')
sys.path.append('/home/snirgaz/Code/pimc/trunk/pyscript')
import Qmc as qmc
sys.path.append('/home/snirgaz/Code/Scripts/PythonScripts')
from readdata import readYAMLparam
import numpy as np
import math
import energycalc as ec

def movetostr(move):
    if (move==0):
        return "OPEN"
    if (move==1):
        return "CLOSE"
    if (move==2):
        return "WIGGLE"
    if (move==3):
        return "SWAP"
    if (move==4):
        return "CENTER_OF_MASS"

class qmc_debug:
    def __init__(self):
        self.q=qmc.Qmc()        
        ##self.fig=plt.figure()
        ##self.ax=self.fig.add_subplot(111)
        ##self.figall=plt.figure()
        ##self.axall=self.figall.add_subplot(111)
        ##self.fig.show()
        ##self.figall.show()
    def init(self,filename):
        self.q.init(filename) 
        self.data=[]
        self.data.append(self.q.getConf())
        self.data.append(self.q.getConf())
        self.newpos=0
        self.oldpos=1
        self.rm=readYAMLparam(filename,"rm")
        self.M=readYAMLparam(filename,"M")
    def newdata(self):
        return self.data[self.newpos]
    def olddata(self):
        return self.data[self.oldpos]
    def plotall(self):
        self.axall.clear()
        data=self.newdata()["wl"]["wl_data"]["wl_conf"]
        for n in range(data.shape[1]):            
            particledata=data[:,n,:]        
            self.axall.plot(range(particledata.shape[0]),particledata[:,0])
        self.figall.canvas.draw()
    def calcek(self):        
        wldata=self.newdata()["wl"]["wl_data"]
        base=wldata["wl_conf"].copy()
        loops=wldata["loops"].copy()
        target=np.zeros_like(base)
        target[:-1]=base[1:]
        target[-1]=wldata["wl_conf"][0][loops].copy()
        if wldata["is_open"]:
            jslice=wldata["worm_slice"]
            jnum=wldata["worm_num"]
            base[jslice][jnum]=wldata["jump_pos"].copy()
        diff=target-base        
        diff[np.where( diff>.5 )]=diff[np.where( diff>.5 )]-1
        diff[np.where( diff<-.5 )]=diff[np.where( diff<-.5 )]+1
#         posdiff=diff[np.where( np.abs(diff)>0.000001 )]
#         print np.reshape(posdiff,[posdiff.shape[0]/3,3])
        ekpy=np.sum(np.power(diff,2))   
        ekc=self.newdata()["wl"]["energy"]["ek"]
        ekdiff=ekpy-ekc
        print "Ek diff - ",ekdiff," Ek Py - ",ekpy      
        return math.fabs(ekdiff)>0.000001
    def calcenergy(self):        
        return ec.calcConfEnergy(self.newdata()["wl"]["wl_data"],self.rm,True)
    def plotconf(self):
        self.ax.clear()
        old_wldata=self.olddata()["wl"]["wl_data"]
        beads=self.newdata()["move"]["beads"];  
        wl_conf=old_wldata["wl_conf"]
        slice_data_new=np.array([b["bead_pos"] for b in beads])
        x_axis=range(len(slice_data_new))
        slice_data_old=np.array([wl_conf[b["wl_slice"]][b["n"]] for b in beads])        
        if old_wldata["is_open"]:
            jumpO=old_wldata["jump_pos"].T
            print "Original",[1 if v["original_jump"] else 0 for v in beads]
            print "Propoded",[1 if v["proposed_jump"] else 0 for v in beads]
            worm_slice=old_wldata["worm_slice"]
            worm_num=old_wldata["worm_num"]
            print "worm slice old-",old_wldata["worm_slice"]
            print "mstart new-",self.newdata()["move"]["mStart"]
            tauwl=old_wldata["worm_slice"]-self.newdata()["move"]["mStart"]
            if tauwl<0:
                tauwl=tauwl+self.M
            print "tau wl -",tauwl
            x_axis_jump=np.insert(x_axis,tauwl,tauwl)
#             print "wl base - ",slice_data_old[tauwl]
#             print "wl base 2 -",old_wldata["wl_conf"][worm_slice][worm_num]
#             print "wl jump - ",jumpO
            slice_data_old=np.insert(slice_data_old,tauwl,jumpO,axis=0)
            self.ax.plot(x_axis_jump,slice_data_old[:,0],'ro-')
        else:
            self.ax.plot(x_axis,slice_data_old [:,0],'ro-')
        moveType=self.newdata()["move"]["moveType"]
        if (moveType!=1 and moveType!=3):
            tau=self.newdata()["move"]["tau"]
            jumpP=self.newdata()["move"]["beads"][tau]["jump_pos"].T            
            print "tau rw -",tau
            x_axis_jump=np.insert(x_axis,tau,tau)
            slice_data_new=np.insert(slice_data_new,tau,jumpP,axis=0)
            self.ax.plot(x_axis_jump,slice_data_new[:,0],'bo-')
        else:
            self.ax.plot(x_axis,slice_data_new [:,0],'bo-')                
        self.fig.canvas.draw() 
    def nextMove(self):
        self.q.nextMove()
        self.data[self.oldpos]=self.q.getConf()
        self.newpos,self.oldpos=self.oldpos,self.newpos
        olddata=self.olddata()["wl"]["wl_data"]
        newdata=self.newdata()["wl"]["wl_data"]
#         print "new data Base ",newdata["wl_conf"][newdata["worm_slice"]][newdata["worm_num"]]
#         print "old data Base ",olddata["wl_conf"][olddata["worm_slice"]][olddata["worm_num"]]
#         print "new data Jump ",self.newdata()["wl"]["wl_data"]["jump_pos"]
#         print "old data Jump ",self.olddata()["wl"]["wl_data"]["jump_pos"]
        ##self.plotconf()
        ##self.plotall()
        self.currenten=self.calcenergy()
        print "EV diff",self.currenten["totalv"]-self.newdata()["wl"]["energy"]["ev"]     
        print "EF diff",self.currenten["totalf"]-self.newdata()["wl"]["energy"]["ef"]
        print movetostr(self.newdata()["move"]["moveType"])
        print "accepted - ",self.newdata()["move"]["accepted"]
        #print "Diff py -c++ ",self.currenten["totalf"]-self.q.calcTotalEnergy()["energy"]["ef"]
        return math.fabs((self.currenten["totalf"]-self.newdata()["wl"]["energy"]["ef"]))>1E-6
    def doMoves(self,n):
        for i in range(n):
            print "move ",i
            self.q.nextMove()            
q=qmc_debug()
q.init('/home/snirgaz/sim_data/bcc/BCC_COM/000/000/sim.yaml')
