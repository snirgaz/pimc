import sys
import matplotlib.pyplot as plt
sys.path.append('/home/snirgaz/Code/pimc/python')
import Qmc as qmc
import numpy as np
import copy
import math
import numpy
from numba import autojit



@autojit
def aziz(diff,rm):
    # All instance attributes must be defined in the initializer
    D = 1.241314
    A = 0.5448504E6
    EPS = 10.8
    ALPHA = 13.353384
    C = numpy.array([ 1.3732412, 0.4253785, 0.1781 ])
    Cd = numpy.array([ 1.3732412 * 6, 0.4253785 * 8, 0.1781 * 10 ])
    dr = np.linalg.norm(diff)
    drS = dr / rm
    dr_minus_ten = math.pow(drS, -10)
    dr_minus_eleven = dr_minus_ten / drS
    drsqr = drS * drS
    p = dr_minus_ten * (C[2] + drsqr * (C[1] + C[0] * drsqr))
    dp = -dr_minus_eleven * (Cd[2] + drsqr * (Cd[1] + Cd[0] * drsqr))
    v = EPS * (A * math.exp(-ALPHA * drS))
    f = EPS * (-ALPHA * A * math.exp(-ALPHA * drS))
    if (drS > D):
        v -= EPS * p
        f -= EPS * dp
    else:
        exp_arg = (D / drS - 1)
        h = math.exp(-pow(exp_arg, 2))
        dh = 2 * D * h * exp_arg / (drsqr)
        v -= h * EPS * p
        f -= EPS * (dh * p + h * dp)
    f /= (dr * rm)
    return v,f
def calcswap(oldconf,swapto,original,rm):
    wl_data=oldconf["wl_conf"]
    jump_slice=oldconf["worm_slice"]
    jump_num=oldconf["worm_num"]
    jump_pos=oldconf["jump_pos"]    
    N=wl_data.shape[1] 
    print jump_num,original,swapto
    ## original
    slice_data=(wl_data[jump_slice,:,:]).copy()
    print "slice_data shape",slice_data.shape
    slice_data[original]=jump_pos
    originaljump=numpy.zeros_like(slice_data)
    testparticle=slice_data[swapto]
    for n in range(N):
        if (n==swapto):
            continue
        diff=slice_data[n]-testparticle
        diff[numpy.where(diff>.5)]=diff[numpy.where(diff>.5)]-1.
        diff[numpy.where(diff<-.5)]=diff[numpy.where(diff<-.5)]+1.  
        cv,cf=aziz(diff,rm)
        originaljump[n]=diff*cf
    ## proposed
    slice_data=(wl_data[jump_slice,:,:]).copy()
    testparticle=slice_data[original].copy()
    slice_data[original]=jump_pos
    proposedjump=numpy.zeros_like(slice_data)    
    for n in range(N):
        if (n==swapto):
            continue
        diff=slice_data[n]-testparticle
        diff[numpy.where(diff>.5)]=diff[numpy.where(diff>.5)]-1.
        diff[numpy.where(diff<-.5)]=diff[numpy.where(diff<-.5)]+1.
        cv,cf=aziz(diff,rm)
        proposedjump[n]=diff*cf
    print proposedjump      
    print originaljump
    
def calcConfEnergy(conf,rm,force):        
    wl_data=conf["wl_conf"]
    M=wl_data.shape[0]
    N=wl_data.shape[1] 
    d=wl_data.shape[2]
    f=numpy.zeros_like(wl_data)
    v=numpy.zeros([M,N])
    vjump=numpy.zeros(N)
    fjump=numpy.zeros((N,d))
    for m in range(M):
        odd=True if m%2==1 else False
        for n in range(N):
            tf=numpy.zeros(d)
            tv=0.
            for np in range(N):
                if (n==np):
                    continue
                diff=wl_data[m,n]-wl_data[m,np]
                diff[numpy.where(diff>.5)]=diff[numpy.where(diff>.5)]-1.
                diff[numpy.where(diff<-.5)]=diff[numpy.where(diff<-.5)]+1.
                cv,cf=aziz(diff,rm)
                if odd:
                    tv+=cv                    
                    tf+=diff*cf
                else:
                    tv+=cv
            v[m,n]=tv
            f[m,n]=tf
    if conf["is_open"]:    
        jump_slice=conf["worm_slice"]
        jump_num=conf["worm_num"]
        jump_pos=conf["jump_pos"]
        v[jump_slice,:]*=.5
        odd=True if conf["worm_slice"]%2==1 else False
        for n in range(N):
            tf=numpy.zeros(d)
            tv=0.
            for np in range(N):
                if (n==np):
                    continue
                if (n==jump_num):
                    diff=jump_pos-wl_data[jump_slice,np]
                elif (np==jump_num):
                    diff=wl_data[jump_slice,n]-jump_pos
                else:
                    diff=wl_data[jump_slice,n]-wl_data[jump_slice,np]
                diff[numpy.where(diff>.5)]=diff[numpy.where(diff>.5)]-1.
                diff[numpy.where(diff<-.5)]=diff[numpy.where(diff<-.5)]+1.
                cv,cf=aziz(diff,rm)
                if odd:
                    tv+=cv
                    tf+=diff*cf
                else:
                    tv+=cv
            vjump[n]=tv
            fjump[n]=tf            
            v[jump_slice,n]+=.5*vjump[n]
    totalv=numpy.sum(v,axis=(1))
    if force:
        totalv[1::2]*=2.  
    totalv=.5*numpy.sum(totalv)
    f[::2]=0.
    totalf_slice=numpy.sum(numpy.power(f,2),(1,2))
    out_total_slice=totalf_slice.copy()
    total_jump=0
    if conf["is_open"] and conf["worm_slice"]%2==1:
        total_jump=numpy.sum(numpy.power(fjump,2),(0,1))
        totalf_slice[conf["worm_slice"]]=.5*(totalf_slice[conf["worm_slice"]]+total_jump)
    if conf["is_open"] and conf["worm_slice"]%2==0:
        fjump[:]=0.
        total_jump=0
    totalf=numpy.sum(totalf_slice)
    return {"v":v,"f":f,"jumpf":fjump,"totalv":totalv,
    "totalf":totalf,"totalf_slice":out_total_slice,"total_jump_slice":total_jump}
# 
# 
# class EnergyCalc:
#     def init(self,rZero):
#         rZero=rZero
#     