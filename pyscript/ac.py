#! /u/snirgaz/anaconda/bin/python2.7
"""
Created on Sun Nov  3 11:26:56 2013

@author: snirgaz
"""
mevKb=11.604505
from os.path import expanduser
home = expanduser("~")
import matplotlib.pylab as plt
import sys
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from maxentparams import params
sys.path.append(home+"/Code/AnalyticCont/trunk/pyscript") 
from analytic_cont import analytic_cont
sys.path.append(home+"/Code/Scripts/PythonScripts")
import readdata as rd
import glob,os,argparse
from multiprocessing import Pool
from operator import itemgetter
import tables as tb


class KeyboardInterruptError(Exception): pass

def listDirs(in_dir):  
    dirs = [d for d in glob.glob(in_dir + '/???') if os.path.isdir(d)]
    return dirs

def skw_read(simfile,err):
    return {"M":rd.readYAMLparam(simfile['yaml_file'],"M"),
            "N":rd.readYAMLparam(simfile['yaml_file'],"N"),
            "skw":
            {"skw_t1":rd.readHDF5(simfile['hdf5_file'],"/SKW_t1",err),
            "skw_t2":rd.readHDF5(simfile['hdf5_file'],"/SKW_t2",err),
            "skw_l":rd.readHDF5(simfile['hdf5_file'],"/SKW_long",err)
            }
            }
class run(object):
    def __init__(self,dumpdir,params):
        self.dumpdir =dumpdir
        self.params= params
    def __call__(self,(i,mean,var)):
        try:
            self.params["dump_dir"]=self.dumpdir+'/sim_'+str(i)
            if not os.path.exists(self.params["dump_dir"]):
                os.makedirs(self.params["dump_dir"])
	    print var.shape,mean.shape
            data={"chi_var":var[1:,1:]
            ,"chi":mean[1:]}
            normdata=np.linalg.norm(data["chi"])
            normmat=(1./normdata)*np.eye(data["chi"].shape[0])
            data["chi"]=np.dot(normmat,data["chi"])
            data["chi_var"]=np.dot(np.dot(normmat,data["chi_var"]),normmat)
            self.params["dof"]=data["chi"].size
            self.ac=analytic_cont(self.params,data)
            resLap=self.ac.runLAp()
            resMaxEnt=self.ac.runMaxEnt()
            return {"omega":resLap["mxData"]["omegaP"],"lap":resLap["anData"]["A_maxCurve"],"MaxEnt":resMaxEnt["anData"]["A_maxCurve"]}
        except KeyboardInterrupt:
            raise KeyboardInterruptError()
def plotAll(allres,dumpdir):
	fig=plt.figure()        
	ax = fig.gca(projection='3d')   
	maxomega=6     
	for i,res in enumerate(allres):
	    x=float(i+1)*np.ones_like(res["MaxEnt"])[np.where(res["omega"]<maxomega)]
	    y=res["omega"][np.where(res["omega"]<maxomega)]
	    z=res["MaxEnt"][np.where(res["omega"]<maxomega)]
	    ax.plot(x, y, z)
	    ax.set_ylim(0,maxomega)
	    ax.set_zlim(0,10)
	fig.savefig(dumpdir+"/allMaxEnt.pdf")
	fig=plt.figure()
	ax = fig.gca(projection='3d')
	for i,res in enumerate(allres):
	    x=float(i+1)*np.ones_like(res["lap"])[np.where(res["omega"]<maxomega)]
	    y=res["omega"][np.where(res["omega"]<maxomega)]
	    z=res["lap"][np.where(res["omega"]<maxomega)]
	    ax.plot(x, y, z)
	    ax.set_ylim(0,maxomega)
	    ax.set_zlim(0,10)
	fig.savefig(dumpdir+"/allLap.pdf")
	fig=plt.figure()        
	ax = fig.add_subplot(121) 
	m=10
	pos = [float(x)/(float(m)) for x in range(len(allres)+1)]       
	maxMaxEnt = [f["omega"][np.argmax(f["MaxEnt"])] for f in allres]
	maxMaxEnt = [0]+maxMaxEnt
	ax.plot(pos,maxMaxEnt,'-o')
	ax.set_xlim(0,1.1)        
	ax.set_ylim(0,3)
	ax = fig.add_subplot(122)
	maxLap = [f["omega"][np.argmax(f["lap"])] for f in allres]
	maxLap = [0]+maxLap
	ax.plot(pos,maxLap,'-o')
	ax.set_xlim(0,1.1)
	ax.set_ylim(0,3)
	fig.savefig(dumpdir+"/disp_rel.pdf")
def dumpData(allres,dumpdir):
	h5file=tb.open_file(dumpdir+"/data.h5", mode = "w")
	maxent=np.array(res["MaxEnt"] for res in allres)
	lap=np.array(res["lap"] for res in allres)
	omega=allres[0]["omega"]
	h5file.create_array("/", 'maxent', maxent)
	h5file.create_array("/", 'lap', lap)
	h5file.create_array("/", 'omega', omega)
	h5file.close()
def runBranch(branch_name,branch_data,params,dump_dir):
	print branch_name
	branch_data=dict(map(lambda k:(k,map(itemgetter(k),branch_data)),branch_data[0].keys()))
	numk=branch_data["Var"][0].shape[0]
	numav=len(branch_data)
	skvar=[np.array([branch_data["Var"][i][j] for i in range(numav)]) for j in range(numk)]
	skmean=[np.array([branch_data["Mean"][i][j] for i in range(numav)]) for j in range(numk)]
	skvar=map(lambda x:(1./float(numav))*np.mean(x,axis=0),skvar)
	skmean=map(lambda x:np.mean(x,axis=0),skmean)
	dumpdir=dump_dir+'/'+branch_name
	print "done"
	if not os.path.exists(dumpdir):
		os.makedirs(dumpdir)
	try:          
	    data=[(i,m,v) for i,(m,v) in enumerate(zip(skmean,skvar))]
	    allres=p.map(run(dumpdir,params),data)
	    p.close()
	    print 'pool map complete'
	except KeyboardInterrupt:
	    print 'got ^C while pool mapping, terminating the pool'
	    p.terminate()
	    print 'pool is terminated'
	except Exception, e:
	    print 'got exception: %r, terminating the pool' % (e,)
	    p.terminate()
	    print 'pool is terminated'
	plotAll(allres,dumpdir)
	dumpData(allres,dumpdir)
if __name__ == "__main__":
	# arguments
	parser = argparse.ArgumentParser(description='MaxEnt')
	parser.add_argument('-dir', action='store', required=True)
	parser.add_argument('-dump', action='store',type=int, required=True)
	parser.add_argument('-err', action='store', type=int, required=True)
	parser.add_argument('-sims', action='store', type=int, required=False)
	parser.add_argument('-L', action='store', type=int, required=False,default=1)
	args = parser.parse_args()
	path=args.dir
	paths=listDirs(path)
	p=[]
	sks=map(lambda x:skw_read(x,args.err),sorted(rd.getSimFiles(path)))
	data=dict(map(lambda k:(k,map(itemgetter(k),sks)),sks[0].keys()))
	data["skw"]=dict(map(lambda k:(k,map(itemgetter(k),data["skw"])),data["skw"][0].keys()))
	dumpdir=path+'/dump_'+str(args.dump)
	print dumpdir
	if not os.path.exists(dumpdir):
		os.makedirs(dumpdir)
	for key,val in data["skw"].iteritems():
		p=Pool(2)
		runBranch(key,val,params,dumpdir)


    
        
