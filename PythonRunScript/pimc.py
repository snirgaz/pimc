
# Imports#! /u/snirgaz/anaconda/bin/python2.7

# Imports

import os,sys,glob,math,stat,shutil,subprocess
import argparse
import random
import yaml
import numpy as np
import itertools
from paramiko import SSHClient, SSHConfig,ProxyCommand
import generateSimData
import fileOps as fo
import singleSim as ss



# Helper Functions
def digit(i):
    return str(i/100)+str((i %100)/10)+str(i%10)

   

class multiSim:
    def __init__(self,allData,pathData,debugMode,fileOps,singleSim):
        self.allData = allData
        self.debugMode = debugMode
        self.pathData = pathData 
        self.fileOps=fileOps   
        self.singleSim=singleSim
    def copysim_files(self,paramfile,delete=False):
        if delete:
            self.fileOps.deleteDirectory(self.pathData['output_path'])            
        self.fileOps.createDir(self.pathData['output_path'])
        self.pathData['output_path']=os.path.join(self.fileOps.getHome(),self.pathData['output_path'])        
        print "Created the Directory"
#         self.pathData['python_file']=os.path.join(self.pathData['output_path'],"Code/PythonRunScript/pimc.py")
#         print self.pathData['python_file']
#         self.pathData['pimc_run_file']=os.path.join(self.pathData['output_path'],"Code/PythonRunScript/run_pimc.py")
        self.fileOps.copyFile(paramfile,os.path.join(self.pathData['output_path'],paramfile))
#         print self.pathData['output_path']
#         self.fileOps.chmod(self.pathData['python_file'],stat.S_IRWXU)
        print "Sim files Copied"
    def download_svn(self):
        codeDir=os.path.join(self.pathData['output_path'],"Code")
        self.fileOps.deleteDirectory(codeDir)            
        self.fileOps.createDir(codeDir)
        print "Delete Code Directory"  
        command='git clone git@bitbucket.org:snirgaz/pimc.git '+codeDir        
        print command
        self.fileOps.shellCommand(command) 
        print "Download Svn"
    def compile_bin(self):
        comvar=self.fileOps.compvar()
        disutilscript=os.path.join(self.pathData['output_path'],"Code","setup.py")
        releasePath=os.path.join(self.pathData['output_path'],"Release")
        self.fileOps.deleteDirectory(releasePath)            
        codeDir=os.path.join(self.pathData['output_path'],"Code")
        command=comvar+"cd "+codeDir+"; "+self.fileOps.python_bin()+" "+disutilscript+" build_ext -b "+releasePath
        if allData["sim_data"]["free"]:
            command=command+" -D POT_FREE"
        self.pathData['libPath']=releasePath
        print command
        self.fileOps.shellCommand(command) 
        print "Compile"
    def generateMultiSim(self): 
        codeDir=os.path.join(self.pathData['output_path'],"Code")
        self.pathData['pythonScriptFileName']=os.path.join(codeDir,"PythonRunScript/run_pimc.py") 
        releasePath=os.path.join(self.pathData['output_path'],"Release") 
        self.pathData['libPath']=os.path.join(releasePath)  
        paramGen=generateSimData.generateMultiSim(self.allData["params"],self.allData["sim_data"])        
        for i,c_sim_data in enumerate(paramGen):
            allData["sim_data"]=c_sim_data
            outDir=os.path.join(self.pathData['output_path'],digit(i))
            self.fileOps.createDir(outDir)
            self.pathData['confNum']=i
            for j in range(c_sim_data["num_sims"]):
                outDirS=os.path.join(outDir,digit(j))
                self.fileOps.createDir(outDirS)
                c_sim_data['out_path_c']=outDirS
                c_sim_data['seed']=j
                print c_sim_data['seed']
                self.pathData['PathDataFileName']=os.path.join(outDirS,"pathdata.yaml")
                self.pathData['YAMLFileName']=os.path.join(outDirS,"sim.yaml")
                self.pathData['scriptFileName']=os.path.join(outDirS,"simRun.sh")
                self.pathData['runAllFileName']=os.path.join(outDirS,"qsubRun.sh")
                self.pathData['PathDataFileName']=os.path.join(outDirS,"pathdata.yaml")
                self.pathData['subNum']=j
                self.pathData['output_path_c']=outDirS
                self.fileOps.createDir(os.path.join(outDirS,'log'))
                self.singleSim.init(c_sim_data,self.pathData,self.fileOps)
                self.singleSim.generateScriptFile()                
                if not (self.debugMode):
                    self.singleSim.runSim()        


if __name__ == '__main__':    
    # parse command line options
    parser = argparse.ArgumentParser(description='Run A WORM ALG Simulation for Phonon')
    # Arguments
    parser.add_argument('-debugMode', action='store_true', default=False)
    parser.add_argument('-out_path',type=str, action='store')
    parser.add_argument('-param_file', action='store')
    parser.add_argument('-recompile', action='store_true', default=False)
    parser.add_argument('-clonegit', action='store_true', default=False)
    parser.add_argument('-delete_folder', action='store_true', default=False)
    parser.add_argument('-free', action='store_true', default=False)
    parser.add_argument('-grid', type=str,action='store')
    args = parser.parse_args()                     
    scriptName=os.path.basename(__file__)
    ##### Path Data
    # Find Grid Files

    pathData={
        'output_path':args.out_path,
        'python_file':os.path.join(args.out_path,scriptName)
        }    
    ## Grid or Local
    file_ops=[]
    fo.localOPs()
    single_sim=[]
    if (args.grid):
        single_sim=ss.SingleSimGrid()
        try:
            if (args.grid=='tamnun'):
                grid_params={'full_name':'tamnun.technion.ac.il','short_name':'tamnun'
                             ,'PBS_name':'all_l_p'
                             ,'compvar':"source /usr/local/profiles/gcc4.7.sh;"
                             ,'python_bin':"/u/snirgaz/anaconda/bin/python"}
                file_ops=fo.gridOPs(grid_params)
            elif (args.grid=='tech-ui01'):
                grid_params={'full_name':'tech-ui01.hep.technion.ac.il','short_name':'tech-ui01'
                             ,'PBS_name':'N'
                             ,'compvar':"source /cvmfs/atlas.cern.ch/repo/sw/atlas-gcc/481/x86_64/setup.sh;"
                             ,'python_bin':"/srv01/technion/snirgaz/Software/anaconda/bin/python"}
                file_ops=fo.gridOPs(grid_params)
            else:
                raise('Unknown grid')
        except Exception, e:
                print >> sys.stderr, "does not exist"
                print >> sys.stderr, "Exception: %s" % str(e)
                sys.exit(1)  
    else:
        file_ops=fo.localOPs()
        single_sim=ss.SingleSimLocal()
    ## First Run
    fileParams=open(args.param_file)
    if args.delete_folder:
        var = raw_input("Are you sure you want to delete the folder? (y/n) ")
        if var!="y":
            print "Not Deleteing"
            args.delete_folder=False                
    allData=yaml.load(fileParams)                    
    allData["sim_data"]["init"]=False
    allData["sim_data"]['run_num']=0       
    if (args.free):
        allData["sim_data"]["free"]=True
    else:
        allData["sim_data"]["free"]=False
    multiSimRun=multiSim(allData,pathData,args.debugMode,file_ops,single_sim)
    #multiSimRun.openconnection()
    multiSimRun.copysim_files(args.param_file,args.delete_folder)
    if args.clonegit:
        multiSimRun.download_svn()
    if args.recompile:
        multiSimRun.download_svn()
        multiSimRun.compile_bin()
    multiSimRun.generateMultiSim()
    fileParams.close()


   

