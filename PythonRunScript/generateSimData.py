import os,sys,glob,math,stat,shutil,subprocess
import argparse
import random
import yaml
import numpy as np
import itertools
from paramiko import SSHClient, SSHConfig,ProxyCommand
import copy
import operator

lambda_zero=6.0596
rm_zero=2.9673

def rot(x):
    return [-x[1],x[0]]
def recApply(func, n):
        if n > 1:
                rec_func = recApply(func, n - 1)
                return lambda x: func(rec_func(x))
        return func
def generate_brag_long(n):
    allrot=[recApply(rot,y)([n,n])+[0] for y in range(1,5)]
    allperm=list(set(reduce(operator.add,[list(itertools.permutations(x)) for x in allrot])))
    return map(list,allperm)
def generate_brag_T1(n,z):
    allrot=[recApply(rot,y)([n,n])+[z] for y in range(1,5)]+[recApply(rot,y)([n,n])+[-z] for y in range(1,5)]
    allperm= list(set(reduce(operator.add,[list(itertools.permutations(x)) for x in allrot])))
    return map(list,allperm)
def generate_brag_T2(n,z):
    allrot=[recApply(rot,y)([n-z,n+z])+[0] for y in range(1,5)]+[recApply(rot,y)([n+z,n-z])+[0] for y in range(1,5)]
    allperm= list(set(reduce(operator.add,[list(itertools.permutations(x)) for x in allrot])))
    return map(list,allperm)


def creatParamList(name,paramdata):
    if paramdata['type']=='list':
        vals=paramdata["vals"]
        return [{"name":name,"val":v} for v in vals]
    elif paramdata['type']=='range':
        vals=[]
        if "scale" not in paramdata.keys():
            vals=np.linspace(paramdata['min'],paramdata['max'],paramdata['num'])
        else:
            if paramdata["scale"]=='log':
                print "Log Scale"
                vals=np.logspace(math.log10(paramdata['min']),math.log10(paramdata['max']),paramdata['num'])        
        return [{"name":name,"val":v} for v in vals.tolist()]
    elif paramdata['type']=='range_diff':
        vals=[]
        vals=[paramdata['min']+n*paramdata['diff'] for n in range(paramdata['num'])]
        return [{"name":name,"val":v} for v in vals]
    else:
        print "Error unknown type"
        sys.exit()

 
def generateMultiSim(params,sim_data):    
        params=[creatParamList(k,params[k]) for k in params.iterkeys()]  
        all_sim_data=[]
        for par_per in itertools.product(*params):
            c_sim_data=sim_data
            for p in par_per:
                name=p["name"]
                val=p["val"]
                print name,"=",val
                sim_data[name]=val
            if ("dyn_sk" or "dyn_sk_square") in c_sim_data["measurements"]:
                n=int(round(math.pow(float(int(c_sim_data["N"])/2),1./3.)))
                print "lat spacing " , n
                c_sim_data["ks_sf"]=[{"name":"long","ks": map(generate_brag_long,range(1,2*n+1))},
                                  {"name":"t1","ks": map(lambda x:generate_brag_T1(n,x),range(1,n+1))},
                                  {"name":"t2","ks": map(lambda x:generate_brag_T2(n,x),range(1,n+1))}
                                 ]
            c_sim_data['mbar']=int(float(sim_data['M'])/float(10))
            print "mbar",sim_data['mbar']
            r_sim=math.pow(float(c_sim_data["N"])/c_sim_data["density"],1./3.)
            c_sim_data["com_dr"]=.05*math.pow(1./float(c_sim_data["N"]),1./3.)
            print "R sim size = ",r_sim," ang"
            n=int(round(math.pow(float(int(c_sim_data["N"])/2),1./3.)))
            c_sim_data["bragg_size"]=n                
            print "lat spacing " , n
            if c_sim_data["free"]:
                c_sim_data["lambda"]=1.
                c_sim_data["rm"]=1.
                c_sim_data["r_sim"]=1.
            else:
                c_sim_data["lambda"]=lambda_zero/(pow(r_sim,2))
                c_sim_data["rm"]=rm_zero/r_sim
                c_sim_data["r_sim"]=r_sim
            yield c_sim_data
            
