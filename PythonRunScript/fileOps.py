import os,sys,glob,math,stat,shutil,subprocess
import argparse
import yaml
import numpy as np
import itertools
from os.path import expanduser
from paramiko import SSHClient, SSHConfig,ProxyCommand
import shutil
import time

class gridOPs:
    def __init__(self,grid_params):
        self.grid_params=grid_params
        self.openconnection()
    def openconnection(self):
        proxy_command = 'ssh %s nc %s %s' % ('aluf.technion.ac.il', self.grid_params["full_name"], 22)
        proxy = ProxyCommand(proxy_command)
        self.client = SSHClient()
        self.client.load_system_host_keys()
        self.client.connect(self.grid_params["short_name"],sock=proxy,allow_agent=True)
        self.sftp = self.client.open_sftp()
        stdin, stdout, stderr = self.client.exec_command("pwd")
        home=stdout.readlines()[0].rstrip()
        print home
        self.grid_params['home']=home
        print "Connection Open"
    def createDir(self,path):
        try:
            self.sftp.chdir(path)  # Test if remote_path exists
        except IOError:
            self.sftp.mkdir(path);
    def writeFile(self,filename,data):
        gridfile=self.sftp.open(filename, mode='w')
        gridfile.write(data)
        gridfile.close()
        self.sftp.chmod(filename,stat.S_IRWXU | stat.S_IRWXO)
    def deleteDirectory(self,path):
        self.client.exec_command('rm -rf '+path)
        print "Deleted Directory - ",path
    def getHome(self):
        return self.grid_params['home']
    def copyFile(self,origin,dest):
        self.sftp.put(origin,dest)
    def chmod(self,filename,mod):
        self.sftp.chmod(filename,mod)
    def shellCommand(self,command):
        i=0
        while True:
            try:
                i=i+1
                (sshin, sshout, ssherr) =self.client.exec_command(command)
                extstat=ssherr.channel.recv_exit_status() 
                print ssherr.read() 
                print sshout.read()
                if i>3:
                    print "No more attempts"
                    sys.exit(1)
                    break
                else:
                    if extstat==0:
                        break
                    else:
                        time.sleep(5) 
                        raise RuntimeError('this is the error message')
            except Exception, err:
                sys.stderr.write('ERROR: %s\n' % str(err))
    def compvar(self):
        return self.grid_params["compvar"]
    def python_bin(self):
        return self.grid_params["python_bin"]               
class localOPs:
    def createDir(self,newdir):
        """works the way a good mkdir should :)
        - already exists, silently complete
        - regular file in the way, raise an exception
        - parent directory(ies) does not exist, make them as well"""
        print newdir
        if os.path.isdir(newdir):
            pass
        elif os.path.isfile(newdir):
            raise OSError("a file with the same name as the desired " \
                          "dir, '%s', already exists." % newdir)
        else:
            head, tail = os.path.split(newdir)
            if head and not os.path.isdir(head):
                _mkdir(head)
            #print "_mkdir %s" % repr(newdir)
            if tail:
                os.mkdir(newdir)
    def writeFile(self,filename,data):
        gridfile=open(filename, mode='w')
        gridfile.write(data)
        gridfile.close()
        os.chmod(filename,stat.S_IRWXU | stat.S_IRWXO)
    def deleteDirectory(self,path):
        if os.path.isdir(path):
            shutil.rmtree(path)
            print "Deleted Directory - ",path
    def getHome(self):        
        home = expanduser("~")
        return home
    def copyFile(self,origin,dest):
        shutil.copy(origin,dest)
    def chmod(self,filename,mod):
        os.chmod(filename,mod)
    def shellCommand(self,command):
        subprocess.check_call(command,shell=True)
    def compvar(self):
        return ""
    def python_bin(self):
        return "python" 