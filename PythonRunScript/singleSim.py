import os,sys,glob,math,stat,shutil,subprocess
import argparse
import random
import yaml
import numpy as np
import itertools
from paramiko import SSHClient, SSHConfig,ProxyCommand
import generateSimData
import fileOps as fo

LocalTemplate="""python {0[pythonScriptFileName]} -lib_path {0[libPath]} -sim_path {0[output_path_c]}
"""


class SingleSimLocal:
    def init(self,simData,pathData,fileOps):
        self.simData = simData
        self.pathData = pathData        
        self.fileOps=fileOps
    def generateScriptFile(self):
        command=LocalTemplate.format(self.pathData)
        self.fileOps.writeFile(self.pathData['scriptFileName'],command)
        yamldata=yaml.safe_dump(self.simData)    
        self.fileOps.writeFile(self.pathData['YAMLFileName'],yamldata)    
        pathdata=yaml.safe_dump(self.pathData)    
        self.fileOps.writeFile(self.pathData['PathDataFileName'],pathdata)
    def generateSim(self):
        ## Print script File    
        self.generateScriptFile()
    def runSim(self):
        print "Running New Sim"
        command="sh "+self.pathData['scriptFileName']+"&"
        print command
        self.fileOps.shellCommand(command)

templatePBS="""#PBS -q {0[pbsName]}
#PBS -N {0[subName]}{0[subNum]}
#PBS -l ncpus={0[num_of_cores]}
#PBS -l mem=2gb
#PBS -o {0[output_path_c]}/log/log.o
#PBS -e {0[output_path_c]}/log/log.e
export OMP_NUM_THREADS={0[num_of_cores]}
{0[comp_var]}
{0[python_bin]} {0[pythonScriptFileName]} -lib_path {0[libPath]} -sim_path {0[output_path_c]} -grid
"""
gridTemplate="""ssh {0[gridName]} 'qsub {0[qSubScriptFileName]}'"""

class SingleSimGrid:
    def init(self,simData,pathData,fileOps):
        self.simData = simData
        self.pathData = pathData
        self.fileOps=fileOps
    def generateScriptFile(self):
        self.pathData['num_of_cores']=self.simData['num_of_cores']
        self.pathData['pbsName']=self.fileOps.grid_params["PBS_name"]
        self.pathData['qSubScriptFileName']=os.path.join(self.pathData["output_path_c"],"qsub.txt")
        self.pathData['comp_var']=self.fileOps.compvar()
        self.pathData['gridName']=self.fileOps.grid_params["short_name"]
        self.pathData['python_bin']=self.fileOps.grid_params["python_bin"]
        self.pathData['subName']=self.simData['subName']
        command=templatePBS.format(self.pathData)
        self.fileOps.writeFile(self.pathData['qSubScriptFileName'],command)    
        yamldata=yaml.safe_dump(self.simData)    
        self.fileOps.writeFile(self.pathData['YAMLFileName'],yamldata)    
        pathdata=yaml.safe_dump(self.pathData)    
        self.fileOps.writeFile(self.pathData['PathDataFileName'],pathdata)
        command=gridTemplate.format(self.pathData)
        self.fileOps.writeFile(self.pathData['scriptFileName'],command)
    def generateSim(self):
        ## Print script File    
        self.generateScriptFile()
    def runSim(self):
        print "Running New Sim"
        command="sh "+self.pathData['scriptFileName']+"&"
        print command
        self.fileOps.shellCommand(command)    
