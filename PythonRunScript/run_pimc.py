import argparse
import yaml
import sys
import shutil
import os
import subprocess

if __name__ == '__main__':    
    # parse command line options
    parser = argparse.ArgumentParser(description='Run A WORM ALG Simulation for Phonon')
    # Arguments
    parser.add_argument('-lib_path',type=str, action='store')
    parser.add_argument('-sim_path',type=str, action='store')
    parser.add_argument('-grid', action='store_true')
    args = parser.parse_args()
    # Load library
    sys.path.append(args.lib_path)
    import pimc_lib as mc
    # load param file
    param_file=os.path.join(args.sim_path,'sim.yaml')
    stream=open(param_file,'r')
    sim_params=yaml.load(stream)
    sim=mc.pimc_lib()
    sim.getParamsFromPython(sim_params)
    sim.init()
    done=sim.run()
    if done:
        print 'No more runs'
    else:
        with open(os.path.join(args.sim_path,"sim.yaml"), 'r') as infile:
                    yamlData=yaml.load(infile)
        with open(os.path.join(args.sim_path,"pathdata.yaml"), 'r') as infile:
                    pathData=yaml.load(infile)
        print yamlData 
        ## Add Init Path
        yamlData['init']=True
        yamlData['init_path']=args.sim_path
        if yamlData['run_num']:
            yamlData['run_num']=1
        else:
            yamlData['run_num']+=1
        yamlstream=yaml.safe_dump(yamlData)   
        yamlfile=open(os.path.join(args.sim_path,"sim.yaml"), mode='w')
        yamlfile.write(yamlstream)
        yamlfile.close() 
        runfile=os.path.join(args.sim_path,'simRun.sh')
        command='sh '+runfile
        process = subprocess.Popen(command, shell=True,
                           stdout=subprocess.PIPE, 
                           stderr=subprocess.PIPE)

        # wait for the process to terminate
        out, err = process.communicate()
        errcode = process.returncode
        print out
        print err
        print errcode