import os
from distutils.core import setup, Extension
 
def findfiles(path,ext):
    cppfiles=[]
    for root, dirs, files in os.walk(path):
        if '.svn' in dirs:
            dirs.remove('.svn')
        for file in files:
            if file.endswith(ext):
                 cppfiles.append(os.path.join(root, file))
    return cppfiles

# define the name of the extension to use
extension_name    = 'pimc_lib'
extension_version = '1.0'
 
# define the directories to search for include files
# to get this to work, you may need to include the path
# to your boost installation. Mine was in 
# '/usr/local/include', hence the corresponding entry.
include_dirs = [ '/srv01/technion/snirgaz/Software/local/include','/home/snirgaz/local/include','/u/snirgaz/local/include'
                ,'/usr/local/include'] 
# define the library directories to include any extra
# libraries that may be needed.  The boost::python
# library for me was located in '/usr/local/lib'
library_dirs = ['/home/snirgaz/local/lib','/u/snirgaz/local/lib','/srv01/technion/snirgaz/Software/local/lib','/usr/local/lib']
 
# define the libraries to link with the boost python library
libraries = ['hdf5','fftw3','pthread','hdf5_cpp','z','yaml-cpp','boost_python','m','boost_numpy' ]
 
# define the source files for the extension
source_files = findfiles("./src",".cpp")
source_files=filter(lambda a: a != "./src/run.cpp", source_files)
print source_files

define_macros=[('NDEBUG', None),
        ('PARALLEL_OPENMP', None),
        ('CENTER_OF_MASS_MOVE',None), 
        ('SWAP',None),
        ('FORCE',None),
        ('POT_AZIZ',None),
        ('PYTHON',None),
        ('SIM_DIM','3')]

language='c++'

extra_compile_args=['-O3', '-std=c++11','-fopenmp','-w']
# create the extension and add it to the python distribution
extra_link_args=['-fopenmp']
setup( name=extension_name, version=extension_version, 
       ext_modules=[Extension( extension_name, source_files
                               ,include_dirs=include_dirs, library_dirs=library_dirs,runtime_library_dirs=library_dirs
                               ,libraries=libraries,define_macros=define_macros,extra_compile_args=extra_compile_args
                               ,extra_link_args=extra_link_args,language=language)] )
